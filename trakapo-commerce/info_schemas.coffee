basket:
	fields: [
		{field: "bid", required: true}
		{field: "sid", required: true}
		{field: "did", required: true}
		{field: "id", cast: "string | lowercase"}
		{
			field: "status",
			type: "string"
		}
		{
			field: "line_items",
			type: "array",
			required: true
			fields: [
				{field: "sku", required: true}
				{field: "name", required: true}
				{field: "quantity", default: 1, cast: "number"}
				{
					field: "price",
					required: true,
					fields: [{field: "value", cast: "number", required: true}]
				}
			]
		},
		{
			field: "prices",
			required: true
			fields: [
				{
					field: "tax",
					fields: [{field: "value", cast: "number", required: true}]
				}
				{
					field: "items",
					fields: [{field: "value", cast: "number", required: true}]
				}
				{
					field: "total",
					fields: [{field: "value", cast: "number", required: true}]
				}
			]
		}
	]

basket_status:
	fields: [
		{field: "id", cast: "string | lowercase"}
		{field: "bid", required: true}
		{field: "status", required: true, type: 'string'}
		{field: "info", required: false, type: 'object'}
	]
user:
	$diff: [
		{
			mode: 'set'
			field: 'fields.baskets'
			value:
				mode: 'relation'
				fact_type: 'basket'
				has: 'many'
				query:
					user_id: 'fact._id'
		}
		{
			mode: 'set'
			field: "fields.bids"
			value:
				mode: "push_unique"
		}
	]
	
session:
	$diff: [
		{
			mode: 'set'
			field: 'fields.baskets'
			value:
				mode: 'relation'
				fact_type: 'basket'
				has: 'many'
				query:
					sid: 'fact._id'
		}
		{
			mode: 'set'
			field: "fields.bids"
			value:
				mode: "push_unique"
		}		
	]

basket:
	fields:
		user:
			mode: 'relation'
			fact_type: 'user'
			has: 'one'
			query:
				_id: 'fact.user_id'
		device:
			mode: 'relation'
			fact_type: 'device'
			has: 'one'
			query:
				_id: 'fact.did'
		session:
			mode: 'relation'
			fact_type: 'session'
			has: 'one'
			query:
				_id: 'fact.sid'

		sids:
			mode: 'push_unique'
		dids:
			mode: 'push_unique'

		price:
			mode: 'extend'
		info:
			mode: 'extend'
		status:
			mode: 'extend'

		source:
			mode: "eval"
			map:
				action: "this.session.actions[0]._value"
			eval: (settings) ->
				fn = () ->
					try
						local = url action.url, 'host'
						query = url action.url, 'query'
						referrer = action.referrer and url action.referrer, 'host'

						if referrer and referrer.length > 0 and referrer isnt local
							if referrer.match /(google|bing|yahoo|ask|aol)\./
								return medium: 'search', source: referrer
							return medium: 'referrer', source: referrer

						if query?.utm_medium?
							return {
								medium: query.utm_medium # email
								source: query.utm_source # keepintouch
								campaign: query.utm_campaign
							}

						return medium: 'direct', source: action.url
					catch e
						return medium: 'error', error: e.message, stage: stage

				return "(#{fn.toString()})()"
{
	mapping_id: "baskets",
	info_type: "basket",
	fact_type: "basket",
	fact_identifier: "info.bid",
	fields: [
		{field: "user_id", value: "info.id", condition: "info.id"}
		{field: "did", value: "info.did"}
		{field: "sid", value: "info.sid"}
		{field: "dids", value: "info.did"}
		{field: "sids", value: "info.sid"}
		{field: "basket", value: "info.line_items", condition: "info.line_items"}
		{field: "price", value: "info.prices", condition: "info.prices"}
		# default the status to this
		{field: "status", value: "('undefined' !== typeof fact && fact.status) ? fact.status : {open: new Date, current: 'open'}"}
	]
}
{
	mapping_id: "basket_status",
	info_type: "basket_status",
	fact_type: "basket",
	fact_identifier: "info.bid",
	update_only: true
	fields: [
		{field: "dids", value: "info.did"}
		{field: "sids", value: "info.sid"}
		{field: "status.{{info.status}}", value: "new Date", condition: "info.status"}
		{field: "status.current", value: "info.status", condition: "info.status"},
		{field: "info", value: "info.info", condition: "info.info"}
	]
}

# multiplex the above to the "user" object to cause hook sends
{
	mapping_id: "user_update",
	info_type: "basket"
	fact_type: "user"
	fact_identifier: "info.id"
	update_only: true
	fields: {}
}
{
	mapping_id: "user_update",
	info_type: "basket_status",
	fact_type: "user"
	fact_identifier: "info.id"
	update_only: true
	fields: {}
}
{
	mapping_id: "session_update",
	info_type: "basket"
	fact_type: "session"
	fact_identifier: "info.sid"
	update_only: true
	fields: {}
}
{
	mapping_id: "session_update",
	info_type: "basket_status",
	fact_type: "session"
	fact_identifier: "info.sid"
	update_only: true
	fields: {}
}
{
	mapping_id: "device_update",
	info_type: "basket"
	fact_type: "device"
	fact_identifier: "info.did"
	update_only: true
	fields: {}
}
{
	mapping_id: "device_update",
	info_type: "basket_status",
	fact_type: "device"
	fact_identifier: "info.did"
	update_only: true
	fields: {}
}

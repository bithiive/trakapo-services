{
	name: "Trakapo Commerce",
	description: "Commerce features",
	version: "1.1.2"
	icon: 'cart'

	# todo: these dependencies are not currently observed
	dependencies: ["trakapo-core"],

	metrics: [
		{
			fact_type: 'basket'
			metric_id: 'sales'
			group: 'status.current'
			by_created: true
			fields:
				count:
					mode: 'count'
				total:
					mode: 'sum'
					input: 'price.total.value'
		}
	]

	info_mappings: [
		#!info_mappings.coffee
	],

	info_schemas:
		#!info_schemas.coffee

	fact_settings:
		#!fact_settings.coffee

}

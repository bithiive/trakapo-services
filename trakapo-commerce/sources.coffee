# not currently used!
(settings) ->
    fn = ->
        do (sessions = this.sessions) ->
            return sessions.map((session) ->
                try
                    if session.referrer and host = url(session.referrer, 'host')    
                        if host.match /(google|bing|yahoo|ask|aol)\./
                            return medium: 'search', source: host
                        if host.indexOf('#{settings.site_url}') < 0
                            return medium: 'referrer', source: session.referrer
                    if session.query?.utm_medium?
                        return {
                            medium: session.query.utm_medium
                            source: session.query.utm_source
                            campaign: session.query.utm_campaign
                        }
                    return medium: 'direct', source: session.actions[0]._value.url
                  catch e
                    return medium: error, error: e.message
            ).reduce((obj, source) ->
                obj[source.medium] ?= []
                obj[source.medium].push source
                return obj
            , {})

    return JSON.stringify(fn.toString()).slice(25, -5).replace('#{settings.site_url}', "'#{settings.site_url}'")

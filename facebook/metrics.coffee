{
	"metric_id": "type_counts",
	"fact_type": "facebook_interaction",
	"by_created": true,
	"backfill_count": 5,
	"group": ["page_id", "type"],
	"fields": {
		"count": {
			"mode": "count"
		}
	}
},

{
	"metric_id": "summary",
	"fact_type": "facebook_page",
	"filter": {
		"summarize": true
	},
	"project": {
		"followers": 1,
		"engagement_ratio": 1
	}
},

{
	"metric_id": "summary",
	"fact_type": "facebook_status",
	"group": "page_id",
	"by_created": true,
	"filter": {
		"text": {"$ne": null}
	},
	"fields": {
		"count": {
			"mode": "count"
		},

		"reply_sum": {
			"mode": "sum",
			"input": "reply_count"
		},
		"reply_avg": {
			"mode": "avg",
			"input": "reply_count"
		},

		"like_sum": {
			"mode": "sum",
			"input": "like_count"
		},
		"like_avg": {
			"mode": "avg",
			"input": "like_count"
		},


		"share_sum": {
			"mode": "sum",
			"input": "share_count"
		},
		"share_avg": {
			"mode": "avg",
			"input": "share_count"
		}
	}
}

# this is the same as facebook_status/summary but filtered on "self" (ie, posts by this page)
{
	"metric_id": "summary_self",
	"fact_type": "facebook_status",
	"backfill_count": 5,
	"group": "page_id",
	"filter": {
		"text": {"$ne": null},
		"self": true
	},
	"by_created": true,
	"fields": {
		"count": {
			"mode": "count"
		},

		"reply_sum": {
			"mode": "sum",
			"input": "reply_count"
		},
		"reply_avg": {
			"mode": "avg",
			"input": "reply_count"
		},

		"like_sum": {
			"mode": "sum",
			"input": "like_count"
		},
		"like_avg": {
			"mode": "avg",
			"input": "like_count"
		},


		"share_sum": {
			"mode": "sum",
			"input": "share_count"
		},
		"share_avg": {
			"mode": "avg",
			"input": "share_count"
		}
	}
}

{
	"metric_id": "numbers",
	"fact_type": "facebook_metric_value",
	"by_created": true,
	"backfill_count": 3,
	"filter": {
		"period": "day", # there are week and days28 entries to ignore
		"value": {
			"$type": 16 # "16-bit integer" values only
		}
	}
	"group": ["object", "metric"],
	"fields": {
		"sum": {
			"mode": 'sum',
			"input": 'value'
		},
		"mean": {
			"mode": 'avg',
			"input": 'value'
		},
		"min": {
			"mode": 'min',
			"input": 'value'
		},
		"max": {
			"mode": 'max',
			"input": 'value'
		}
	}
}


{
	"metric_id": "followers",
	"fact_type": "facebook_metric_value",
	"by_created": true,
	"backfill_count": 3,
	"filter": {
		"period": 'all_time',
		"metric": 'followers'
	},
	"group": 'object',
	"fields": {
		"start": {
			"mode": 'last',
			"input": 'value'
		},
		"end": {
			"mode": 'first',
			"input": 'value'
		},
		"min": {
			"mode": 'min',
			"input": 'value'
		},
		"max": {
			"mode": 'max',
			"input": 'value'
		}
	}
}

{
	"metric_id": "reach",
	"fact_type": "facebook_metric_value",
	"by_created": true,
	"backfill_count": 3,
	"filter": {
		"period": 'week',
		"metric": 'page_impressions_unique'
	},
	"group": 'object',
	"fields": {
		"sum": {
			"mode": 'sum',
			"input": 'value'
		},
		"mean": {
			"mode": 'avg',
			"input": 'value'
		},
		"min": {
			"mode": 'min',
			"input": 'value'
		},
		"max": {
			"mode": 'max',
			"input": 'value'
		}
	}
}

{
	"mapping_id": "facebook_comment_interaction",
	"info_type": "facebook_comment",
	"fact_type": "facebook_interaction",
	"fact_identifier": "info._id",
	"fields": [
		{
			"field": "_created",
			"value": "new Date(info.timestamp)"
		},
		{
			"field": "type",
			"value": "'reply'"
		},
		{
			"field": "text",
			"value": "info.text"
		},
		{
			"field": "page_id",
			"value": "info.account"
		},
		{
			"field": "user_id",
			"value": "info.from._id"
		},
		{
			"field": "status_id",
			"value": "info.parent_id"
		}
	]
},

{
	"mapping_id": "facebook_comment_status",
	"info_type": "facebook_comment",
	"fact_type": "facebook_status",
	"fact_identifier": "info.parent_id",
	"fields": []
},

{
	"mapping_id": "facebook_comment_page",
	"info_type": "facebook_comment",
	"fact_type": "facebook_page",
	"fact_identifier": "info.from._id",
	"fields": [
		{
			"field": "name",
			"value": "info.from.name"
		}
	]
}

"facebook_page": {
	"fields": {
		"statuses": {
			"mode": "relation",
			"fact_type": "facebook_status",
			"has": "many",
			"query": {
				"page_id": "fact._id"
			}
		},

		# these are mapped in via a metric on interactions, coming through as individual {type=count} info
		# as such, they need to extend the existing object
		"count": {
			"mode": "extend"
		},

		# # this will only be not-null for "summarize=true" pages.
		"engagement_ratio": {
			"mode": "eval",
			"condition": "fact.summarize",
			"eval": "parseFloat((fact.followers / fact.engagement).toFixed(4))"
		},

		"reach": {
			"mode": "relation",
			"fact_type": "facebook_metric",
			"has": "many",
			"query": {
				"object": "fact._id",
				"metric": "'page_impressions_unique'",
				"period": "'week'"
			}
		},

		"engagement_vals": {
			"mode": "relation",
			"fact_type": "facebook_metric",
			"has": "many",
			"query": {
				"object": "fact._id",
				"metric": "'engagement'",
				"period": "'week'"
			}
		},

		"followers_vals": {
			"mode": "relation",
			"fact_type": "facebook_metric",
			"has": "many",
			"query": {
				"object": "fact._id",
				"metric": "'followers'",
				"period": "'all_time'"
			}
		}
	}
},

"facebook_status": {
	"fields": {
		"self": {
			"mode": "eval",
			"eval": "fact.page_id == fact.user_id"
		},
		"page": {
			"mode": "relation",
			"fact_type": "facebook_page",
			"has": "one",
			"query": {
				"_id": "fact.page_id"
			}
		},

		"likes": {
			"mode": "relation",
			"fact_type": "facebook_interaction",
			"has": "many",
			"query": {
				"type": "'like'",
				"status_id": "fact._id"
			}
		},

		"like_count": {
			"mode": "eval",
			"eval": "relation.count('likes')"
		},

		"replies": {
			"mode": "relation",
			"fact_type": "facebook_interaction",
			"has": "many",
			"query": {
				"type": "'reply'",
				"status_id": "fact._id"
			}
		},

		"reply_count": {
			"mode": "eval",
			"eval": "relation.count('replies')"
		}

		"interaction_count": {
			"mode": "eval",
			"eval": "relation.count('replies', 'likes')"
		}
	}
},

"facebook_interaction": {
	"fields": {
		"page": {
			"mode": "relation",
			"fact_type": "facebook_page",
			"has": "one",
			"query": {
				"_id": "fact.page_id"
			}
		},

		"user": {
			"mode": "relation",
			"fact_type": "facebook_page",
			"has": "one",
			"query": {
				"_id": "fact.user_id"
			}
		},

		"status": {
			"mode": "relation",
			"fact_type": "facebook_status",
			"has": "one",
			"query": {
				"_id": "fact.status_id"
			}
		}
	}
},

"facebook_metric": {
	"fields": {
		"values": {
			"mode": "relation",
			"fact_type": "facebook_metric_value",
			"has": "many",
			"query": {
				"object": "fact.object",
				"metric": "fact.metric",
				"period": "fact.period"
			}
		}
	}
}

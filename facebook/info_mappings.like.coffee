{
	"mapping_id": "facebook_like_interaction",
	"info_type": "facebook_like",
	"fact_type": "facebook_interaction",
	"fact_identifier": "info._id",
	"fields": [
		{
			"field": "_created",
			"value": "new Date(info.timestamp)"
		},
		{
			"field": "type",
			"value": "'like'"
		},
		{
			"field": "page_id",
			"value": "info.account"
		},
		{
			"field": "user_id",
			"value": "info.user._id"
		},
		{
			"field": "status_id",
			"value": "info.parent_id"
		}
	]
},

{
	"mapping_id": "facebook_like_status",
	"info_type": "facebook_like",
	"fact_type": "facebook_status",
	"fact_identifier": "info.parent_id",
	"fields": []
},

{
	"mapping_id": "facebook_like_page",
	"info_type": "facebook_like",
	"fact_type": "facebook_page",
	"fact_identifier": "info.user._id",
	"fields": [
		{
			"field": "handle",
			"value": "info.user.url.split('/')[3] === 'pages' ? info.user.url.split('/')[4] : info.user.url.split('/')[3]"
		},
		{
			"field": "url",
			"value": "info.user.url"
		},
		{
			"field": "name",
			"value": "info.user.name"
		},
		{
			"field": "image",
			"value": "info.user.picture"
		}
	]
}

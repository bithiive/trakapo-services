{
	"mapping_id": "facebook_status",
	"info_type": "facebook_status",
	"fact_type": "facebook_status",
	"fact_identifier": "info._id",
	"fields": [
		{
			"field": "_created",
			"value": "new Date(info.timestamp)"
		},
		{
			"field": "text",
			"value": "info.text"
		},
		{
			"field": "page_id",
			"value": "info.account"
		},
		{
			"field": "user_id",
			"value": "info.from._id"
		},
		{
			"field": "like_count",
			"value": "info.like_count || 0"
		},
		{
			"field": "reply_count",
			"value": "info.reply_count || 0"
		},
		{
			"field": "share_count",
			"value": "info.share_count || 0"
		}
	]
}

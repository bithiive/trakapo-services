{
	"mapping_id": "facebook_page",
	"info_type": "facebook_page",
	"fact_type": "facebook_page",
	"fact_identifier": "info._id",
	"fields": [
		# {
		# 	"field": "_created",
		# 	"value": "new Date(info.user_since)"
		# },
		{
			"field": "handle",
			"value": "info.screen_name"
		},
		{
			"field": "name",
			"value": "info.name"
		},
		{
			"field": "location",
			"value": "info.location"
		},
		{
			"field": "followers",
			"value": "info.followers"
		},
		{
			"field": "engagement",
			"value": "info.engagement"
		},
		{
			"field": "image",
			"value": "info.image"
		},
		{
			"field": "summarize",
			"value": "!!info.summarize"
		}
	]
}

# map metric results into the count field
{
	"mapping_id": "facebook_page_er_counts",
	"info_type": "facebook_page_er_count",
	"fact_type": "facebook_page",
	"fact_identifier": "info._id.page_id",
	"fields": [
		{
			"field": "count.{{info._id.type}}",
			"value": "info.count"
		}
	]
},


{
	"mapping_id": "facebook_page_metric_engagement",
	"info_type": "facebook_page",
	"fact_type": "facebook_metric",
	"fact_identifier": "info._id + '-engagement-day'",
	"fields": [
		{
			"field": "name",
			"value": "'Engagement'"
		},
		{
			"field": "description",
			"value": "''"
		},
		{
			"field": "page_id",
			"value": "info._id"
		},
		{
			"field": "object",
			"value": "info._id"
		},
		{
			"field": "metric",
			"value": "'engagement'"
		},
		{
			"field": "period",
			"value": "'day'"
		}
	]
},
{
	"mapping_id": "facebook_page_metric_values_engagement",
	"info_type": "facebook_page",
	"fact_type": "facebook_metric_value",
	"fact_identifier": "info._id + '-engagement-day-' + (info.historic || (new Date()).toISOString())",
	"fields": [
		{
			"field": "_created",
			"value": "new Date(info.historic || new Date())"
		},
		{
			"field": "value",
			"value": "info.engagement"
		},
		{
			"field": "object",
			"value": "info._id"
		},
		{
			"field": "metric",
			"value": "'engagement'"
		},
		{
			"field": "period",
			"value": "'day'"
		}
	]
},

{
	"mapping_id": "facebook_page_metric_followers",
	"info_type": "facebook_page",
	"fact_type": "facebook_metric",
	"fact_identifier": "info._id + '-followers-all_time'",
	"fields": [
		{
			"field": "name",
			"value": "'Followers'"
		},
		{
			"field": "description",
			"value": "''"
		},
		{
			"field": "page_id",
			"value": "info._id"
		},
		{
			"field": "object",
			"value": "info._id"
		},
		{
			"field": "metric",
			"value": "'followers'"
		},
		{
			"field": "period",
			"value": "'all_time'"
		}
	]
},
{
	"mapping_id": "facebook_page_metric_values_followers",
	"info_type": "facebook_page",
	"fact_type": "facebook_metric_value",
	"fact_identifier": "info._id + '-followers-all_time-' + (info.historic || (new Date()).toISOString())",
	"fields": [
		{
			"field": "_created",
			"value": "new Date(info.historic || new Date())"
		},
		{
			"field": "value",
			"value": "info.followers"
		},
		{
			"field": "object",
			"value": "info._id"
		},
		{
			"field": "metric",
			"value": "'followers'"
		},
		{
			"field": "period",
			"value": "'all_time'"
		}
	]
}

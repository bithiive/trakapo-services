{
	"mapping_id": "facebook_metric_values",
	"info_type": "facebook_metric",
	"fact_type": "facebook_metric_value",
	"fact_identifier": "info._id.object + '-' + info._id.metric + '-' + info._id.period + '-' + info._id.timestamp",
	"fields": [
		{
			"field": "_created",
			"value": "new Date(info._id.timestamp)"
		},
		{
			"field": "value",
			"value": "info.value"
		},
		{
			"field": "object",
			"value": "info._id.object"
		},
		{
			"field": "metric",
			"value": "info._id.metric"
		},
		{
			"field": "period",
			"value": "info._id.period"
		}
	]
},

# Because mongo is poor at compression and it kinda makes sense to separate these anyway
{
	"mapping_id": "facebook_metric",
	"info_type": "facebook_metric",
	"fact_type": "facebook_metric",
	"fact_identifier": "info._id.object + '-' + info._id.metric + '-' + info._id.period",
	"fields": [
		{
			"field": "name",
			"value": "info.name"
		},
		{
			"field": "description",
			"value": "info.description"
		},
		{
			"field": "page_id",
			"value": "info.account"
		},
		{
			"field": "object",
			"value": "info._id.object"
		},
		{
			"field": "metric",
			"value": "info._id.metric"
		},
		{
			"field": "period",
			"value": "info._id.period"
		}

	]
}

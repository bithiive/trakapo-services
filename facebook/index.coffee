{
	"name": "Facebook",
	"description": "Facebook social service",
	"version": "0.1.5",
	"public": false,

	"info_mappings": [
		#!info_mappings.coffee
	],

	"metrics": [
		#!metrics.coffee
	],

	"hooks": {
		#!hooks.coffee
	},

	"fact_settings": {
		#!fact_settings.coffee
	}
}

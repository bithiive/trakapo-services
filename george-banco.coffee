{
	"name": "George Banco",
	"version": "1.0.11",
	"public": false,
	"dependencies": ["trakapo-core"],

	"info_mappings": [
		{
			"mapping_id": "guarantors_from_tracking",
			"fact_type": "guarantor",
			"fact_identifier": "info.id + '-' + info.action.data.Guarantor.email",
			"info_type": "track",
			"conditions": [
				"info.action.type == 'form'",
				"info.action.form_id == '/apply/step-5'",
				"info.action.data.Guarantor.email.indexOf('@') !== -1"
			],
			"fields": [
				{
					"field": "_created",
					"value": "(new Date()).toISOString()"
				},
				{
					"field": "first_name",
					"value": "info.action.data.Guarantor.first_name"
				},
				{
					"field": "surname",
					"value": "info.action.data.Guarantor.surname"
				},
				{
					"field": "applicant_confirmed_age",
					"value": "!!(info.action.data.Guarantor.applicant_confirmed_age)"
				},
				{
					"field": "email",
					"value": "info.action.data.Guarantor.email"
				},
				{
					"field": "mobile_number",
					"value": "info.action.data.Guarantor.mobile_number"
				},
				{
					"field": "applicant_relation",
					"value": "info.action.data.Guarantor.applicant_relation"
				},
				{
					"field": "last_did",
					"value": "info.did"
				},
				{
					"field": "last_sid",
					"value": "info.sid"
				},
				{
					"field": "user_id",
					"value": "info.id"
				}
			]
		},
		
		{
			"mapping_id": "guarantors_from_guarantor",
			"fact_type": "guarantor",
			"fact_identifier": "info.id + '-' + info.email",
			"info_type": "guarantor",
			"conditions": [
				"info.email.indexOf('@') !== -1"
			],
			"fields": [
				{
					"field": "_created",
					"value": "(new Date()).toISOString()"
				},
				{
					"field": "first_name",
					"value": "info.first_name || fact.data.first_name"
				},
				{
					"field": "surname",
					"value": "info.surname || fact.data.surname"
				},
				{
					"field": "applicant_confirmed_age",
					"value": "!!(info.applicant_confirmed_age) || fact.data.applicant_confirmed_age"
				},
				{
					"field": "email",
					"value": "info.email || fact.data.email"
				},
				{
					"field": "mobile_number",
					"value": "info.mobile_number || fact.data.mobile_number"
				},
				{
					"field": "applicant_relation",
					"value": "info.applicant_relation || fact.data.applicant_relation"
				},
				{
					"field": "account_activated",
					"value": "info.account_activated",
					"condition": "typeof info.account_activated !== 'undefined'"
				},
				{
					"field": "application_completed_step",
					"value": "info.application_completed_step",
					"condition": "typeof info.application_completed_step !== 'undefined'"
				},
				{
					"field": "activation_token",
					"value": "( info.activation_token === false || info.activation_token === 'false' ) ? '' : info.activation_token",
					"condition": "typeof info.activation_token !== 'undefined'"
				},
				{
					"field": "activation_token_sms",
					"value": "( info.activation_token_sms === false || info.activation_token_sms === 'false' ) ? '' : info.activation_token_sms",
					"condition": "typeof info.activation_token_sms !== 'undefined'"
				},
				{
					"field": "status",
					"value": "info.status",
					"condition": "typeof info.status !== 'undefined'"
				},
				{
					"field": "user_id",
					"value": "info.id || fact.data.id"
				}
			]
		}
		
		{
			"mapping_id": "guarantors_from_guarantor_update_user",
			"fact_type": "user",
			"fact_identifier": "info.id",
			"info_type": "guarantor",
			"conditions": [
				"info.email.indexOf('@') !== -1"
			],
			"fields": []
		}
		
		
	],

	"fact_settings": {
		"user": {
			"$diff": [
				{
					"mode": "set",
					"field": "fields.guarantors",
					"value": {
						"mode": "relation",
						"fact_type": "guarantor",
						"has": "many",
						"query": {
							"user_id": "fact._id",
							"_created": 'x={$gt: new Date(2014, 7, 1)}'
						}
					}
				}
			]
		},
		
		"guarantor": {
			"fields": {
				"_created": {
					"mode": "oldest"
				}
			}
		}
		
	},

	tracking_goals:
		identify_new_step_1: {
			triggers: [
				{
					type: "load",
					element: ".quick-apply > form"
				},
				{
					type: "event",
					event: "blur",
					element: "input",
					processForm: true
				},
				{
					type: "submit",
					element: ".quick-apply > form"
				}
			],
			conditions: [
				{
					type: "exists",
					element: ".quick-apply > form"
				},
				{
					type: "contains",
					element: ".quick-apply > form #inputLoanApplicantEmail",
					value: "@"
				}
			],
			events: [
				{
					type: "identify",
					atomic: true,
					data: {
						id: "=data['loan-applicant-email']",
						email: "=data['loan-applicant-email']",
						name: "=data['loan-applicant-name']",
						info:
							first_name: "=data['loan-applicant-name']",
							loan_amount: "=data['quick-apply-borrow']",
							loan_term: "=data['quick-apply-period']",
							tel: "=data['loan-applicant-phone-number']"
					},
					restrict: "=restrict(session, key, data)"
				}
			]
		},
		identify_step_1: {
			triggers: [
				{
					type: "load",
					element: "form#applicant-form-step-1"
				},
				{
					type: "event",
					event: "blur",
					element: "input",
					processForm: true
				},
				{
					type: "submit",
					element: "form#applicant-form-step-1"
				}
			],
			conditions: [
				{
					type: "exists",
					element: "form#applicant-form-step-1"
				},
				{
					type: "contains",
					element: "form#applicant-form-step-1 [name=\"LoanApplicationStepOne[applicant][email]\"]",
					value: "@"
				}
			],
			events: [
				{
					type: "identify",
					atomic: true,
					data: {
						id: "=data.LoanApplicationStepOne.applicant.email",
						email: "=data.LoanApplicationStepOne.applicant.email",
						name: "=data.LoanApplicationStepOne.applicant.first_name + ( ' ' + data.LoanApplicationStepOne.applicant.middle_name + ' ' ).replace( '  ', ' ' ) + data.LoanApplicationStepOne.applicant.surname",
						info:
							title: "=data.LoanApplicationStepOne.applicant.title",
							first_name: "=data.LoanApplicationStepOne.applicant.first_name",
							middle_name: "=data.LoanApplicationStepOne.applicant.middle_name",
							last_name: "=data.LoanApplicationStepOne.applicant.surname",
							loan_amount: "=data.LoanApplicationStepOne.loan_application.loan_amount",
							loan_term: "=data.LoanApplicationStepOne.loan_application.loan_term"
					},
					restrict: "=restrict(session, key, data)"
				}
			]
		},
		identify_step_2: {
			triggers: [
				{
					type: "load",
					element: ".form-step-2 form"
				},
				{
					type: "event",
					event: "blur",
					element: "input",
					processForm: true
				},
				{
					type: "submit",
					element: ".form-step-2 form"
				}
			],
			conditions: [
				{
					type: "exists",
					element: ".form-step-2 form"
				},
				{
					type: "contains",
					element: ".form-step-2 form [name=\"LoanApplicationStepTwo[applicant][mobile_number]\"]",
					value: "7"
				}
			],
			events: [
				{
					type: "identify",
					atomic: true,
					data: {
						info: {
							tel: "=data.LoanApplicationStepTwo.applicant.mobile_number",
							street_address: "=data.LoanApplicationStepTwo.address_1.address_thouroughfare",
							city: "=data.LoanApplicationStepTwo.address_1.address_post_town",
							postcode: "=data.LoanApplicationStepTwo.address_1.address_postcode"
						}
					},
					restrict: "=restrict(session, key, data)"
				}
			]
		},
		step: {
			triggers: [
				{
					type: "load"
				}
			],
			conditions: [
				{
					type: "url",
					path: "/apply/"
				}
			],
			events: [
				{
					type: "identify",
					atomic: true,
					data:
						info:
							stage: "=window.location.pathname.match(/\\/apply\\/([^\\/]*)/)[1]"
					restrict: "=restrict(session, key, data)"
				}
			]
		}
	
}

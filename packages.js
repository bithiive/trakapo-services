{
	"remarketing": {
		"name": "Trakapo Remarketing",
		"version": "0.3.2",
		"icon": "newspaper",
		"description": "Display different content based on user properties",
		"dependencies": [
			"trakapo-core"
		]
	},
	"trakapo-clickmap": {
		"name": "Trakapo Clickmap",
		"version": "0.0.2",
		"icon": "mouse",
		"description": "Track which links are clicked on a page",
		"dependencies": []
	},
	"trakapo-commerce": {
		"name": "Trakapo Commerce",
		"version": "1.1.2",
		"icon": "cart",
		"description": "Commerce features",
		"dependencies": [
			"trakapo-core"
		]
	},
	"trakapo-core": {
		"name": "Trakapo Core",
		"version": "1.2.5",
		"icon": "line-graph",
		"description": "Primary configuration for Trakapo accounts",
		"dependencies": []
	}
}
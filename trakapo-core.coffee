{
	"name": "Trakapo Core",
	"version": "1.2.5",
	"description": "Primary configuration for Trakapo accounts",
	"icon": "line-graph",

	# context:
	#     action_id: (sid, type, action) ->
	#         return sid + '-' + encrypt('md5', Date.now() + type + JSON.stringify(action)).slice(0, 8)

	info_mappings: [
		{
			mapping_id: "device"
			fact_type: "device"
			fact_identifier: "info.did"
			info_type: "track"
			fields:
				device: "info.device"
				user_id: "info.id || ('undefined' !== typeof fact ? fact.user_id : null)"
				sids: "info.sid"
		}
		{
			mapping_id: "user"
			fact_type: "user"
			fact_identifier: "info.id"
			info_type: "track"
			fields:
				last_sid: "info.sid"
				last_did: "info.did"
				sids: "info.sid"
				dids: "info.did"
		}
		{
			mapping_id: "sessions"
			fact_type: "session"
			fact_identifier: "info.sid"
			info_type: "track"
			fields:
				did: "info.did"
				dids: "info.did"
				actions: "info.action"
		}

		# # direct action...
		# {
		#     mapping_id: "action",
		#     info_type: "identify",
		#     fact_type: "action",
		#     fact_identifier: "action_id(info.sid, info.action.type, info.action)"
		#     fields: [
		#         { field: 'sid', value: 'info.sid' }
		#         { field: 'did', value: 'info.did' }
		#         { field: 'user_id', value: 'info.id', condition: 'info.id' }
		#         { field: 'type', value: 'info.action.type || info.type' }
		#         { field: 'action', value: 'info.action' }
		#     ]
		# }
		
		{
			mapping_id: "device"
			fact_type: "device"
			fact_identifier: "info.did"
			info_type: "identify"
			fields:
				user_id: "info.id"
				sids: "info.sid"
		}
		{
			mapping_id: "user"
			fact_type: "user"
			fact_identifier: "info.id"
			info_type: "identify"
			fields:
				email: "info.email || fact.email"
				name: "info.name || fact.name"
				info: "info.info"
		}
		{
			mapping_id: "session"
			fact_type: "session"
			fact_identifier: "info.sid"
			info_type: "identify"
			conditions: ["fact.user_id != info.id"]
			fields: {
				user_id: "info.id",
				actions: {
					type: '"identify"'
					id: 'info.id'
					email: 'info.email'
					name: 'info.name'
					info: "info.info"
				}
			}
		}

		# # direct action...
		# {
		#     mapping_id: "action",
		#     info_type: "identify",
		#     fact_type: "action",
		#     fact_identifier: "action_id(info.sid, 'identify', info)"
		#     fields:
		#         sid: 'info.sid'
		#         did: 'info.did'
		#         user_id: 'info.id'
		#         type: 'identify'
		#         action: 'x = {id: info.id, email: info.email, name: info.name, info: info.info}'
		# }
		
		{
			mapping_id: "fields"
			fact_type: "fields"
			fact_identifier: "info.type"
			info_type: "fields"
			fields:
				field_paths: "info.field_paths"
				field_counts: "info.field_paths"
				field_types: "info.field_types"
		}
		
		{
			mapping_id: "errors"
			fact_type: "errors"
			fact_identifier: "Date.now() + '-' + info.sid"
			info_type: "error"
			fields:
				sid: "info.sid",
				source: "info.source"
				error: "info.error",
				info: "info.extra"
		}

		
	],
	info_schemas:
		identify:
			fields: [
				{
					field: 'id'
					required: true
					cast: 'string | lowercase'
				}
			]
		track:
			fields: [
				{field: 'action.type', type: 'string', required: true}
				{field: 'sid', cast: 'string', required: true}
				{field: 'did', cast: 'string', required: true}
				{field: 'user_id', cast: 'string | lowercase'}
				{field: 'id', cast: 'string | lowercase'}
			]

	metrics: [
		# top users by how much there score changed.
		# this NEEDS the "day" range to exist, so that's forced...
		{
			"fact_type": "user",
			"metric_id": "most_improved",
			"project": {
				"score.today": 1 # include this, to reduce effort
			},
			"sort": "-score.change.day.absolute",
			"limit": 50
		}

		# number of updated sessions today
		{
			"fact_type": "session",
			"metric_id": "unique_visitors",
			"fields": {
				"count": {
					"mode": "count"
				}
			}
		}

		# number of actions produced
		{
			"fact_type": "session",
			"metric_id": "action_counts",
			"unwind": "actions",
			"filter": {
				"actions._time": "$bucket"
			},
			"group": "actions._value.type",
			"fields": {
				"count": {
					"mode": "count"
				}
			}
		}

		# top-viewed pages this day
		{
			"fact_type": "session",
			"metric_id": "popular_pages",
			"unwind": "actions",
			"filter": {
				"actions._value.type": "page",
				"actions._time": "$bucket"
			}
			"group": "actions._value.url",
			"fields": {
				"count": {
					"mode": "count"
				}
			},
			"sort": "-count",
			"limit": 50
		}

		# top referrering domains over time
		{
			"fact_type": "session",
			"metric_id": "referrer_domains",
			"filter": {
				"referrer_domain": {
					"$exists": 1
				}
			},
			"group": "referrer_domain",
			"fields": {
				"count": {
					"mode": "count"
				}
			},
			"sort": "-count",
			"limit": 50
		}

		# top referring urls domains over time
		{
			"fact_type": "session",
			"metric_id": "referrer_urls",
			"group": "referrer",
			"fields": {
				"count": {
					"mode": "count"
				}
			},
			"sort": "-count",
			"limit": 50
		}

		# top countries+cities
		{
			"fact_type": "device",
			"metric_id": "top_countries",
			"filter": {
				"location.country_name": {
					"$exists": true
				}
			},
			"group": "location.country_name",
			"fields": {
				"count": {
					"mode": "count"
				}
			},
			"sort": "-count",
			"limit": 50
		}
		{
			"fact_type": "device",
			"metric_id": "top_cities",
			"filter": {
				"location.city": {
					"$exists": true
				}
			},
			"group": "location.city",
			"fields": {
				"count": {
					"mode": "count"
				},
				"country": {
					"mode": "first",
					"input": "location.country_name"
				}
			},
			"sort": "-count",
			"limit": 50
		}

		# pageview stats
		{
			"fact_type": "session",
			"metric_id": "pages",
			"unwind": "actions",
			"filter": {
				"actions._value.type": "page",
				"actions._time": "$bucket"
			},
			"group": "actions._value.url",
			"fields": {
				"count": {
					"mode": "count"
				}
			},
			"sort": "-count"
		}
		
	],
	fact_settings: {
		device:
			fields:
				location:
					mode: 'eval'
					# this key may eventually be a faction key, for now it doesnt matter
					condition: "'undefined' == typeof fact.location"
					eval: 'fact.location ? fact.location : http("http://freegeoip.net/json/" + fact.device.ip)'
				user:
					mode: 'relation'
					fact_type: "user"
					has: "one"
					query:
						_id: "fact.user_id"
				sessions:
					mode: 'relation'
					fact_type: "session"
					has: "many"
					query:
						did: "fact._id"

				sids:
					mode: "push_unique"
		
		referrer:
			fields:
				path:
					mode: "inc_map"
				# remove "full" from the database over time
				full:
					mode: "remove"
		
		session:
			fields:
				device:
					mode: 'relation'
					fact_type: "device"
					has: "one"
					query:
						_id: "fact.did"
				user:
					mode: 'relation'
					fact_type: 'user'
					has: 'one'
					query:
						_id: 'fact.user_id'

				dids:
					mode: "push_unique"

				user_id:
					mode: 'eval'
					condition: '"string" != typeof fact.user_id'
					map:
						uid: "fact.device.user_id"
					eval: "'string' == typeof uid ? uid : null"

				actions:
					mode: "all"
				referrer:
					mode: "eval"
					condition: "'undefined' == typeof fact.referrer"
					eval: "fact.get('actions._value.referrer').shift()"
				referrer_domain:
					mode: 'eval'
					condition: "'undefined' == typeof fact.referrer_domain"
					eval: 'url(fact.referrer, "host")'
				query:
					mode: "eval"
					condition: "'undefined' == typeof fact.query"
					eval: "url(fact.get('actions._value.url').shift(), 'query')"


				source:
					mode: "eval"
					eval: (settings) ->
						fn = (action) ->
							try
								local = url action.url, 'host'
								query = url action.url, 'query'
								referrer = action.referrer and url action.referrer, 'host'

								# google CPC
								if query.gclid?
									return medium: 'cpc', source: 'google', campaign: query.gclid

								# some non-local referrer
								if referrer and referrer.length > 0 and referrer isnt local
									# a search referrer
									if engine = referrer.match /(google|bing|yahoo|ask|aol)\./
										return medium: 'search', source: engine[1]
									return medium: 'referrer', source: referrer

								# other UTM data
								if query?.utm_medium?
									return {
										medium: query.utm_medium
										source: query.utm_source
										campaign: query.utm_campaign
									}

								return medium: 'direct', source: action.url
							catch e
								return medium: 'error', error: e.message, stage: stage

						return "(#{fn.toString()})(fact.actions[0]._value)"

				# score:
				# 	mode: "eval"
				# 	eval: "new_score"
				# 	map:
				# 		actions: "this.actions"

				# 		rules: (settings) ->
				# 			if not settings.score_rules? or not Array.isArray settings.score_rules
				# 				settings.score_rules = []
				# 			return 'rules = ' + JSON.stringify settings.score_rules

				# 		changes: (settings) ->
				# 			if not settings.score_intervals? or not Array.isArray settings.score_intervals
				# 				settings.score_intervals = []
				# 			return 'changes = ' + JSON.stringify settings.score_intervals

				# 		new_score: (settings) ->
				# 			fn = (actions, rules, changes) ->

				# 				if not actions?.map?
				# 					actions = []

				# 				# between earliest date and "now"
				# 				start_date = Infinity
				# 				actions = actions
				# 					.filter (action) ->
				# 						action and action._time? and action._value?
				# 					.map (action) ->
				# 						action._time = new Date action._time
				# 						start_date = Math.min start_date, action._time.getTime()
				# 						return action
				# 				# sort actions by date ascending
				# 					.sort (one, two) ->
				# 						return one._time.getTime() - two._time.getTime()

				# 				make_datestamp = (date) ->
				# 					pad = (v) -> (v/100).toFixed(2).slice(2)
				# 					date = new Date date
				# 					return [date.getFullYear(), pad(date.getMonth()), pad(date.getDate())].join('-')

				# 				eval_cond = (action, cond) ->
				# 					try
				# 						val = action._value[cond.field].toString()
				# 						switch cond.method
				# 							when 'eq' then return val is cond.value
				# 							when 'neq' then return val isnt cond.value
				# 							when 'start' then return val.indexOf(cond.value) is 0
				# 							when 'end' then return val.slice(0 - cond.value.length) is cond.value
				# 							when 'pattern' then return new RegExp(cond.value).test(val)
				# 					return false

				# 				scores = min: {}, full: {}, change: {}, current: 0
				# 				date = new Date(start_date).setHours(23, 59, 60, 0)
				# 				days = []
				# 				while actions.length > 0 and date < new Date().setHours(23, 59, 60, 999)
				# 					ts = make_datestamp date
				# 					while actions.length > 0 and actions[0]._time < date
				# 						action = actions.shift()
				# 						scores.current += rules.reduce ((value, rule) ->
				# 							pass = rule.conditions.every (cond) -> eval_cond action, cond
				# 							if not pass
				# 								return value
				# 							if rule?.score?.weight?
				# 								return value + parseInt rule.score.weight
				# 							if rule?.score?
				# 								return value + parseInt rule.score

				# 							console.log 'no score?', rule
				# 							return value

				# 						), 0
				# 						scores.min[ts] = scores.current
				# 					scores.full[ts] = scores.current
				# 					days.push scores.current
				# 					date += 86400 * 1000

				# 				# this one is required for the metric to work
				# 				# possibly make it a setting to change the tracked metric?
				# 				if (changes.filter((c) -> c.name is 'day').length) is 0
				# 					changes.push {name: 'day', interval: [1, 'day']}

				# 				changes.forEach (change) ->
				# 					key = change.name
				# 					n = change.interval[0] or 1
				# 					switch change.interval[1]
				# 						# "day" is implict/default
				# 						when 'week'  then n *= 7
				# 						when 'month' then n *= 30
				# 						when 'year'  then n *= 365

				# 					val = scores.current - (days[days.length - n - 1] ? 0)
				# 					scores.change[key] = absolute: 0, percentage: 0
				# 					if scores.current > 0
				# 						scores.change[key] = absolute: val, percentage: -100 + Math.ceil 100 * (scores.current + val) / scores.current

				# 				ret = scores.min
				# 				ret.change = scores.change
				# 				ret.today = scores.current
				# 				return ret

				# 			return "(#{fn.toString()})(actions, rules, changes)"
		
		user:
			fields:

				# generate a probably-unique ID which can be used in email link params
				# possibilities are 62^N with 50% collision chances at following N: 62^0.5N
				#  4 = 3844
				#  5 = 30,267
				#  6 = 238 thousand
				#  8 = 14 million
				# 10 = 916 million
				eid:
					mode: 'eval'
					eval: "encrypt('md5', this._id, 'base64').replace(/[^a-z0-9]+/ig, '').slice(0, 6)"

				info:
					mode: 'extend'
				devices:
					mode: 'relation'
					fact_type: "device"
					has: "many"
					query:
						user_id: "fact._id"

				sids:
					mode: "push_unique"

				dids:
					mode: "push_unique"

				score:
					mode: "eval"
					eval: "new_score"
					map:

						# # this version is slightly quicker but is also much longer. consider
						# # creating a function which allows something like get_field('devices.sessions', 'actions')
						#device_ids: "relation.get_field('devices', '_id')",
						#pipe: "pipe = [{$match: {did: {$in: device_ids}}}, {$unwind: '$actions'}, {$group: {_id: null, actions: {$push: '$actions'}}}]",
						#agg: "relation._getCollection('session').aggregate(pipe)",
						#actions: "agg[0].actions",

						actions: "this.devices.sessions.actions"

						rules: (settings) ->
							if not settings.score_rules? or not Array.isArray settings.score_rules
								settings.score_rules = []
							return 'rules = ' + JSON.stringify settings.score_rules

						changes: (settings) ->
							if not settings.score_intervals? or not Array.isArray settings.score_intervals
								settings.score_intervals = []
							return 'changes = ' + JSON.stringify settings.score_intervals

						new_score: (settings) ->
							fn = (actions, rules, changes) ->

								if not actions?.map?
									actions = []

								# between earliest date and "now"
								start_date = Infinity
								actions = actions
									.filter (action) ->
										action and action._time? and action._value?
									.map (action) ->
										action._time = new Date action._time
										start_date = Math.min start_date, action._time.getTime()
										return action
								# sort actions by date ascending
									.sort (one, two) ->
										return one._time.getTime() - two._time.getTime()

								make_datestamp = (date) ->
									pad = (v) -> (v/100).toFixed(2).slice(2)
									date = new Date date
									return [date.getFullYear(), pad(date.getMonth() + 1), pad(date.getDate())].join('-')

								eval_cond = (action, cond) ->
									if not Array.isArray cond.value
										cond.value = [cond.value]

									try
										val = action._value
										path = cond.field.split('.')
										for item in path
											if item.charAt(0) is '$'
												val = url val, item.substr 1
												if not val?
													return false
											else if val[item]?
												val = val[item]
											else
												return false

										val = val.toString()

										if cond.method is 'exists'
											return true

										return cond.value.some (value) ->
											try
												switch cond.method
													when 'eq' then return val is value
													when 'neq' then return val isnt value
													when 'start' then return val.indexOf(value) is 0
													when 'contains' then return val.indexOf(value) >= 0
													when 'end' then return val.slice(0 - value.length) is value
													when 'pattern' then return new RegExp(value).test(val)
											return false

									return false

								scores = min: {}, full: {}, change: {}, current: 0
								date = new Date(start_date).setHours(23, 59, 60, 0)
								days = []
								while actions.length > 0 and date < new Date().setHours(23, 59, 60, 999)
									ts = make_datestamp date
									while actions.length > 0 and actions[0]._time < date
										action = actions.shift()
										scores.current += rules.reduce ((value, rule) ->
											pass = rule.conditions.every (cond) -> eval_cond action, cond
											if not pass
												return value
											if rule?.score?.weight?
												return value + parseInt rule.score.weight
											if rule?.score?
												return value + parseInt rule.score

											console.log 'no score?', rule
											return value

										), 0
										scores.min[ts] = scores.current
									scores.full[ts] = scores.current
									days.push scores.current
									date += 86400 * 1000

								# this one is required for the metric to work
								# possibly make it a setting to change the tracked metric?
								if (changes.filter((c) -> c.name is 'day').length) is 0
									changes.push {name: 'day', interval: [1, 'day']}

								changes.forEach (change) ->
									key = change.name
									n = change.interval[0] or 1
									switch change.interval[1]
										# "day" is implict/default
										when 'week'  then n *= 7
										when 'month' then n *= 30
										when 'year'  then n *= 365

									val = scores.current - (days[days.length - n - 1] ? 0)
									scores.change[key] = absolute: 0, percentage: 0
									if scores.current > 0
										scores.change[key] = absolute: val, percentage: -100 + Math.ceil 100 * (scores.current + val) / scores.current

								ret = scores.min
								ret.change = scores.change
								ret.today = scores.current
								return ret

							return "(#{fn.toString()})(actions, rules, changes)"
		
	}
	

	tracking_goals:
		page:
			triggers: [{type: 'load'}]
			events: [
				{
					type: "track",
					data:
						device: "=T.helpers.device()",
						action:
							type: "page",
							url: "=location.href",
							title: "=document.title",
							referrer: "=document.referrer"
				}
			]
		form:
			triggers: [{type: 'submit', element: 'form'}]
			events: [
				{
					type: 'track'
					data:
						action:
							type: 'form'
							form_id: '=element.id || location.pathname'
							data: '=data'
				}
			]

		utm_identify:
				triggers: [{type: "load"}],
				conditions: [
					{
						type: "eval",
						eval: "window.location.search.match(/utm_email=([^&]+@[^&]+)(&|$)/)"
					}
				],
				events: [
					{
						type: "identify",
						data:
							id: "=window.location.search.match(/utm_email=([^&]+?)(&|$)/)[1]"
							email: "=window.location.search.match(/utm_email=([^&]+?)(&|$)/)[1]"
							name: null
					}
				]

	tracking_software: []
	
}

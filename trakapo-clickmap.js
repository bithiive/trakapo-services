({
  name: "Trakapo Clickmap",
  version: "0.0.2",
  description: "Track which links are clicked on a page",
  icon: 'mouse',
  tracking_goals: {
    links: {
      triggers: [
        {
          type: 'click',
          element: 'a[href]'
        }
      ],
      events: [
        {
          type: "link",
          atomic: false,
          data: {
            url: "=location.href",
            source: "=T.helpers.selector(element)",
            target: "=element.href"
          }
        }
      ]
    }
  },
  tracking_scripts: {
    clicktracking: function() {
      var fn;
      fn = function() {
        return T.helpers.selector = function(el) {
          var element, i, idx, names, sibling, siblings, xpath;
          xpath = '';
          while (element && element.nodeType === 1) {
            element = element.parentNode;
            siblings = Sizzle('> *', element.parentNode);
            idx = '';
            for (i in siblings) {
              sibling = siblings[i];
              if (sibling === element) {
                idx = '[' + (parseInt(i) + 1) + ']';
              }
            }
            xpath = '/' + element.tagName.toLowerCase() + idx + xpath;
          }
          return xpath;
          names = [];
          while (el.parentNode) {
            if (el.id) {
              names.unshift('#' + el.id);
              break;
            } else {
              if (el === el.ownerDocument.documentElement) {
                names.unshift(el.tagName);
              } else {
                for (var c=1, e=el; e.previousElementSibling; e = e.previousElementSibling,c++)
                                    names.unshift(el.tagName+":nth-child("+c+")");
                                ;
              }
              el = el.parentNode;
            }
          }
          return names.join(' > ');
        };
      };
      return '(' + fn.toString() + ')()';
    }
  }
});

({
  "name": "Twitter",
  "description": "Twitter social service",
  "version": "0.1.1",
  "public": false,
  "info_mappings": [
    {
      mapping_id: "twitter_keyword_count",
      info_type: "twitter_keyword",
      fact_type: "twitter_keyword",
      fact_identifier: "info.keyword.replace(/[^a-z0-9_]+/gi, '_')",
      fields: {
        keyword: "info.keyword",
        count: 1
      }
    }, {
      mapping_id: "twitter_page",
      info_type: "twitter_page",
      fact_type: "twitter_page",
      fact_identifier: "info._id",
      fields: {
        _created: 'new Date(info.user_since)',
        handle: "info.screen_name",
        name: "info.name",
        location: "info.location",
        followers: "info.followers",
        image: "info.image",
        summarize: "info.summarize == false ? false : true"
      }
    }, {
      mapping_id: "twitter_status",
      info_type: "twitter_status",
      fact_type: "twitter_status",
      fact_identifier: "info._id",
      fields: [
        {
          field: "_created",
          value: "new Date(info.timestamp)"
        }, {
          field: "text",
          value: "info.text"
        }, {
          field: "page_id",
          value: 'info.account'
        }, {
          field: "user_id",
          value: 'info.account'
        }, {
          field: "base_reach",
          value: "info.user.reach"
        }, {
          field: "like_count_poll",
          value: "info.like_count"
        }, {
          field: "share_count_poll",
          value: "info.share_count"
        }
      ]
    }, {
      mapping_id: "twitter_follow_interaction",
      info_type: "twitter_follow",
      fact_type: "twitter_interaction",
      fact_identifier: "info.account + '-' + info.type + '-' + info._id",
      fields: {
        _created: 'new Date(info.timestamp)',
        type: 'info.type',
        page_id: 'info.account',
        user_id: 'info._id'
      }
    }, {
      mapping_id: "twitter_follow_page",
      info_type: "twitter_follow",
      fact_type: "twitter_page",
      fact_identifier: "info._id",
      fields: {
        handle: "info.screen_name",
        name: "info.name",
        location: "info.location",
        followers: "info.followers",
        lang: 'info.lang',
        image: "info.image"
      }
    }, {
      mapping_id: "twitter_mention_interaction",
      info_type: "twitter_mention",
      fact_type: "twitter_interaction",
      fact_identifier: "info.account + '-' + info.type + '-' + info.user._id + '-' + info._id",
      fields: {
        _created: 'new Date(info.timestamp)',
        type: 'info.type',
        text: 'info.text',
        created: 'new Date()',
        page_id: 'info.account',
        user_id: 'info.user._id'
      }
    }, {
      mapping_id: "twitter_mention_page",
      info_type: "twitter_mention",
      fact_type: "twitter_page",
      fact_identifier: "info.user._id",
      fields: {
        handle: "info.user.screen_name",
        name: "info.user.name",
        location: "info.user.location",
        followers: "info.user.followers",
        lang: 'info.user.lang',
        image: "info.user.image"
      }
    }, {
      mapping_id: "twitter_reply_interaction",
      info_type: "twitter_reply",
      fact_type: "twitter_interaction",
      fact_identifier: "info.account + '-' + info.type + '-' + info.user._id + '-' + info._id",
      fields: {
        type: 'info.type',
        text: 'info.text',
        _created: 'new Date(info.timestamp)',
        page_id: 'info.account',
        user_id: 'info.user._id',
        status_id: 'info.parent_id'
      }
    }, {
      mapping_id: "twitter_reply_status",
      info_type: "twitter_reply",
      fact_type: "twitter_status",
      fact_identifier: "info.parent_id",
      fields: {}
    }, {
      mapping_id: "reply_page",
      info_type: "reply",
      fact_type: "twitter_page",
      fact_identifier: "info.user._id",
      fields: {
        handle: "info.user.screen_name",
        name: "info.user.name",
        location: "info.user.location",
        followers: "info.user.followers",
        lang: 'info.user.lang',
        image: "info.user.image"
      }
    }, {
      mapping_id: "twitter_retweet_interaction",
      info_type: "twitter_retweet",
      fact_type: "twitter_interaction",
      fact_identifier: "info.account + '-' + info.type + '-' + info.user._id + '-' + info._id",
      fields: {
        _created: 'new Date(info.timestamp)',
        type: 'info.type',
        page_id: 'info.account',
        user_id: 'info.user._id',
        status_id: 'info._id',
        reach: 'info.user.followers'
      }
    }, {
      mapping_id: "twitter_retweet_status",
      info_type: "twitter_retweet",
      fact_type: "twitter_status",
      fact_identifier: "info._id",
      fields: {}
    }, {
      mapping_id: "twitter_retweet_page",
      info_type: "twitter_retweet",
      fact_type: "twitter_page",
      fact_identifier: "info.user._id",
      fields: {
        handle: "info.user.screen_name",
        name: "info.user.name",
        location: "info.user.location",
        followers: "info.user.followers",
        lang: 'info.user.lang',
        image: "info.user.image"
      }
    }, {
      mapping_id: "twitter_favorite_interaction",
      info_type: "twitter_favorite",
      fact_type: "twitter_interaction",
      fact_identifier: "info.account + '-' + info.type + '-' + info.user._id + '-' + info._id",
      fields: {
        _created: 'new Date(info.timestamp)',
        type: 'info.type',
        page_id: 'info.account',
        user_id: 'info.user._id',
        status_id: 'info._id'
      }
    }, {
      mapping_id: "twitter_favorite_status",
      info_type: "twitter_favorite",
      fact_type: "twitter_status",
      fact_identifier: "info._id",
      fields: {}
    }, {
      mapping_id: "twitter_favorite_page",
      info_type: "twitter_favorite",
      fact_type: "twitter_page",
      fact_identifier: "info.user._id",
      fields: {
        handle: "info.user.screen_name",
        name: "info.user.name",
        location: "info.user.location",
        followers: "info.user.followers",
        lang: 'info.user.lang',
        image: "info.user.image"
      }
    }, {
      mapping_id: "twitter_unfavorite_interaction",
      info_type: "twitter_unfavorite",
      fact_type: "twitter_interaction",
      fact_identifier: "info.account + '-' + info.type + '-' + info.user._id + '-' + info._id",
      fields: {
        _created: 'new Date(info.timestamp)',
        type: 'info.type',
        page: 'info.account',
        user: 'info.user._id'
      }
    }, {
      mapping_id: "twitter_unfavorite_status",
      info_type: "twitter_unfavorite",
      fact_type: "twitter_status",
      fact_identifier: "info._id",
      fields: {}
    }, {
      mapping_id: "twitter_unfavorite_page",
      info_type: "twitter_unfavorite",
      fact_type: "twitter_page",
      fact_identifier: "info.user._id",
      fields: {
        handle: "info.user.screen_name",
        name: "info.user.name",
        location: "info.user.location",
        followers: "info.user.followers",
        lang: 'info.user.lang',
        image: "info.user.image"
      }
    }
  ],
  "metrics": [
    {
      metric_id: "type_counts",
      fact_type: "twitter_interaction",
      by_created: true,
      group: ['page_id', 'type'],
      fields: {
        count: {
          mode: 'count'
        }
      }
    }, {
      metric_id: "summary",
      fact_type: "twitter_page",
      filter: {
        summarize: true
      },
      group: ['_id'],
      fields: {
        followers: {
          mode: 'max',
          input: 'followers'
        },
        followers_start: {
          mode: 'min',
          input: 'followers'
        },
        engagement_ratio: {
          mode: 'avg',
          input: 'engagement_ratio'
        },
        count_mention: {
          mode: 'sum',
          input: 'count.mention'
        },
        count_reply: {
          mode: 'sum',
          input: 'count.reply'
        },
        count_favorite: {
          mode: 'sum',
          input: 'count.favorite'
        },
        count_retweet: {
          mode: 'sum',
          input: 'count.retweet'
        },
        count_follow: {
          mode: 'sum',
          input: 'count.follow'
        }
      }
    }, {
      metric_id: 'summary',
      fact_type: 'twitter_status',
      group: 'page_id',
      by_created: true,
      backfill_count: 3,
      filter: {
        reach: {
          $gte: 0
        }
      },
      fields: {
        count: {
          mode: 'count'
        },
        reach_sum: {
          mode: 'sum',
          input: 'reach'
        },
        reach_avg: {
          mode: 'avg',
          input: 'reach'
        },
        reply_sum: {
          mode: 'sum',
          input: 'reply_count'
        },
        reply_avg: {
          mode: 'avg',
          input: 'reply_count'
        },
        share_sum: {
          mode: 'sum',
          input: 'share_count'
        },
        share_avg: {
          mode: 'avg',
          input: 'share_count'
        },
        share_poll_sum: {
          mode: 'sum',
          input: 'share_count_poll'
        },
        share_poll_avg: {
          mode: 'avg',
          input: 'share_count_poll'
        },
        like_poll_sum: {
          mode: 'sum',
          input: 'like_count_poll'
        },
        like_poll_avg: {
          mode: 'avg',
          input: 'like_count_poll'
        }
      }
    }
  ],
  "fact_settings": {
    twitter_page: {
      fields: {
        statuses: {
          mode: 'relation',
          fact_type: 'twitter_status',
          has: 'many',
          query: {
            page_id: 'fact._id'
          }
        },
        favorites: {
          mode: 'relation',
          fact_type: 'twitter_interaction',
          has: 'many',
          query: {
            page_id: 'fact._id',
            type: '"favorite"',
            _created: 'x={$gt: new Date(new Date().setDate(1) - (Date.now() % 86400000))}'
          }
        },
        retweets: {
          mode: 'relation',
          fact_type: 'twitter_interaction',
          has: 'many',
          query: {
            page_id: 'fact._id',
            type: '"retweet"',
            _created: 'x={$gt: new Date(new Date().setDate(1) - (Date.now() % 86400000))}'
          }
        },
        replies: {
          mode: 'relation',
          fact_type: 'twitter_interaction',
          has: 'many',
          query: {
            page_id: 'fact._id',
            type: '"reply"',
            _created: 'x={$gt: new Date(new Date().setDate(1) - (Date.now() % 86400000))}'
          }
        },
        count: {
          mode: 'eval',
          map: {
            fvc: 'relation.count("favorites")',
            rtc: 'relation.count("retweets")',
            rec: 'relation.count("replies")'
          },
          "eval": 'x = {favorite: fvc, retweet: rtc, reply: rec}'
        },
        engagement: {
          mode: 'eval',
          condition: "this.summarize",
          map: {
            fvc: 'relation.count("favorites")',
            rtc: 'relation.count("retweets")',
            rec: 'relation.count("replies")'
          },
          "eval": '(fvc * 1.5) + (rtc * 0.5) + rec'
        },
        engagement_ratio: {
          mode: 'eval',
          condition: "this.summarize",
          map: {
            fvc: 'relation.count("favorites")',
            rtc: 'relation.count("retweets")',
            rec: 'relation.count("replies")',
            engagement: '(fvc * 1.5) + (rtc * 0.5) + rec',
            ratio: 'parseFloat(this.followers / engagement)'
          },
          "eval": 'parseFloat(ratio.toFixed(4))'
        }
      }
    },
    twitter_status: {
      fields: {
        page: {
          mode: 'relation',
          fact_type: 'twitter_page',
          has: 'one',
          query: {
            _id: 'fact.page_id'
          }
        },
        shares: {
          mode: 'relation',
          fact_type: 'twitter_interaction',
          has: 'many',
          query: {
            type: '"retweet"',
            status_id: 'fact._id'
          }
        },
        share_count: {
          mode: 'eval',
          "eval": 'relation.count("shares")'
        },
        reach: {
          mode: 'eval',
          map: {
            share_reach: 'relation.sum_field("shares", "reach")',
            text: ['this.text', ''],
            base: ['this.base_reach', 0],
            page_reach: ['(text.toString().charAt(0) == "@" ? 1 : (base || this.page.followers))', 0]
          },
          "eval": 'Number(share_reach) + Number(page_reach)'
        },
        likes: {
          mode: 'relation',
          fact_type: 'twitter_interaction',
          has: 'many',
          query: {
            type: '"favorite"',
            status_id: 'fact._id'
          }
        },
        like_count: {
          mode: 'eval',
          "eval": 'relation.count("likes")'
        },
        replies: {
          mode: 'relation',
          fact_type: 'twitter_interaction',
          has: 'many',
          query: {
            type: '"reply"',
            status_id: 'fact._id'
          }
        },
        reply_count: {
          mode: 'eval',
          "eval": 'relation.count("replies")'
        },
        interaction_count: {
          mode: 'eval',
          "eval": 'relation.count("likes", "replies", "shares")'
        }
      }
    },
    twitter_interaction: {
      fields: {
        page: {
          mode: 'relation',
          fact_type: 'twitter_page',
          has: 'one',
          query: {
            _id: 'fact.page_id'
          }
        },
        user: {
          mode: 'relation',
          fact_type: 'twitter_page',
          has: 'one',
          query: {
            _id: 'fact.user_id'
          }
        },
        status: {
          mode: 'relation',
          fact_type: 'twitter_status',
          has: 'one',
          query: {
            _id: 'fact.status_id'
          }
        }
      }
    },
    twitter_keyword: {
      fields: {
        count: {
          mode: 'inc'
        }
      }
    }
  }
});

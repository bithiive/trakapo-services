user:
	fields:

		# generate a probably-unique ID which can be used in email link params
		# possibilities are 62^N with 50% collision chances at following N: 62^0.5N
		#  4 = 3844
		#  5 = 30,267
		#  6 = 238 thousand
		#  8 = 14 million
		# 10 = 916 million
		eid:
			mode: 'eval'
			eval: "encrypt('md5', this._id, 'base64').replace(/[^a-z0-9]+/ig, '').slice(0, 6)"

		info:
			mode: 'extend'
		devices:
			mode: 'relation'
			fact_type: "device"
			has: "many"
			query:
				user_id: "fact._id"

		sids:
			mode: "push_unique"

		dids:
			mode: "push_unique"

		score:
			mode: "eval"
			eval: "new_score"
			map:

				# # this version is slightly quicker but is also much longer. consider
				# # creating a function which allows something like get_field('devices.sessions', 'actions')
				#device_ids: "relation.get_field('devices', '_id')",
				#pipe: "pipe = [{$match: {did: {$in: device_ids}}}, {$unwind: '$actions'}, {$group: {_id: null, actions: {$push: '$actions'}}}]",
				#agg: "relation._getCollection('session').aggregate(pipe)",
				#actions: "agg[0].actions",

				actions: "this.devices.sessions.actions"

				rules: (settings) ->
					if not settings.score_rules? or not Array.isArray settings.score_rules
						settings.score_rules = []
					return 'rules = ' + JSON.stringify settings.score_rules

				changes: (settings) ->
					if not settings.score_intervals? or not Array.isArray settings.score_intervals
						settings.score_intervals = []
					return 'changes = ' + JSON.stringify settings.score_intervals

				new_score: (settings) ->
					fn = (actions, rules, changes) ->

						if not actions?.map?
							actions = []

						# between earliest date and "now"
						start_date = Infinity
						actions = actions
							.filter (action) ->
								action and action._time? and action._value?
							.map (action) ->
								action._time = new Date action._time
								start_date = Math.min start_date, action._time.getTime()
								return action
						# sort actions by date ascending
							.sort (one, two) ->
								return one._time.getTime() - two._time.getTime()

						make_datestamp = (date) ->
							pad = (v) -> (v/100).toFixed(2).slice(2)
							date = new Date date
							return [date.getFullYear(), pad(date.getMonth() + 1), pad(date.getDate())].join('-')

						eval_cond = (action, cond) ->
							if not Array.isArray cond.value
								cond.value = [cond.value]

							try
								val = action._value
								path = cond.field.split('.')
								for item in path
									if item.charAt(0) is '$'
										val = url val, item.substr 1
										if not val?
											return false
									else if val[item]?
										val = val[item]
									else
										return false

								val = val.toString()

								if cond.method is 'exists'
									return true

								return cond.value.some (value) ->
									try
										switch cond.method
											when 'eq' then return val is value
											when 'neq' then return val isnt value
											when 'start' then return val.indexOf(value) is 0
											when 'contains' then return val.indexOf(value) >= 0
											when 'end' then return val.slice(0 - value.length) is value
											when 'pattern' then return new RegExp(value).test(val)
									return false

							return false

						scores = min: {}, full: {}, change: {}, current: 0
						date = new Date(start_date).setHours(23, 59, 60, 0)
						days = []
						while actions.length > 0 and date < new Date().setHours(23, 59, 60, 999)
							ts = make_datestamp date
							while actions.length > 0 and actions[0]._time < date
								action = actions.shift()
								scores.current += rules.reduce ((value, rule) ->
									pass = rule.conditions.every (cond) -> eval_cond action, cond
									if not pass
										return value
									if rule?.score?.weight?
										return value + parseInt rule.score.weight
									if rule?.score?
										return value + parseInt rule.score

									console.log 'no score?', rule
									return value

								), 0
								scores.min[ts] = scores.current
							scores.full[ts] = scores.current
							days.push scores.current
							date += 86400 * 1000

						# this one is required for the metric to work
						# possibly make it a setting to change the tracked metric?
						if (changes.filter((c) -> c.name is 'day').length) is 0
							changes.push {name: 'day', interval: [1, 'day']}

						changes.forEach (change) ->
							key = change.name
							n = change.interval[0] or 1
							switch change.interval[1]
								# "day" is implict/default
								when 'week'  then n *= 7
								when 'month' then n *= 30
								when 'year'  then n *= 365

							val = scores.current - (days[days.length - n - 1] ? 0)
							scores.change[key] = absolute: 0, percentage: 0
							if scores.current > 0
								scores.change[key] = absolute: val, percentage: -100 + Math.ceil 100 * (scores.current + val) / scores.current

						ret = scores.min
						ret.change = scores.change
						ret.today = scores.current
						return ret

					return "(#{fn.toString()})(actions, rules, changes)"

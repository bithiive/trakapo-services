{
	mapping_id: "fields"
	fact_type: "fields"
	fact_identifier: "info.type"
	info_type: "fields"
	fields:
		field_paths: "info.field_paths"
		field_counts: "info.field_paths"
		field_types: "info.field_types"
}

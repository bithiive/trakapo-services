# context:
#     action_id: (sid, type, action) ->
#         return sid + '-' + encrypt('md5', Date.now() + type + JSON.stringify(action)).slice(0, 8)

info_mappings: [
	#!faction.mappings_track.coffee
	#!faction.mappings_identify.coffee
	#!faction.mappings_fields.coffee
	#!faction.mappings_errors.coffee
],
info_schemas:
	identify:
		fields: [
			{
				field: 'id'
				required: true
				cast: 'string | lowercase'
			}
		]
	track:
		fields: [
			{field: 'action.type', type: 'string', required: true}
			{field: 'sid', cast: 'string', required: true}
			{field: 'did', cast: 'string', required: true}
			{field: 'user_id', cast: 'string | lowercase'}
			{field: 'id', cast: 'string | lowercase'}
		]

metrics: [
	#!faction.metrics.coffee
],
fact_settings: {
	#!faction.fact_settings_device.coffee
	#!faction.fact_settings_referrer.coffee
	#!faction.fact_settings_session.coffee
	#!faction.fact_settings_user.coffee
}

{
	mapping_id: "device"
	fact_type: "device"
	fact_identifier: "info.did"
	info_type: "track"
	fields:
		device: "info.device"
		user_id: "info.id || ('undefined' !== typeof fact ? fact.user_id : null)"
		sids: "info.sid"
}
{
	mapping_id: "user"
	fact_type: "user"
	fact_identifier: "info.id"
	info_type: "track"
	fields:
		last_sid: "info.sid"
		last_did: "info.did"
		sids: "info.sid"
		dids: "info.did"
}
{
	mapping_id: "sessions"
	fact_type: "session"
	fact_identifier: "info.sid"
	info_type: "track"
	fields:
		did: "info.did"
		dids: "info.did"
		actions: "info.action"
}

# # direct action...
# {
#     mapping_id: "action",
#     info_type: "identify",
#     fact_type: "action",
#     fact_identifier: "action_id(info.sid, info.action.type, info.action)"
#     fields: [
#         { field: 'sid', value: 'info.sid' }
#         { field: 'did', value: 'info.did' }
#         { field: 'user_id', value: 'info.id', condition: 'info.id' }
#         { field: 'type', value: 'info.action.type || info.type' }
#         { field: 'action', value: 'info.action' }
#     ]
# }

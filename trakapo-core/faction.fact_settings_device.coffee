device:
	fields:
		location:
			mode: 'eval'
			# this key may eventually be a faction key, for now it doesnt matter
			condition: "'undefined' == typeof fact.location"
			eval: 'fact.location ? fact.location : http("http://freegeoip.net/json/" + fact.device.ip)'
		user:
			mode: 'relation'
			fact_type: "user"
			has: "one"
			query:
				_id: "fact.user_id"
		sessions:
			mode: 'relation'
			fact_type: "session"
			has: "many"
			query:
				did: "fact._id"

		sids:
			mode: "push_unique"


	sections: [
		{
			title: "Users",
			path: "/users"
		},
		{
			title: "Pages",
			path: "/pages"
		}
	],
	pages: [
		{
			url: "/users",
			order:
				before: "*"

			callback: ->
				@widgets.table =
					size: 'full'
					type: 'table'
					title: 'Users'
					data: '/facts/user'

					pageable: true
					paging: page: 0, limit: 50

					sortable: true,
					sort: "score.today": 'desc'

					filterable: true
					filter: {}

					columns: [
						{
							title: 'ID',
							field: '_id',
						},
						{
							title: 'Email',
							field: 'email'
						},
						{
							title: 'Score',
							field: 'score.today'
							filter: false
						}
						# todo: score graph
					]
				@done()
		},

		{
			url: "/users/:id",
			order:
				before: "*"
			context:
				getOpts:
					map:
						devices: "this.devices.device",
						sessions: "this.devices.sessions",
						baskets: "this.devices.sessions.basket",
						actionCount: "this.devices.sessions.actions.length",
						score: "this.score",
						email: "this.email",
						name: "this.name",
						id: "this._id"

			callback: ->
				@faction.fact.get 'user', @context.params.id, {}, (err, user) =>
					if err then return @error err
					if not user then return @error.notFound 'user'

					@context.user = user
					@context.fns = {}

					@context.fns.duration = duration = (list) ->
						values = (item._time or item._updated for item in list).filter(Boolean)
						min = Math.min.apply Math, values
						max = Math.max.apply Math, values
						return max - min

					@context.fns.sortTime = sortTime = (one, two) ->
						return (one._time or one._updated) - (two._time or two._updated)

					ip_lookups = {}
					for device in user.devices or []
						do (device) =>
							device.location = {}
							if ip = device.ip
								ip_lookups[ip] ?= @$http.get '//freegeoip.net/json/' + ip
								ip_lookups[ip].then (result) =>
									if result.data
										result.data.country_name or= 'Unknown'
										result.data.city or= 'Unknown'
										device.location = result.data

										@widgets.user_map.points.push {
											latitude: result.data.latitude
											longitude: result.data.longitude
											label: result.data.city
										}

					user.sessions.sort sortTime
					for session in user.sessions or []
						session.actions ?= []
						session.duration = duration session.actions
						session.actions.sort sortTime

					@widgets.user_personal =
						size: 'half'
						title: 'Personal'
						type: 'info'
						items: [
							{name: 'ID', value: user._id},
							{name: 'Email', value: user.email}
							{name: 'Name', value: user.name}
						]

					@widgets.user_summary =
						size: 'half'
						title: 'Summary'
						type: 'info'
						items: [
							{name: 'Total visits', value: user.sessions.length},
							{name: 'Score', value: user.score.today ? 0},
							{name: 'Total activity', value: user.actions.length},
							{name: 'Total duration', value: duration user.actions}
						]

					@widgets.user_devices =
						size: 'half'
						title: 'Device info'
						type: 'info'
						items: [
							{name: 'Browser', value: 'TODO!'}
							{name: 'Screen', value: 'TODO!'}
							{name: 'Country', value: 'TODO!'}
							{name: 'City', value: 'TODO!'}
						]
					#todo: location map
					@widgets.user_map =
						size: 'half'
						title: 'Location'
						type: 'map'
						points: []

					# todo: sessions!
					@done()
		}
	]

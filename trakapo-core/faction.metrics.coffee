# top users by how much there score changed.
# this NEEDS the "day" range to exist, so that's forced...
{
	"fact_type": "user",
	"metric_id": "most_improved",
	"project": {
		"score.today": 1 # include this, to reduce effort
	},
	"sort": "-score.change.day.absolute",
	"limit": 50
}

# number of updated sessions today
{
	"fact_type": "session",
	"metric_id": "unique_visitors",
	"fields": {
		"count": {
			"mode": "count"
		}
	}
}

# number of actions produced
{
	"fact_type": "session",
	"metric_id": "action_counts",
	"unwind": "actions",
	"filter": {
		"actions._time": "$bucket"
	},
	"group": "actions._value.type",
	"fields": {
		"count": {
			"mode": "count"
		}
	}
}

# top-viewed pages this day
{
	"fact_type": "session",
	"metric_id": "popular_pages",
	"unwind": "actions",
	"filter": {
		"actions._value.type": "page",
		"actions._time": "$bucket"
	}
	"group": "actions._value.url",
	"fields": {
		"count": {
			"mode": "count"
		}
	},
	"sort": "-count",
	"limit": 50
}

# top referrering domains over time
{
	"fact_type": "session",
	"metric_id": "referrer_domains",
	"filter": {
		"referrer_domain": {
			"$exists": 1
		}
	},
	"group": "referrer_domain",
	"fields": {
		"count": {
			"mode": "count"
		}
	},
	"sort": "-count",
	"limit": 50
}

# top referring urls domains over time
{
	"fact_type": "session",
	"metric_id": "referrer_urls",
	"group": "referrer",
	"fields": {
		"count": {
			"mode": "count"
		}
	},
	"sort": "-count",
	"limit": 50
}

# top countries+cities
{
	"fact_type": "device",
	"metric_id": "top_countries",
	"filter": {
		"location.country_name": {
			"$exists": true
		}
	},
	"group": "location.country_name",
	"fields": {
		"count": {
			"mode": "count"
		}
	},
	"sort": "-count",
	"limit": 50
}
{
	"fact_type": "device",
	"metric_id": "top_cities",
	"filter": {
		"location.city": {
			"$exists": true
		}
	},
	"group": "location.city",
	"fields": {
		"count": {
			"mode": "count"
		},
		"country": {
			"mode": "first",
			"input": "location.country_name"
		}
	},
	"sort": "-count",
	"limit": 50
}

# pageview stats
{
	"fact_type": "session",
	"metric_id": "pages",
	"unwind": "actions",
	"filter": {
		"actions._value.type": "page",
		"actions._time": "$bucket"
	},
	"group": "actions._value.url",
	"fields": {
		"count": {
			"mode": "count"
		}
	},
	"sort": "-count"
}

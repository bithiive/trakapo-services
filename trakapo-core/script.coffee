tracking_goals:
	page:
		triggers: [{type: 'load'}]
		events: [
			{
				type: "track",
				data:
					device: "=T.helpers.device()",
					action:
						type: "page",
						url: "=location.href",
						title: "=document.title",
						referrer: "=document.referrer"
			}
		]
	form:
		triggers: [{type: 'submit', element: 'form'}]
		events: [
			{
				type: 'track'
				data:
					action:
						type: 'form'
						form_id: '=element.id || location.pathname'
						data: '=data'
			}
		]

	utm_identify:
			triggers: [{type: "load"}],
			conditions: [
				{
					type: "eval",
					eval: "window.location.search.match(/utm_email=([^&]+@[^&]+)(&|$)/)"
				}
			],
			events: [
				{
					type: "identify",
					data:
						id: "=window.location.search.match(/utm_email=([^&]+?)(&|$)/)[1]"
						email: "=window.location.search.match(/utm_email=([^&]+?)(&|$)/)[1]"
						name: null
				}
			]

tracking_software: []

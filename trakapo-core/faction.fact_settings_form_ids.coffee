form_ids:
	fields:
		count:
			mode: "inc"
		count_day:
			mode: "inc"
			grouping: "day"
		count_week:
			mode: "inc"
			grouping: "week"

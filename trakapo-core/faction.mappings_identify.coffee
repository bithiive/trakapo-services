{
	mapping_id: "device"
	fact_type: "device"
	fact_identifier: "info.did"
	info_type: "identify"
	fields:
		user_id: "info.id"
		sids: "info.sid"
}
{
	mapping_id: "user"
	fact_type: "user"
	fact_identifier: "info.id"
	info_type: "identify"
	fields:
		email: "info.email || fact.email"
		name: "info.name || fact.name"
		info: "info.info"
}
{
	mapping_id: "session"
	fact_type: "session"
	fact_identifier: "info.sid"
	info_type: "identify"
	conditions: ["fact.user_id != info.id"]
	fields: {
		user_id: "info.id",
		actions: {
			type: '"identify"'
			id: 'info.id'
			email: 'info.email'
			name: 'info.name'
			info: "info.info"
		}
	}
}

# # direct action...
# {
#     mapping_id: "action",
#     info_type: "identify",
#     fact_type: "action",
#     fact_identifier: "action_id(info.sid, 'identify', info)"
#     fields:
#         sid: 'info.sid'
#         did: 'info.did'
#         user_id: 'info.id'
#         type: 'identify'
#         action: 'x = {id: info.id, email: info.email, name: info.name, info: info.info}'
# }

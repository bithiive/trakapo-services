eval_cond = (action, cond) ->
	if not Array.isArray cond.value
		cond.value = [cond.value]

	try
		val = action._value
		path = cond.field.split('.')
		for item in path
			if val[item]?
				val = val[item]
			else
				return false

		val = val.toString()

		if cond.method is 'exists'
			return true

		return cond.value.some (value) ->
			try
				switch cond.method
					when 'eq' then return val is value
					when 'neq' then return val isnt value
					when 'start' then return val.indexOf(value) is 0
					when 'contains' then return val.indexOf(value) >= 0
					when 'end' then return val.slice(0 - value.length) is value
					when 'pattern' then return new RegExp(value).test(val)
			return false

	return false

console.log 'eq single value no path', true is eval_cond {_value: {name: "Richard"}}, {value: "Richard", method: "eq", field: "name"}
console.log 'eq multi value no path', true is eval_cond {_value: {name: "Richard"}}, {value: ["Sam", "Richard", "John"], method: "eq", field: "name"}

console.log 'neq+', true is eval_cond {_value: {name: "Richard"}}, {value: "John", method: "neq", field: "name"}
console.log 'neq-', false is eval_cond {_value: {name: "Richard"}}, {value: "Richard", method: "neq", field: "name"}

console.log 'start+', true is eval_cond {_value: {name: "Richard"}}, {value: "Rich", method: "start", field: "name"}
console.log 'start-', false is eval_cond {_value: {name: "Richard"}}, {value: "chard", method: "start", field: "name"}

console.log 'end+', true is eval_cond {_value: {name: "Richard"}}, {value: "chard", method: "end", field: "name"}
console.log 'end-', false is eval_cond {_value: {name: "Richard"}}, {value: "Rich", method: "end", field: "name"}

console.log 'contains+', true is eval_cond {_value: {name: "Richard"}}, {value: "ich", method: "contains", field: "name"}
console.log 'contains-', false is eval_cond {_value: {name: "Richard"}}, {value: "wibbly", method: "contains", field: "name"}

console.log 'pattern+', true is eval_cond {_value: {name: "Richard Lyon"}}, {value: "ard.+n", method: "pattern", field: "name"}

console.log 'deep path eq', true is eval_cond {_value: {name: {first: "Richard", last: "Lyon"}}}, {value: "Lyon", method: "eq", field: "name.last"}

console.log 'shallow exists', true is eval_cond {_value: {name: {first: "Richard", last: "Lyon"}}}, {method: "exists", field: "name"}
console.log 'deep exists', true is eval_cond {_value: {name: {first: "Richard", last: "Lyon"}}}, {method: "exists", field: "name.last"}

session:
	fields:
		device:
			mode: 'relation'
			fact_type: "device"
			has: "one"
			query:
				_id: "fact.did"
		user:
			mode: 'relation'
			fact_type: 'user'
			has: 'one'
			query:
				_id: 'fact.user_id'

		dids:
			mode: "push_unique"

		user_id:
			mode: 'eval'
			condition: '"string" != typeof fact.user_id'
			map:
				uid: "fact.device.user_id"
			eval: "'string' == typeof uid ? uid : null"

		actions:
			mode: "all"
		referrer:
			mode: "eval"
			condition: "'undefined' == typeof fact.referrer"
			eval: "fact.get('actions._value.referrer').shift()"
		referrer_domain:
			mode: 'eval'
			condition: "'undefined' == typeof fact.referrer_domain"
			eval: 'url(fact.referrer, "host")'
		query:
			mode: "eval"
			condition: "'undefined' == typeof fact.query"
			eval: "url(fact.get('actions._value.url').shift(), 'query')"


		source:
			mode: "eval"
			eval: (settings) ->
				fn = (action) ->
					try
						local = url action.url, 'host'
						query = url action.url, 'query'
						referrer = action.referrer and url action.referrer, 'host'

						# google CPC
						if query.gclid?
							return medium: 'cpc', source: 'google', campaign: query.gclid

						# some non-local referrer
						if referrer and referrer.length > 0 and referrer isnt local
							# a search referrer
							if engine = referrer.match /(google|bing|yahoo|ask|aol)\./
								return medium: 'search', source: engine[1]
							return medium: 'referrer', source: referrer

						# other UTM data
						if query?.utm_medium?
							return {
								medium: query.utm_medium
								source: query.utm_source
								campaign: query.utm_campaign
							}

						return medium: 'direct', source: action.url
					catch e
						return medium: 'error', error: e.message, stage: stage

				return "(#{fn.toString()})(fact.actions[0]._value)"

		# score:
		# 	mode: "eval"
		# 	eval: "new_score"
		# 	map:
		# 		actions: "this.actions"

		# 		rules: (settings) ->
		# 			if not settings.score_rules? or not Array.isArray settings.score_rules
		# 				settings.score_rules = []
		# 			return 'rules = ' + JSON.stringify settings.score_rules

		# 		changes: (settings) ->
		# 			if not settings.score_intervals? or not Array.isArray settings.score_intervals
		# 				settings.score_intervals = []
		# 			return 'changes = ' + JSON.stringify settings.score_intervals

		# 		new_score: (settings) ->
		# 			fn = (actions, rules, changes) ->

		# 				if not actions?.map?
		# 					actions = []

		# 				# between earliest date and "now"
		# 				start_date = Infinity
		# 				actions = actions
		# 					.filter (action) ->
		# 						action and action._time? and action._value?
		# 					.map (action) ->
		# 						action._time = new Date action._time
		# 						start_date = Math.min start_date, action._time.getTime()
		# 						return action
		# 				# sort actions by date ascending
		# 					.sort (one, two) ->
		# 						return one._time.getTime() - two._time.getTime()

		# 				make_datestamp = (date) ->
		# 					pad = (v) -> (v/100).toFixed(2).slice(2)
		# 					date = new Date date
		# 					return [date.getFullYear(), pad(date.getMonth()), pad(date.getDate())].join('-')

		# 				eval_cond = (action, cond) ->
		# 					try
		# 						val = action._value[cond.field].toString()
		# 						switch cond.method
		# 							when 'eq' then return val is cond.value
		# 							when 'neq' then return val isnt cond.value
		# 							when 'start' then return val.indexOf(cond.value) is 0
		# 							when 'end' then return val.slice(0 - cond.value.length) is cond.value
		# 							when 'pattern' then return new RegExp(cond.value).test(val)
		# 					return false

		# 				scores = min: {}, full: {}, change: {}, current: 0
		# 				date = new Date(start_date).setHours(23, 59, 60, 0)
		# 				days = []
		# 				while actions.length > 0 and date < new Date().setHours(23, 59, 60, 999)
		# 					ts = make_datestamp date
		# 					while actions.length > 0 and actions[0]._time < date
		# 						action = actions.shift()
		# 						scores.current += rules.reduce ((value, rule) ->
		# 							pass = rule.conditions.every (cond) -> eval_cond action, cond
		# 							if not pass
		# 								return value
		# 							if rule?.score?.weight?
		# 								return value + parseInt rule.score.weight
		# 							if rule?.score?
		# 								return value + parseInt rule.score

		# 							console.log 'no score?', rule
		# 							return value

		# 						), 0
		# 						scores.min[ts] = scores.current
		# 					scores.full[ts] = scores.current
		# 					days.push scores.current
		# 					date += 86400 * 1000

		# 				# this one is required for the metric to work
		# 				# possibly make it a setting to change the tracked metric?
		# 				if (changes.filter((c) -> c.name is 'day').length) is 0
		# 					changes.push {name: 'day', interval: [1, 'day']}

		# 				changes.forEach (change) ->
		# 					key = change.name
		# 					n = change.interval[0] or 1
		# 					switch change.interval[1]
		# 						# "day" is implict/default
		# 						when 'week'  then n *= 7
		# 						when 'month' then n *= 30
		# 						when 'year'  then n *= 365

		# 					val = scores.current - (days[days.length - n - 1] ? 0)
		# 					scores.change[key] = absolute: 0, percentage: 0
		# 					if scores.current > 0
		# 						scores.change[key] = absolute: val, percentage: -100 + Math.ceil 100 * (scores.current + val) / scores.current

		# 				ret = scores.min
		# 				ret.change = scores.change
		# 				ret.today = scores.current
		# 				return ret

		# 			return "(#{fn.toString()})(actions, rules, changes)"

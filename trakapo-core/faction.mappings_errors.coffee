{
	mapping_id: "errors"
	fact_type: "errors"
	fact_identifier: "Date.now() + '-' + info.sid"
	info_type: "error"
	fields:
		sid: "info.sid",
		source: "info.source"
		error: "info.error",
		info: "info.extra"
}


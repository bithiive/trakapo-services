{
	name: "MUSS specific options",
	version: "1.0.0",
	public: false
	
	fact_settings:
		session:
			$diff: [
				{
					field: "fields.langs",
					mode: "set",
					value:
						mode: 'eval'
						eval: (settings) ->
							fn = (actions) ->
								actions.filter (action) ->
									action._value.type is 'page'
								.map (action) ->
									val = action._value.url.split('/')[3]
									return 'es' if val is 'es_es'
									return 'ru' if val is 'ru_ru'
									return 'en'
								.reduce((obj, lang) ->
									obj[lang]++
									obj.primary = (if obj[lang] > obj[obj.primary] then lang else obj.primary)
									obj.last = lang
									return obj
								, {es: 0, en: 0, ru: 0, primary: 'en'})
							return "(#{fn.toString()})(this.actions)"
				}
			]
		user:
			$diff: [
				{
					field: "fields.days_to_purchase",
					mode: "set",
					value:
						mode: 'eval'
						map:
							sessions: 'this.devices.sessions'
						eval: ->
							fn = ->
								# sort sessions ascending
								sessions.sort (a, b) -> if new Date(a._updated) < new Date(b._updated) then -1 else 1

								# loop through counting sessions/actions until checkout is found
								start = Infinity
								end = false
								session_count = 0
								action_count = 0
								found = do ->
									for session in sessions
										session_count++
										for action in session.actions
											action_count++
											try start = Math.min start, new Date action._time
											try
												if action._value.type is 'page' and action._value.url.match /checkout\/[0-9]+\/complete\/?$/
													end = new Date action._time
													return true

								days = Math.abs moment(start).diff(end, 'days', true).toFixed 1
								if found
									return days
								else
									return -1
							return "(#{fn.toString()})()"
				}
			]

	pages: [
		{
			url: "/users",
			order:
				after: "*"

			callback: ->
				this.widgets.table.fields.columns.push {
					field: 'langs.primary'
					title: 'Language'
					sort: false
					filter:
						type: 'select'
						values:
							en: 'English'
							es: 'Spanish'
							ru: 'Russian'
				}
				this.done()
		}
	]
}
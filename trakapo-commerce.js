({
  name: "Trakapo Commerce",
  description: "Commerce features",
  version: "1.1.2",
  icon: 'cart',
  dependencies: ["trakapo-core"],
  metrics: [
    {
      fact_type: 'basket',
      metric_id: 'sales',
      group: 'status.current',
      by_created: true,
      fields: {
        count: {
          mode: 'count'
        },
        total: {
          mode: 'sum',
          input: 'price.total.value'
        }
      }
    }
  ],
  info_mappings: [
    {
      mapping_id: "baskets",
      info_type: "basket",
      fact_type: "basket",
      fact_identifier: "info.bid",
      fields: [
        {
          field: "user_id",
          value: "info.id",
          condition: "info.id"
        }, {
          field: "did",
          value: "info.did"
        }, {
          field: "sid",
          value: "info.sid"
        }, {
          field: "dids",
          value: "info.did"
        }, {
          field: "sids",
          value: "info.sid"
        }, {
          field: "basket",
          value: "info.line_items",
          condition: "info.line_items"
        }, {
          field: "price",
          value: "info.prices",
          condition: "info.prices"
        }, {
          field: "status",
          value: "('undefined' !== typeof fact && fact.status) ? fact.status : {open: new Date, current: 'open'}"
        }
      ]
    }, {
      mapping_id: "basket_status",
      info_type: "basket_status",
      fact_type: "basket",
      fact_identifier: "info.bid",
      update_only: true,
      fields: [
        {
          field: "dids",
          value: "info.did"
        }, {
          field: "sids",
          value: "info.sid"
        }, {
          field: "status.{{info.status}}",
          value: "new Date",
          condition: "info.status"
        }, {
          field: "status.current",
          value: "info.status",
          condition: "info.status"
        }, {
          field: "info",
          value: "info.info",
          condition: "info.info"
        }
      ]
    }, {
      mapping_id: "user_update",
      info_type: "basket",
      fact_type: "user",
      fact_identifier: "info.id",
      update_only: true,
      fields: {}
    }, {
      mapping_id: "user_update",
      info_type: "basket_status",
      fact_type: "user",
      fact_identifier: "info.id",
      update_only: true,
      fields: {}
    }, {
      mapping_id: "session_update",
      info_type: "basket",
      fact_type: "session",
      fact_identifier: "info.sid",
      update_only: true,
      fields: {}
    }, {
      mapping_id: "session_update",
      info_type: "basket_status",
      fact_type: "session",
      fact_identifier: "info.sid",
      update_only: true,
      fields: {}
    }, {
      mapping_id: "device_update",
      info_type: "basket",
      fact_type: "device",
      fact_identifier: "info.did",
      update_only: true,
      fields: {}
    }, {
      mapping_id: "device_update",
      info_type: "basket_status",
      fact_type: "device",
      fact_identifier: "info.did",
      update_only: true,
      fields: {}
    }
  ],
  info_schemas: {
    basket: {
      fields: [
        {
          field: "bid",
          required: true
        }, {
          field: "sid",
          required: true
        }, {
          field: "did",
          required: true
        }, {
          field: "id",
          cast: "string | lowercase"
        }, {
          field: "status",
          type: "string"
        }, {
          field: "line_items",
          type: "array",
          required: true,
          fields: [
            {
              field: "sku",
              required: true
            }, {
              field: "name",
              required: true
            }, {
              field: "quantity",
              "default": 1,
              cast: "number"
            }, {
              field: "price",
              required: true,
              fields: [
                {
                  field: "value",
                  cast: "number",
                  required: true
                }
              ]
            }
          ]
        }, {
          field: "prices",
          required: true,
          fields: [
            {
              field: "tax",
              fields: [
                {
                  field: "value",
                  cast: "number",
                  required: true
                }
              ]
            }, {
              field: "items",
              fields: [
                {
                  field: "value",
                  cast: "number",
                  required: true
                }
              ]
            }, {
              field: "total",
              fields: [
                {
                  field: "value",
                  cast: "number",
                  required: true
                }
              ]
            }
          ]
        }
      ]
    },
    basket_status: {
      fields: [
        {
          field: "id",
          cast: "string | lowercase"
        }, {
          field: "bid",
          required: true
        }, {
          field: "status",
          required: true,
          type: 'string'
        }, {
          field: "info",
          required: false,
          type: 'object'
        }
      ]
    }
  },
  fact_settings: {
    user: {
      $diff: [
        {
          mode: 'set',
          field: 'fields.baskets',
          value: {
            mode: 'relation',
            fact_type: 'basket',
            has: 'many',
            query: {
              user_id: 'fact._id'
            }
          }
        }, {
          mode: 'set',
          field: "fields.bids",
          value: {
            mode: "push_unique"
          }
        }
      ]
    },
    session: {
      $diff: [
        {
          mode: 'set',
          field: 'fields.baskets',
          value: {
            mode: 'relation',
            fact_type: 'basket',
            has: 'many',
            query: {
              sid: 'fact._id'
            }
          }
        }, {
          mode: 'set',
          field: "fields.bids",
          value: {
            mode: "push_unique"
          }
        }
      ]
    },
    basket: {
      fields: {
        user: {
          mode: 'relation',
          fact_type: 'user',
          has: 'one',
          query: {
            _id: 'fact.user_id'
          }
        },
        device: {
          mode: 'relation',
          fact_type: 'device',
          has: 'one',
          query: {
            _id: 'fact.did'
          }
        },
        session: {
          mode: 'relation',
          fact_type: 'session',
          has: 'one',
          query: {
            _id: 'fact.sid'
          }
        },
        sids: {
          mode: 'push_unique'
        },
        dids: {
          mode: 'push_unique'
        },
        price: {
          mode: 'extend'
        },
        info: {
          mode: 'extend'
        },
        status: {
          mode: 'extend'
        },
        source: {
          mode: "eval",
          map: {
            action: "this.session.actions[0]._value"
          },
          "eval": function(settings) {
            var fn;
            fn = function() {
              var e, local, query, referrer;
              try {
                local = url(action.url, 'host');
                query = url(action.url, 'query');
                referrer = action.referrer && url(action.referrer, 'host');
                if (referrer && referrer.length > 0 && referrer !== local) {
                  if (referrer.match(/(google|bing|yahoo|ask|aol)\./)) {
                    return {
                      medium: 'search',
                      source: referrer
                    };
                  }
                  return {
                    medium: 'referrer',
                    source: referrer
                  };
                }
                if ((query != null ? query.utm_medium : void 0) != null) {
                  return {
                    medium: query.utm_medium,
                    source: query.utm_source,
                    campaign: query.utm_campaign
                  };
                }
                return {
                  medium: 'direct',
                  source: action.url
                };
              } catch (_error) {
                e = _error;
                return {
                  medium: 'error',
                  error: e.message,
                  stage: stage
                };
              }
            };
            return "(" + (fn.toString()) + ")()";
          }
        }
      }
    }
  }
});

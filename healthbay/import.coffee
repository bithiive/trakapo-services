{
    info_type: "import",
    fact_type: "basket",
    mapping_id: "basket_import",
    fact_identifier: "info.basket.bid",
    fields: [
        {field: "_updated", value: "new Date(info.timestamp)"},
        {field: "user_id", value: "info.user._id"},
        {field: "sid", value: "info.session"}
        {field: "did", value: "info.device"}
        {field: "basket", value: "info.basket.line_items"}
        {field: "price", value: "info.basket.price || info.basket.prices"},
        {field: "status.{{info.basket.status}}", value: "new Date(info.timestamp)"}
        {field: "status.current", value: "info.basket.status"}
        {field: "imported", value: true}
    ]
},
{
    info_type: "import",
    fact_type: "user",
    mapping_id: "user_import",
    fact_identifier: "info.user._id",
    fields: {
        _updated: "new Date(info.timestamp)",
        email: "info.user.email",
        name: "info.user.name",
        info: "info.user.info",
        imported: true
    }
}
{
    info_type: "import",
    fact_type: "session",
    mapping_id: "session_import",
    fact_identifier: "info.session",
    fields: {
        _updated: "new Date(info.timestamp)",
        did: "info.device",
        actions: "[]",
        referrer: "null",
        query: "{}",
        imported: true
    }
}
{
    info_type: "import",
    fact_type: "device",
    mapping_id: "device_import",
    fact_identifier: "info.device",
    fields: {
        _updated: "new Date(info.timestamp)",
        user_id: "info.user._id",
        imported: true
    }
}

({
  name: "Coals2U",
  version: "1.0.1",
  "public": false,
  tracking_goals: {
    checkout_start: {
      triggers: [
        {
          type: "load"
        }
      ],
      conditions: [
        {
          type: "url",
          path: "/checkout"
        }
      ],
      events: [
        {
          type: "basket_status",
          atomic: true,
          data: {
            bid: "=window.location.pathname.match(/checkout\\/([0-9]+)\\/?/)[1]",
            status: "checkout"
          }
        }
      ]
    },
    checkout_shipping: {
      triggers: [
        {
          type: "click",
          element: "#edit-customer-profile-shipping .checkout-next-fieldset"
        }
      ],
      conditions: [
        {
          type: "url",
          path: "/checkout"
        }
      ],
      events: [
        {
          type: "basket_status",
          atomic: true,
          data: {
            bid: "=window.location.pathname.match(/checkout\\/([0-9]+)\\/?/)[1]",
            status: "shipping"
          }
        }
      ]
    },
    checkout_billing: {
      triggers: [
        {
          type: "click",
          element: "#edit-customer-profile-billing .checkout-next-fieldset"
        }
      ],
      conditions: [
        {
          type: "url",
          path: "/checkout"
        }
      ],
      events: [
        {
          type: "basket_status",
          atomic: true,
          data: {
            bid: "=window.location.pathname.match(/checkout\\/([0-9]+)\\/?/)[1]",
            status: "billing"
          }
        }
      ]
    },
    checkout_payment: {
      triggers: [
        {
          type: "click",
          element: "#edit-commerce-fieldgroup-pane-group-order-comments .js-checkout-submit-button"
        }, {
          type: "click",
          element: "input.checkout-continue.form-submit"
        }
      ],
      conditions: [
        {
          type: "url",
          path: "/checkout"
        }
      ],
      events: [
        {
          type: "basket_status",
          atomic: true,
          data: {
            bid: "=window.location.pathname.match(/checkout\\/([0-9]+)\\/?/)[1]",
            status: "payment"
          }
        }
      ]
    }
  },
  fact_settings: {
    session: {
      $diff: [
        {
          mode: "set",
          field: "fields.postcode",
          value: {
            mode: "eval",
            map: {
              datums: "this.actions.map(function (action) { return action._value.data || action._value.info }).filter(Boolean)",
              postcode: "datums.map(function (datum) { return datum.postcode }).filter(Boolean).pop()",
              regex: "x = /^(([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]?)([0-9][ABD-HJLN-UW-Z]{2})|GIR 0AA)$/",
              m: "(postcode || '').toString().toUpperCase().replace(/[^0-9A-Z]+/g, '').match(regex)"
            },
            "eval": "x = {full: m[1], district: m[2], sector: m[3].charAt(0), unit: m[3].slice(1)}"
          }
        }, {
          mode: "set",
          field: "fields.stage_created_basket",
          value: {
            mode: "eval",
            "eval": "fact.actions.filter(function(action) { return action._value.form_id && action._value.form_id.match(/^commerce.cart.add.to.cart.form/) }).length > 0"
          }
        }, {
          mode: "set",
          field: "fields.stage_checkout",
          value: {
            mode: "eval",
            "eval": "fact.actions.filter(function(action) { return action._value.url && action._value.url.match(/checkout\\/[0-9]+\\/complete$/) }).length > 0"
          }
        }
      ]
    },
    device: {
      $diff: [
        {
          mode: "set",
          field: "fields.postcode",
          value: {
            mode: "eval",
            map: {
              postcodes: "this.sessions.filter(function(session) { return session.postcode }).map(function(session) { session.postcode.time = session.actions[session.actions.length - 1]._time; return session.postcode })"
            },
            "eval": "postcodes.reduce(function(a, b) { return (a.time < b.time) ? b : a })"
          }
        }, {
          mode: "set",
          field: "fields.depot",
          value: {
            mode: "relation",
            fact_type: "depots",
            has: "one",
            query: {
              postcodes: "fact.postcode ? fact.postcode.district + fact.postcode.sector : 'NULL'"
            }
          }
        }, {
          mode: "set",
          field: "fields.depot_id",
          value: {
            mode: "eval",
            "eval": "this.depot._id"
          }
        }, {
          mode: "set",
          field: "fields.source",
          value: {
            mode: "eval",
            "eval": "this.sessions.source.pop()"
          }
        }, {
          mode: "set",
          field: "fields.stage_created_basket",
          value: {
            mode: "eval",
            "eval": "this.sessions.filter(function(session) { return session.stage_created_basket }).length"
          }
        }, {
          mode: "set",
          field: "fields.stage_checkout",
          value: {
            mode: "eval",
            "eval": "this.sessions.filter(function(session) { return session.stage_checkout }).length"
          }
        }
      ]
    },
    basket: {
      $diff: [
        {
          mode: "set",
          field: "fields.depot_id",
          value: {
            mode: "eval",
            "eval": "this.device.depot_id || 'ERR'",
            "default": "ERR"
          }
        }
      ]
    }
  }
});

{
	"name": "Amaven specific options",
	"version": "1.0.0",
	"public": false

	"tracking_goals": {
		"enhanced_contact_form_identify": {
			"triggers": [
				{
					"type": "load",
					"element": "form#contact-entityform-edit-form"
				},
				{
					"type": "submit",
					"element": "form#contact-entityform-edit-form"
				},
				{
					"type": "event",
					"event": "blur"
					"element": "input"
					"processForm": true
				}
			],
			"conditions": [
				{
					"type": "exists",
					"element": "form#contact-entityform-edit-form"
				},
				{
					"type": "contains",
					"element": "form#contact-entityform-edit-form #edit-field-contact-email-und-0-email",
					"value": "@",
				}
			],
			"events": [
				{
					"type": "identify",
					"atomic": true,
					"data": {
						"id": "=data.field_contact_email.und[0].email",
						"email": "=data.field_contact_email.und[0].email",
						"name": "=data.field_contact_name.und[0].value",
						"info": {
							"first_name": "=data.field_contact_name.und[0].value.split(' ').shift()",
							"last_name": "=data.field_contact_name.und[0].value.split(' ').pop()",
							"newsletter_subscriber": "=data.field_contact_newsletter_signup.und === '1'"
						}
					},
					"restrict": "=restrict(session, key, data)"
				}
			]
		}
	}
}

fact_settings:
	session:
		$diff: [
			{
				mode: "set",
				field: "fields.postcode"
				value: 
					mode: "eval"
					map:
						# list of all postcodes entered in this session
						datums: "this.actions.map(function (action) { return action._value.data || action._value.info }).filter(Boolean)"
						postcode: "datums.map(function (datum) { return datum.postcode }).filter(Boolean).pop()"
						regex: "x = /^(([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]?)([0-9][ABD-HJLN-UW-Z]{2})|GIR 0AA)$/"
						# extract teh information using this regex
						m: "(postcode || '').toString().toUpperCase().replace(/[^0-9A-Z]+/g, '').match(regex)"
					eval: "x = {full: m[1], district: m[2], sector: m[3].charAt(0), unit: m[3].slice(1)}"
			},
			{
				mode: "set"
				field: "fields.stage_created_basket"
				value:
					mode: "eval",
					# boolean if there is at least one commerce cart created form entry
					eval: "fact.actions.filter(function(action) { return action._value.form_id && action._value.form_id.match(/^commerce.cart.add.to.cart.form/) }).length > 0"
			}
			{
				mode: "set"
				field: "fields.stage_checkout",
				value:
					mode: "eval",
					# boolean if there is at least one checkout/**/complete page entry
					eval: "fact.actions.filter(function(action) { return action._value.url && action._value.url.match(/checkout\\/[0-9]+\\/complete$/) }).length > 0"
			}
		]

	device:
		$diff: [
			# extract the most recent postcode from the sessions
			{
				mode: "set",
				field: "fields.postcode"
				value:
					mode: "eval",
					map: 
						# list of all postcodes of this devices sessions
						# with the time of the last action in the session appended
						postcodes: "this.sessions.filter(function(session) { return session.postcode }).map(function(session) { session.postcode.time = session.actions[session.actions.length - 1]._time; return session.postcode })"
						# now reduce to the most recent session with a postcode
					eval: "postcodes.reduce(function(a, b) { return (a.time < b.time) ? b : a })"
			}
			{
				mode: "set"
				field: "fields.depot"
				value:
					mode: "relation"
					fact_type: "depots"
					has: "one"
					query:
						postcodes: "fact.postcode ? fact.postcode.district + fact.postcode.sector : 'NULL'"
			}
			# de-normalize the depot id for metrics
			{
				mode: "set",
				field: "fields.depot_id"
				value:
					mode: "eval"
					eval: "this.depot._id"
			}
			# extract the most recent source from the sessions? ask about this
			{
				mode: "set",
				field: "fields.source"
				value:
					mode: "eval",
					eval: "this.sessions.source.pop()"
			}
			# extract the C2U stage info from the sessions
			{
				mode: "set",
				field: "fields.stage_created_basket",
				value:
					mode: "eval"
					eval: "this.sessions.filter(function(session) { return session.stage_created_basket }).length"
			}
			{
				mode: "set",
				field: "fields.stage_checkout",
				value:
					mode: "eval"
					eval: "this.sessions.filter(function(session) { return session.stage_checkout }).length"
			}
		]
	basket:
		$diff: [
			{
				mode: "set",
				field: "fields.depot_id",
				value:
					mode: "eval",
					eval: "this.device.depot_id || 'ERR'"
					default: "ERR"
			}
		]
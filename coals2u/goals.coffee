tracking_goals:
	checkout_start: {
		triggers: [
			{
				type: "load"
			}
		],
		conditions: [
			{
				type: "url",
				path: "/checkout"
			}
		],
		events: [
			{
				type: "basket_status",
				atomic: true,
				data: {
					bid: "=window.location.pathname.match(/checkout\\/([0-9]+)\\/?/)[1]"
					status: "checkout"
				}
			}

		]
	}
	checkout_shipping: {
		triggers: [
			{
				type: "click",
				element: "#edit-customer-profile-shipping .checkout-next-fieldset"
			}
		],
		conditions: [
			{
				type: "url",
				path: "/checkout"
			}
		],
		events: [
			{
				type: "basket_status",
				atomic: true,
				data: {
					bid: "=window.location.pathname.match(/checkout\\/([0-9]+)\\/?/)[1]"
					status: "shipping"
				}
			}
		]
	},
	checkout_billing: {
		triggers: [
			{
				type: "click",
				element: "#edit-customer-profile-billing .checkout-next-fieldset"
			}
		],
		conditions: [
			{
				type: "url",
				path: "/checkout"
			}
		],
		events: [
			{
				type: "basket_status",
				atomic: true,
				data: {
					bid: "=window.location.pathname.match(/checkout\\/([0-9]+)\\/?/)[1]"
					status: "billing"
				}
			}
		]
	},
	checkout_payment: {
		triggers: [
			{
				type: "click",
				element: "#edit-commerce-fieldgroup-pane-group-order-comments .js-checkout-submit-button"
			},
			{
				type: "click",
				element: "input.checkout-continue.form-submit"
			}
		],
		conditions: [
			{
				type: "url",
				path: "/checkout"
			}
		],
		events: [
			{
				type: "basket_status",
				atomic: true,
				data: {
					bid: "=window.location.pathname.match(/checkout\\/([0-9]+)\\/?/)[1]"
					status: "payment"
				}
			}
		]
	},

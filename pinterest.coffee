{
	"name": "Pinterest",
	"description": "Pinterest social service",
	"version": "0.1.8",
	"public": false,

	"info_mappings": [
		{
			"mapping_id": "pinterest_board",
			"info_type": "pinterest_board",
			"fact_type": "pinterest_board",
			"fact_identifier": "info._id",
			"fields": [
				{
					"field": "_created",
					"value": "new Date(info._created)"
				},
				{
					"field": "name",
					"value": "info.name"
				},
				{
					"field": "description",
					"value": "info.description"
				},
				{
					"field": "image",
					"value": "info.image"
				},
				{
					"field": "url",
					"value": "info.url"
				},
				{
					"field": "category",
					"value": "info.category"
				},
				{
					"field": "page_id",
					"value": "info.account"
				},
				{
					"field": "user_id",
					"value": "( info.user && info.user._id ) || info.account"
				},
				{
					"field": "pin_count",
					"value": "info.pin_count"
				},
				{
					"field": "followers",
					"value": "info.follower_count"
				},
				{
					"field": "summarize",
					"value": "!!info.summarize"
				}
			]
		}
		
		{
			"mapping_id": "pinterest_comment_interaction",
			"info_type": "pinterest_comment",
			"fact_type": "pinterest_interaction",
			"fact_identifier": "info._id",
			"fields": [
				{
					"field": "_created",
					"value": "new Date(info._created)"
				},
				{
					"field": "type",
					"value": "'reply'"
				},
				{
					"field": "text",
					"value": "info.text"
				},
				{
					"field": "page_id",
					"value": "info.account"
				},
				{
					"field": "user_id",
					"value": "info.user._id"
				},
				{
					"field": "pin_id",
					"value": "info.parent_id"
				}
			]
		},
		
		{
			"mapping_id": "pinterest_comment_pin",
			"info_type": "pinterest_comment",
			"fact_type": "pinterest_pin",
			"fact_identifier": "info.parent_id",
			"fields": []
		},
		
		{
			"mapping_id": "pinterest_comment_user",
			"info_type": "pinterest_comment",
			"fact_type": "pinterest_user",
			"fact_identifier": "info.user._id",
			"fields": [
				{
					"field": "_created",
					"value": "new Date(info.user._created)"
				},
				{
					"field": "handle",
					"value": "info.user.screen_name"
				},
				{
					"field": "name",
					"value": "info.user.name"
				},
				{
					"field": "location",
					"value": "info.user.location"
				},
				{
					"field": "followers",
					"value": "info.user.followers"
				},
				{
					"field": "image",
					"value": "info.user.image"
				},
				{
					"field": "like_count",
					"value": "info.user.like_count"
				},
				{
					"field": "pin_count",
					"value": "info.user.pin_count"
				},
				{
					"field": "board_count",
					"value": "info.user.board_count"
				},
				{
					"field": "gender",
					"value": "info.user.gender"
				},
				{
					"field": "url",
					"value": "info.user.url"
				}
			]
		}
		
		{
			"mapping_id": "pinterest_follow_interaction",
			"info_type": "pinterest_follow",
			"fact_type": "pinterest_interaction",
			"fact_identifier": "info._id",
			"fields": [
				{
					"field": "_created",
					"value": "new Date(info.timestamp)"
				},
				{
					"field": "type",
					"value": "info.type"
				},
				{
					"field": "page_id",
					"value": "info.account"
				},
				{
					"field": "user_id",
					"value": "info.user._id"
				},
				{
					"field": "parent_id",
					"value": "info.parent_id"
				}
			]
		},
		
		{
			"mapping_id": "pinterest_follow_user",
			"info_type": "pinterest_follow",
			"fact_type": "pinterest_user",
			"fact_identifier": "info.user._id",
			"fields": [
				{
					"field": "_created",
					"value": "new Date(info.user._created)"
				},
				{
					"field": "handle",
					"value": "info.user.screen_name"
				},
				{
					"field": "name",
					"value": "info.user.name"
				},
				{
					"field": "location",
					"value": "info.user.location"
				},
				{
					"field": "followers",
					"value": "info.user.followers"
				},
				{
					"field": "image",
					"value": "info.user.image"
				},
				{
					"field": "like_count",
					"value": "info.user.like_count"
				},
				{
					"field": "pin_count",
					"value": "info.user.pin_count"
				},
				{
					"field": "board_count",
					"value": "info.user.board_count"
				},
				{
					"field": "gender",
					"value": "info.user.gender"
				},
				{
					"field": "url",
					"value": "info.user.url"
				}
			]
		}
		
		{
			"mapping_id": "pinterest_like_interaction",
			"info_type": "pinterest_like",
			"fact_type": "pinterest_interaction",
			"fact_identifier": "info._id",
			"fields": [
				{
					"field": "_created",
					"value": "new Date(info._created)"
				},
				{
					"field": "type",
					"value": "'like'"
				},
				{
					"field": "page_id",
					"value": "info.account"
				},
				{
					"field": "user_id",
					"value": "info.user._id"
				},
				{
					"field": "pin_id",
					"value": "info.parent_id"
				}
			]
		},
		
		{
			"mapping_id": "pinterest_like_pin",
			"info_type": "pinterest_like",
			"fact_type": "pinterest_pin",
			"fact_identifier": "info.parent_id",
			"fields": []
		},
		
		{
			"mapping_id": "pinterest_like_user",
			"info_type": "pinterest_like",
			"fact_type": "pinterest_user",
			"fact_identifier": "info.user._id",
			"fields": [
				{
					"field": "_created",
					"value": "new Date(info.user._created)"
				},
				{
					"field": "handle",
					"value": "info.user.screen_name"
				},
				{
					"field": "name",
					"value": "info.user.name"
				},
				{
					"field": "location",
					"value": "info.user.location"
				},
				{
					"field": "followers",
					"value": "info.user.followers"
				},
				{
					"field": "image",
					"value": "info.user.image"
				},
				{
					"field": "like_count",
					"value": "info.user.like_count"
				},
				{
					"field": "pin_count",
					"value": "info.user.pin_count"
				},
				{
					"field": "board_count",
					"value": "info.user.board_count"
				},
				{
					"field": "gender",
					"value": "info.user.gender"
				},
				{
					"field": "url",
					"value": "info.user.url"
				}
			]
		}
		
		{
			"mapping_id": "pinterest_pin",
			"info_type": "pinterest_pin",
			"fact_type": "pinterest_pin",
			"fact_identifier": "info._id",
			"fields": [
				{
					"field": "_created",
					"value": "new Date(info._created)"
				},
				{
					"field": "text",
					"value": "info.text"
				},
				{
					"field": "image",
					"value": "info.image"
				},
				{
					"field": "domain",
					"value": "info.domain"
				},
				{
					"field": "link",
					"value": "info.link"
				},
				{
					"field": "page_id",
					"value": "info.account"
				},
				{
					"field": "user_id",
					"value": "( info.user && info.user._id ) || info.account"
				},
				{
					"field": "board_id",
					"value": "( info.board && info.board._id )"
				},
				{
					"field": "like_count",
					"value": "info.like_count || 0"
				},
				{
					"field": "reply_count",
					"value": "info.comment_count || 0"
				},
				{
					"field": "share_count",
					"value": "info.repin_count || 0"
				}
			]
		}
		
		{
			"mapping_id": "pinterest_repin_interaction",
			"info_type": "pinterest_repin",
			"fact_type": "pinterest_interaction",
			"fact_identifier": "info._id",
			"fields": [
				{
					"field": "_created",
					"value": "new Date(info._created)"
				},
				{
					"field": "type",
					"value": "'share'"
				},
				{
					"field": "page_id",
					"value": "info.account"
				},
				{
					"field": "user_id",
					"value": "info.user._id"
				},
				{
					"field": "pin_id",
					"value": "info.parent_id"
				},
				{
					"field": "board_id",
					"value": "info.board._id"
				}
			]
		},
		
		{
			"mapping_id": "pinterest_repin_pin",
			"info_type": "pinterest_repin",
			"fact_type": "pinterest_pin",
			"fact_identifier": "info.parent_id",
			"fields": []
		},
		
		{
			"mapping_id": "pinterest_repin_user",
			"info_type": "pinterest_repin",
			"fact_type": "pinterest_user",
			"fact_identifier": "info.user._id",
			"fields": [
				{
					"field": "_created",
					"value": "new Date(info.user._created)"
				},
				{
					"field": "handle",
					"value": "info.user.screen_name"
				},
				{
					"field": "name",
					"value": "info.user.name"
				},
				{
					"field": "location",
					"value": "info.user.location"
				},
				{
					"field": "followers",
					"value": "info.user.followers"
				},
				{
					"field": "image",
					"value": "info.user.image"
				},
				{
					"field": "like_count",
					"value": "info.user.like_count"
				},
				{
					"field": "pin_count",
					"value": "info.user.pin_count"
				},
				{
					"field": "board_count",
					"value": "info.user.board_count"
				},
				{
					"field": "gender",
					"value": "info.user.gender"
				},
				{
					"field": "url",
					"value": "info.user.url"
				}
			]
		}
		
		
		{
			"mapping_id": "pinterest_repin_board",
			"info_type": "pinterest_repin",
			"fact_type": "pinterest_board",
			"fact_identifier": "info.board._id",
			"fields": [
				{
					"field": "name",
					"value": "info.board.name"
				},
				{
					"field": "description",
					"value": "info.board.description"
				},
				{
					"field": "image",
					"value": "info.board.image"
				},
				{
					"field": "url",
					"value": "info.board.url"
				},
				{
					"field": "pin_count",
					"value": "info.board.pin_count"
				},
				{
					"field": "follower_count",
					"value": "info.board.follower_count"
				},
				{
					"field": "category",
					"value": "info.board.category"
				}
			]
		}
		
		{
			"mapping_id": "pinterest_user",
			"info_type": "pinterest_user",
			"fact_type": "pinterest_user",
			"fact_identifier": "info._id",
			"fields": [
				{
					"field": "_created",
					"value": "new Date(info._created)"
				},
				{
					"field": "handle",
					"value": "info.screen_name"
				},
				{
					"field": "name",
					"value": "info.name"
				},
				{
					"field": "location",
					"value": "info.location"
				},
				{
					"field": "follower_count",
					"value": "info.followers"
				},
				{
					"field": "image",
					"value": "info.image"
				},
				{
					"field": "summarize",
					"value": "!!info.summarize"
				},
				{
					"field": "like_count",
					"value": "info.like_count"
				},
				{
					"field": "pin_count",
					"value": "info.pin_count"
				},
				{
					"field": "board_count",
					"value": "info.board_count"
				},
				{
					"field": "gender",
					"value": "info.gender"
				},
				{
					"field": "url",
					"value": "info.url"
				}
			]
		}
		
		# map metric results into the count field
		{
			"mapping_id": "pinterest_user_er_counts",
			"info_type": "pinterest_user_er_count",
			"fact_type": "pinterest_user",
			"fact_identifier": "info._id.page_id",
			"fields": [
				{
					"field": "count.{{info._id.type}}",
					"value": "info.count"
				}
			]
		}
		
		
	],

	"metrics": [
		{
			"metric_id": "type_counts",
			"fact_type": "pinterest_interaction",
			"by_created": true,
			"group": ["page_id", "type"],
			"fields": {
				"count": {
					"mode": "count"
				}
			}
		},
		
		{
			"metric_id": "summary",
			"fact_type": "pinterest_user",
			"group": "handle",
			"filter": {
				"summarize": true
			},
			"fields": {
				"followers_start": {
					"mode": "min",
					"input": "follower_count"
				},
				"followers_end": {
					"mode": "max",
					"input": "follower_count"
				},
				"like_sum": {
					"mode": "sum",
					"input": "like_count"
				},
				"like_avg": {
					"mode": "avg",
					"input": "like_count"
				},
				"pin_count_sum": {
					"mode": "sum",
					"input": "pin_count"
				},
				"pin_count_avg": {
					"mode": "avg",
					"input": "pin_count"
				},
				"board_count_sum": {
					"mode": "sum",
					"input": "board_count"
				},
				"board_count_avg": {
					"mode": "avg",
					"input": "board_count"
				},
				"engagement_sum": {
					"mode": "sum",
					"input": "engagement"
				},
				"engagement_avg": {
					"mode": "avg",
					"input": "engagement"
				},
				"engagement_ratio_sum": {
					"mode": "sum",
					"input": "engagement_ratio"
				},
				"engagement_ratio_avg": {
					"mode": "avg",
					"input": "engagement_ratio"
				}
			}
		}
		
		{
			"metric_id": "summary",
			"fact_type": "pinterest_board",
			"group": "_id",
			"by_created": true,
			"filter": {
				"summarize": true
			},
			"fields": {
				"pin_count_sum": {
					"mode": "sum",
					"input": "pin_count"
				},
				"pin_count_avg": {
					"mode": "avg",
					"input": "pin_count"
				},
				"followers_start": {
					"mode": "min",
					"input": "followers"
				},
				"followers_end": {
					"mode": "max",
					"input": "followers"
				},
				"reach_sum": {
					"mode": "sum",
					"input": "reach"
				},
				"reach_avg": {
					"mode": "avg",
					"input": "reach"
				},
				"like_sum": {
					"mode": "sum",
					"input": "like_count"
				},
				"like_avg": {
					"mode": "avg",
					"input": "like_count"
				},
				"reply_sum": {
					"mode": "sum",
					"input": "reply_count"
				},
				"reply_avg": {
					"mode": "avg",
					"input": "reply_count"
				},
				"share_sum": {
					"mode": "sum",
					"input": "share_count"
				},
				"share_avg": {
					"mode": "avg",
					"input": "share_count"
				},
				"interaction_sum": {
					"mode": "sum",
					"input": "interaction_count"
				},
				"interaction_avg": {
					"mode": "avg",
					"input": "interaction_count"
				},
				"engagement_sum": {
					"mode": "sum",
					"input": "engagement"
				},
				"engagement_avg": {
					"mode": "avg",
					"input": "engagement"
				},
				"engagement_ratio_sum": {
					"mode": "sum",
					"input": "engagement_ratio"
				},
				"engagement_ratio_avg": {
					"mode": "avg",
					"input": "engagement_ratio"
				}
			}
		},
		
		{
			"metric_id": "summary",
			"fact_type": "pinterest_pin",
			"group": "user_id",
			"by_created": true,
			"fields": {
				"reach_sum": {
					"mode": "sum",
					"input": "reach"
				},
				"reach_avg": {
					"mode": "avg",
					"input": "reach"
				},
				"like_sum": {
					"mode": "sum",
					"input": "like_count"
				},
				"like_avg": {
					"mode": "avg",
					"input": "like_count"
				},
				"reply_sum": {
					"mode": "sum",
					"input": "reply_count"
				},
				"reply_avg": {
					"mode": "avg",
					"input": "reply_count"
				},
				"share_sum": {
					"mode": "sum",
					"input": "share_count"
				},
				"share_avg": {
					"mode": "avg",
					"input": "share_count"
				},
				"interaction_sum": {
					"mode": "sum",
					"input": "interaction_count"
				},
				"interaction_avg": {
					"mode": "avg",
					"input": "interaction_count"
				}
			}
		}
		
	],

	"hooks": {
		
	},

	"fact_settings": {
		"pinterest_user": {
			"fields": {
				"boards": {
					"mode": "relation",
					"fact_type": "pinterest_board",
					"has": "many",
					"query": {
						"user_id": "fact._id"
					}
				},
		
				"followers": {
					"mode": "relation",
					"fact_type": "pinterest_interaction",
					"has": "many",
					"query": {
						"type": "'user_follow'",
						"page_id": "fact.handle"
					}
				},
		
				# this is now written directly
				# consider using a "map" type to give historic values?
				# "follower_count": {
				# 	"mode": "eval",
				# 	"eval": "relation.count('followers')"
				# },
		
				# these are mapped in via a metric on interactions, coming through as individual {type=count} info
				# as such, they need to extend the existing object
				"count": {
					"mode": "extend"
				},
		
				# # this will only be not-null for "summarize=true" pages.
				"engagement": {
					"mode": "eval",
					"condition": "this.summarize",
					"map": {
						"fvc": "relation.sum_field('boards', 'like_count')",
						"rtc": "relation.sum_field('boards', 'share_count')",
						"rec": "relation.sum_field('boards', 'reply_count')",
						"engagement": "(fvc * 1.5) + (rtc * 0.5) + rec"
					},
					"eval": "engagement"
				},
		
				"engagement_ratio": {
					"mode": "eval",
					"condition": "this.summarize",
					"map": {
						"fvc": "relation.sum_field('boards', 'like_count')",
						"rtc": "relation.sum_field('boards', 'share_count')",
						"rec": "relation.sum_field('boards', 'reply_count')",
						"engagement": "(fvc * 1.5) + (rtc * 0.5) + rec",
						"ratio": "this.follower_count / engagement"
					},
					"eval": "parseFloat(ratio.toFixed(4))"
				},
		
				"reach": {
					"mode": "eval",
					"map": {
						"share_reach": "relation.sum_field('boards', 'reach')",
						"page_reach": "this.follower_count"
					},
					"eval": "Number(share_reach) + Number(page_reach)"
				}
		
			}
		},
		
		"pinterest_board": {
			"fields": {
				"page": {
					"mode": "relation",
					"fact_type": "pinterest_user",
					"has": "one",
					"query": {
						"_id": "fact.user_id"
					}
				},
		
				"pins": {
					"mode": "relation",
					"fact_type": "pinterest_pin",
					"has": "many",
					"query": {
						"board_id": "fact._id"
					}
				},
		
				"like_count": {
					"mode": "eval",
					"eval": "relation.sum_field('pins', 'like_count')"
				},
		
				"reply_count": {
					"mode": "eval",
					"eval": "relation.sum_field('pins', 'reply_count')"
				},
		
				"share_count": {
					"mode": "eval",
					"eval": "relation.sum_field('pins', 'share_count')"
				},
		
				"interaction_count": {
					"mode": "eval",
					"eval":  "relation.sum_field('pins', 'interaction_count')"
				},
		
				"reach": {
					"mode": "eval",
					"map": {
						"share_reach": "relation.sum_field('pins', 'reach')",
						"page_reach": "this.followers"
					},
					"eval": "Number(share_reach) + Number(page_reach)"
				},
		
				"engagement": {
					"mode": "eval",
					"map": {
						"fvc": "relation.sum_field('pins', 'like_count')",
						"rtc": "relation.sum_field('pins', 'share_count')",
						"rec": "relation.sum_field('pins', 'reply_count')",
						"engagement": "(fvc * 1.5) + (rtc * 0.5) + rec"
					},
					"eval": "engagement"
				},
		
				"engagement_ratio": {
					"mode": "eval",
					"map": {
						"fvc": "relation.sum_field('pins', 'like_count')",
						"rtc": "relation.sum_field('pins', 'share_count')",
						"rec": "relation.sum_field('pins', 'reply_count')",
						"engagement": "(fvc * 1.5) + (rtc * 0.5) + rec",
						"ratio": "this.followers / engagement"
					},
					"eval": "parseFloat(ratio.toFixed(4))"
				}
			}
		},
		
		"pinterest_pin": {
			"fields": {
				"page": {
					"mode": "relation",
					"fact_type": "pinterest_user",
					"has": "one",
					"query": {
						"_id": "fact.user_id"
					}
				},
		
				"board": {
					"mode": "relation",
					"fact_type": "pinterest_board",
					"has": "one",
					"query": {
						"_id": "fact.board_id"
					}
				},
		
				"likes": {
					"mode": "relation",
					"fact_type": "pinterest_interaction",
					"has": "many",
					"query": {
						"type": "'like'",
						"pin_id": "fact._id"
					}
				},
		
				"like_count": {
					"mode": "eval",
					"eval": "relation.count('likes')"
				},
		
				"replies": {
					"mode": "relation",
					"fact_type": "pinterest_interaction",
					"has": "many",
					"query": {
						"type": "'reply'",
						"pin_id": "fact._id"
					}
				},
		
				"reply_count": {
					"mode": "eval",
					"eval": "relation.count('replies')"
				},
		
				"shares": {
					"mode": "relation",
					"fact_type": "pinterest_interaction",
					"has": "many",
					"query": {
						"type": "'share'",
						"pin_id": "fact._id"
					}
				},
		
				"share_count": {
					"mode": "eval",
					"eval": "relation.count('shares')"
				},
		
				"interaction_count": {
					"mode": "eval",
					"eval": "relation.count('replies', 'likes', 'shares')"
				},
		
				"reach": {
					"mode": "eval",
					"map": {
						"share_reach": "relation.sum_field('shares', 'reach')",
						"page_reach": ["this.page.followers", 0]
					},
					"eval": "Number(share_reach) + Number(page_reach)"
				}
			}
		},
		
		"pinterest_interaction": {
			"fields": {
				"_created": {
					"mode": "oldest"
				},
		
				"page": {
					"mode": "relation",
					"fact_type": "pinterest_user",
					"has": "one",
					"query": {
						"page_id": "fact.page_id"
					}
				},
		
				"user": {
					"mode": "relation",
					"fact_type": "pinterest_user",
					"has": "one",
					"query": {
						"_id": "fact.user_id"
					}
				},
		
				"pin": {
					"mode": "relation",
					"fact_type": "pinterest_pin",
					"has": "one",
					"query": {
						"_id": "fact.pin_id"
					}
				}
			}
		}
		
	}
}

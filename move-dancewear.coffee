{
	name: "Move Dancewear"
	version: "1.0.0",
	public: false

	dependencies: ["trakapo-core", "trakapo-commerce"],

	tracking_goals:
		identify: {
			triggers: [
				{
					type: "load"
				}
			],
			conditions: [
				{
					type: "eval",
					"eval": "_trakapo_vars.user && _trakapo_vars.user.email !== null"
				}
			],
			events: [
				{
					type: "identify",
					atomic: true,
					data: {
						id: "=_trakapo_vars.user.email",
						email: "=_trakapo_vars.user.email",
						name: "=_trakapo_vars.user.name",
						info: "=_trakapo_vars.user.info"
					},
					restrict: "=restrict(session, key, data)"
				}
			]
		},
		identify_monitor: {
			triggers: [
				{
					type: "load"
				},
				{
					type: "submit",
					element: "form#single_checkout_form"
				},
				{
					type: "event",
					event: "blur",
					element: "input",
					processForm: true
				}
			],
			conditions: [
				{
					type: "exists",
					element: "form#single_checkout_form",
					debug: true
				},
				{
					type: "contains",
					element: "form#single_checkout_form #email_address",
					value: "@",
					debug: true
				}
			],
			events: [
				{
					type: "identify",
					atomic: true,
					data: {
						id: "=data.email_address",
						email: "=data.email_address",
						name: "=data.firstname + ' ' + data.lastname"
					},
					restrict: "=restrict(session, key, data)"
				}
			]
		},
		identify_teacher: {
			triggers: [
				{
					type: "load"
				}
			],
			conditions: [
				{
					type: "eval",
					"eval": "_trakapo_vars.user && _trakapo_vars.user.email === null && _trakapo_vars.user.info.customers_email_address"
				}
			],
			events: [
				{
					type: "identify",
					atomic: true,
					data: {
						id: "=_trakapo_vars.user.info.customers_email_address",
						email: "=_trakapo_vars.user.info.customers_email_address",
						name: "=_trakapo_vars.user.info.customers_firstname + ' ' + _trakapo_vars.user.info.customers_lastname",
						info: "=_trakapo_vars.user.info"
					},
					restrict: "=restrict(session, key, data)"
				}
			]
		},
		basket: {
			triggers: [
				{
					type: "load"
				}
			],
			conditions: [
				{
					type: "eval",
					"eval": "typeof _trakapo_vars.cart === 'object' && _trakapo_vars.cart.line_items"
				}
			],
			events: [
				{
					type: "basket",
					atomic: true,
					data: {
						bid: "=T.getIds( ).session",
						line_items: "=_trakapo_vars.cart.line_items",
						prices: "=_trakapo_vars.cart.prices"
					},
					restrict: "=restrict(session, key, data)"
				}
			]
		},
		basket_status_checkout: {
			triggers: [
				{
					type: "load"
				}
			],
			conditions: [
				{
					type: "url",
					path: "/cp_single_checkout.php"
				}
			],
			events: [
				{
					type: "basket_status",
					atomic: true,
					data: {
						bid: "=T.getIds( ).session",
						status: "checkout"
					},
					restrict: "=restrict(session, key, data)"
				}
			]
		},
		basket_status_complete_ensurance: {
			triggers: [
				{
					type: "load"
				}
			],
			conditions: [
				{
					type: "url",
					path: "/checkout_success.php"
				}
			],
			events: [
				{
					type: "basket_status",
					atomic: true,
					data: {
						bid: "=T.getIds( ).session",
						status: "complete"
					},
					restrict: "=restrict(session, key, data)"
				}
			]
		},
		basket_status_complete: {
			triggers: [
				{
					type: "load"
				}
			],
			conditions: [
				{
					type: "eval",
					"eval": "_trakapo_vars.user && _trakapo_vars.user.info.postcode"
				}
			],
			events: [
				{
					type: "basket_status",
					atomic: true,
					data: {
						bid: "=T.getIds( ).session",
						status: "complete",
						info: {
							tel: "=_trakapo_vars.user.info.tel",
							delivery_address: {
								company: "=_trakapo_vars.user.info.company",
								street_address: "=_trakapo_vars.user.info.street_address",
								suburb: "=_trakapo_vars.user.info.suburb",
								city: "=_trakapo_vars.user.info.city",
								postcode: "=_trakapo_vars.user.info.postcode",
								state: "=_trakapo_vars.user.info.state",
								country: "=_trakapo_vars.user.info.country"
							}
						}
					},
					restrict: "=restrict(session, key, data)"
				}
			]
		}
	

}
{
	"mapping_id": "pinterest_repin_interaction",
	"info_type": "pinterest_repin",
	"fact_type": "pinterest_interaction",
	"fact_identifier": "info._id",
	"fields": [
		{
			"field": "_created",
			"value": "new Date(info._created)"
		},
		{
			"field": "type",
			"value": "'share'"
		},
		{
			"field": "page_id",
			"value": "info.account"
		},
		{
			"field": "user_id",
			"value": "info.user._id"
		},
		{
			"field": "pin_id",
			"value": "info.parent_id"
		},
		{
			"field": "board_id",
			"value": "info.board._id"
		}
	]
},

{
	"mapping_id": "pinterest_repin_pin",
	"info_type": "pinterest_repin",
	"fact_type": "pinterest_pin",
	"fact_identifier": "info.parent_id",
	"fields": []
},

{
	"mapping_id": "pinterest_repin_user",
	"info_type": "pinterest_repin",
	"fact_type": "pinterest_user",
	"fact_identifier": "info.user._id",
	"fields": [
		{
			"field": "_created",
			"value": "new Date(info.user._created)"
		},
		{
			"field": "handle",
			"value": "info.user.screen_name"
		},
		{
			"field": "name",
			"value": "info.user.name"
		},
		{
			"field": "location",
			"value": "info.user.location"
		},
		{
			"field": "followers",
			"value": "info.user.followers"
		},
		{
			"field": "image",
			"value": "info.user.image"
		},
		{
			"field": "like_count",
			"value": "info.user.like_count"
		},
		{
			"field": "pin_count",
			"value": "info.user.pin_count"
		},
		{
			"field": "board_count",
			"value": "info.user.board_count"
		},
		{
			"field": "gender",
			"value": "info.user.gender"
		},
		{
			"field": "url",
			"value": "info.user.url"
		}
	]
}


{
	"mapping_id": "pinterest_repin_board",
	"info_type": "pinterest_repin",
	"fact_type": "pinterest_board",
	"fact_identifier": "info.board._id",
	"fields": [
		{
			"field": "name",
			"value": "info.board.name"
		},
		{
			"field": "description",
			"value": "info.board.description"
		},
		{
			"field": "image",
			"value": "info.board.image"
		},
		{
			"field": "url",
			"value": "info.board.url"
		},
		{
			"field": "pin_count",
			"value": "info.board.pin_count"
		},
		{
			"field": "follower_count",
			"value": "info.board.follower_count"
		},
		{
			"field": "category",
			"value": "info.board.category"
		}
	]
}

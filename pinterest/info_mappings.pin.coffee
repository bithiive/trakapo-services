{
	"mapping_id": "pinterest_pin",
	"info_type": "pinterest_pin",
	"fact_type": "pinterest_pin",
	"fact_identifier": "info._id",
	"fields": [
		{
			"field": "_created",
			"value": "new Date(info._created)"
		},
		{
			"field": "text",
			"value": "info.text"
		},
		{
			"field": "image",
			"value": "info.image"
		},
		{
			"field": "domain",
			"value": "info.domain"
		},
		{
			"field": "link",
			"value": "info.link"
		},
		{
			"field": "page_id",
			"value": "info.account"
		},
		{
			"field": "user_id",
			"value": "( info.user && info.user._id ) || info.account"
		},
		{
			"field": "board_id",
			"value": "( info.board && info.board._id )"
		},
		{
			"field": "like_count",
			"value": "info.like_count || 0"
		},
		{
			"field": "reply_count",
			"value": "info.comment_count || 0"
		},
		{
			"field": "share_count",
			"value": "info.repin_count || 0"
		}
	]
}

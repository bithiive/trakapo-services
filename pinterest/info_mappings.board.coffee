{
	"mapping_id": "pinterest_board",
	"info_type": "pinterest_board",
	"fact_type": "pinterest_board",
	"fact_identifier": "info._id",
	"fields": [
		{
			"field": "_created",
			"value": "new Date(info._created)"
		},
		{
			"field": "name",
			"value": "info.name"
		},
		{
			"field": "description",
			"value": "info.description"
		},
		{
			"field": "image",
			"value": "info.image"
		},
		{
			"field": "url",
			"value": "info.url"
		},
		{
			"field": "category",
			"value": "info.category"
		},
		{
			"field": "page_id",
			"value": "info.account"
		},
		{
			"field": "user_id",
			"value": "( info.user && info.user._id ) || info.account"
		},
		{
			"field": "pin_count",
			"value": "info.pin_count"
		},
		{
			"field": "followers",
			"value": "info.follower_count"
		},
		{
			"field": "summarize",
			"value": "!!info.summarize"
		}
	]
}

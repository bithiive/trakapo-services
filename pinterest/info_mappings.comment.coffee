{
	"mapping_id": "pinterest_comment_interaction",
	"info_type": "pinterest_comment",
	"fact_type": "pinterest_interaction",
	"fact_identifier": "info._id",
	"fields": [
		{
			"field": "_created",
			"value": "new Date(info._created)"
		},
		{
			"field": "type",
			"value": "'reply'"
		},
		{
			"field": "text",
			"value": "info.text"
		},
		{
			"field": "page_id",
			"value": "info.account"
		},
		{
			"field": "user_id",
			"value": "info.user._id"
		},
		{
			"field": "pin_id",
			"value": "info.parent_id"
		}
	]
},

{
	"mapping_id": "pinterest_comment_pin",
	"info_type": "pinterest_comment",
	"fact_type": "pinterest_pin",
	"fact_identifier": "info.parent_id",
	"fields": []
},

{
	"mapping_id": "pinterest_comment_user",
	"info_type": "pinterest_comment",
	"fact_type": "pinterest_user",
	"fact_identifier": "info.user._id",
	"fields": [
		{
			"field": "_created",
			"value": "new Date(info.user._created)"
		},
		{
			"field": "handle",
			"value": "info.user.screen_name"
		},
		{
			"field": "name",
			"value": "info.user.name"
		},
		{
			"field": "location",
			"value": "info.user.location"
		},
		{
			"field": "followers",
			"value": "info.user.followers"
		},
		{
			"field": "image",
			"value": "info.user.image"
		},
		{
			"field": "like_count",
			"value": "info.user.like_count"
		},
		{
			"field": "pin_count",
			"value": "info.user.pin_count"
		},
		{
			"field": "board_count",
			"value": "info.user.board_count"
		},
		{
			"field": "gender",
			"value": "info.user.gender"
		},
		{
			"field": "url",
			"value": "info.user.url"
		}
	]
}

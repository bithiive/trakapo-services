{
	"mapping_id": "pinterest_user",
	"info_type": "pinterest_user",
	"fact_type": "pinterest_user",
	"fact_identifier": "info._id",
	"fields": [
		{
			"field": "_created",
			"value": "new Date(info._created)"
		},
		{
			"field": "handle",
			"value": "info.screen_name"
		},
		{
			"field": "name",
			"value": "info.name"
		},
		{
			"field": "location",
			"value": "info.location"
		},
		{
			"field": "follower_count",
			"value": "info.followers"
		},
		{
			"field": "image",
			"value": "info.image"
		},
		{
			"field": "summarize",
			"value": "!!info.summarize"
		},
		{
			"field": "like_count",
			"value": "info.like_count"
		},
		{
			"field": "pin_count",
			"value": "info.pin_count"
		},
		{
			"field": "board_count",
			"value": "info.board_count"
		},
		{
			"field": "gender",
			"value": "info.gender"
		},
		{
			"field": "url",
			"value": "info.url"
		}
	]
}

# map metric results into the count field
{
	"mapping_id": "pinterest_user_er_counts",
	"info_type": "pinterest_user_er_count",
	"fact_type": "pinterest_user",
	"fact_identifier": "info._id.page_id",
	"fields": [
		{
			"field": "count.{{info._id.type}}",
			"value": "info.count"
		}
	]
}

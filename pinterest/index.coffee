{
	"name": "Pinterest",
	"description": "Pinterest social service",
	"version": "0.1.8",
	"public": false,

	"info_mappings": [
		#!info_mappings.coffee
	],

	"metrics": [
		#!metrics.coffee
	],

	"hooks": {
		#!hooks.coffee
	},

	"fact_settings": {
		#!fact_settings.coffee
	}
}

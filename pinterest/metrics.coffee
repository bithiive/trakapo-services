{
	"metric_id": "type_counts",
	"fact_type": "pinterest_interaction",
	"by_created": true,
	"group": ["page_id", "type"],
	"fields": {
		"count": {
			"mode": "count"
		}
	}
},

{
	"metric_id": "summary",
	"fact_type": "pinterest_user",
	"group": "handle",
	"filter": {
		"summarize": true
	},
	"fields": {
		"followers_start": {
			"mode": "min",
			"input": "follower_count"
		},
		"followers_end": {
			"mode": "max",
			"input": "follower_count"
		},
		"like_sum": {
			"mode": "sum",
			"input": "like_count"
		},
		"like_avg": {
			"mode": "avg",
			"input": "like_count"
		},
		"pin_count_sum": {
			"mode": "sum",
			"input": "pin_count"
		},
		"pin_count_avg": {
			"mode": "avg",
			"input": "pin_count"
		},
		"board_count_sum": {
			"mode": "sum",
			"input": "board_count"
		},
		"board_count_avg": {
			"mode": "avg",
			"input": "board_count"
		},
		"engagement_sum": {
			"mode": "sum",
			"input": "engagement"
		},
		"engagement_avg": {
			"mode": "avg",
			"input": "engagement"
		},
		"engagement_ratio_sum": {
			"mode": "sum",
			"input": "engagement_ratio"
		},
		"engagement_ratio_avg": {
			"mode": "avg",
			"input": "engagement_ratio"
		}
	}
}

{
	"metric_id": "summary",
	"fact_type": "pinterest_board",
	"group": "_id",
	"by_created": true,
	"filter": {
		"summarize": true
	},
	"fields": {
		"pin_count_sum": {
			"mode": "sum",
			"input": "pin_count"
		},
		"pin_count_avg": {
			"mode": "avg",
			"input": "pin_count"
		},
		"followers_start": {
			"mode": "min",
			"input": "followers"
		},
		"followers_end": {
			"mode": "max",
			"input": "followers"
		},
		"reach_sum": {
			"mode": "sum",
			"input": "reach"
		},
		"reach_avg": {
			"mode": "avg",
			"input": "reach"
		},
		"like_sum": {
			"mode": "sum",
			"input": "like_count"
		},
		"like_avg": {
			"mode": "avg",
			"input": "like_count"
		},
		"reply_sum": {
			"mode": "sum",
			"input": "reply_count"
		},
		"reply_avg": {
			"mode": "avg",
			"input": "reply_count"
		},
		"share_sum": {
			"mode": "sum",
			"input": "share_count"
		},
		"share_avg": {
			"mode": "avg",
			"input": "share_count"
		},
		"interaction_sum": {
			"mode": "sum",
			"input": "interaction_count"
		},
		"interaction_avg": {
			"mode": "avg",
			"input": "interaction_count"
		},
		"engagement_sum": {
			"mode": "sum",
			"input": "engagement"
		},
		"engagement_avg": {
			"mode": "avg",
			"input": "engagement"
		},
		"engagement_ratio_sum": {
			"mode": "sum",
			"input": "engagement_ratio"
		},
		"engagement_ratio_avg": {
			"mode": "avg",
			"input": "engagement_ratio"
		}
	}
},

{
	"metric_id": "summary",
	"fact_type": "pinterest_pin",
	"group": "user_id",
	"by_created": true,
	"fields": {
		"reach_sum": {
			"mode": "sum",
			"input": "reach"
		},
		"reach_avg": {
			"mode": "avg",
			"input": "reach"
		},
		"like_sum": {
			"mode": "sum",
			"input": "like_count"
		},
		"like_avg": {
			"mode": "avg",
			"input": "like_count"
		},
		"reply_sum": {
			"mode": "sum",
			"input": "reply_count"
		},
		"reply_avg": {
			"mode": "avg",
			"input": "reply_count"
		},
		"share_sum": {
			"mode": "sum",
			"input": "share_count"
		},
		"share_avg": {
			"mode": "avg",
			"input": "share_count"
		},
		"interaction_sum": {
			"mode": "sum",
			"input": "interaction_count"
		},
		"interaction_avg": {
			"mode": "avg",
			"input": "interaction_count"
		}
	}
}

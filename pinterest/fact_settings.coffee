"pinterest_user": {
	"fields": {
		"boards": {
			"mode": "relation",
			"fact_type": "pinterest_board",
			"has": "many",
			"query": {
				"user_id": "fact._id"
			}
		},

		"followers": {
			"mode": "relation",
			"fact_type": "pinterest_interaction",
			"has": "many",
			"query": {
				"type": "'user_follow'",
				"page_id": "fact.handle"
			}
		},

		# this is now written directly
		# consider using a "map" type to give historic values?
		# "follower_count": {
		# 	"mode": "eval",
		# 	"eval": "relation.count('followers')"
		# },

		# these are mapped in via a metric on interactions, coming through as individual {type=count} info
		# as such, they need to extend the existing object
		"count": {
			"mode": "extend"
		},

		# # this will only be not-null for "summarize=true" pages.
		"engagement": {
			"mode": "eval",
			"condition": "this.summarize",
			"map": {
				"fvc": "relation.sum_field('boards', 'like_count')",
				"rtc": "relation.sum_field('boards', 'share_count')",
				"rec": "relation.sum_field('boards', 'reply_count')",
				"engagement": "(fvc * 1.5) + (rtc * 0.5) + rec"
			},
			"eval": "engagement"
		},

		"engagement_ratio": {
			"mode": "eval",
			"condition": "this.summarize",
			"map": {
				"fvc": "relation.sum_field('boards', 'like_count')",
				"rtc": "relation.sum_field('boards', 'share_count')",
				"rec": "relation.sum_field('boards', 'reply_count')",
				"engagement": "(fvc * 1.5) + (rtc * 0.5) + rec",
				"ratio": "this.follower_count / engagement"
			},
			"eval": "parseFloat(ratio.toFixed(4))"
		},

		"reach": {
			"mode": "eval",
			"map": {
				"share_reach": "relation.sum_field('boards', 'reach')",
				"page_reach": "this.follower_count"
			},
			"eval": "Number(share_reach) + Number(page_reach)"
		}

	}
},

"pinterest_board": {
	"fields": {
		"page": {
			"mode": "relation",
			"fact_type": "pinterest_user",
			"has": "one",
			"query": {
				"_id": "fact.user_id"
			}
		},

		"pins": {
			"mode": "relation",
			"fact_type": "pinterest_pin",
			"has": "many",
			"query": {
				"board_id": "fact._id"
			}
		},

		"like_count": {
			"mode": "eval",
			"eval": "relation.sum_field('pins', 'like_count')"
		},

		"reply_count": {
			"mode": "eval",
			"eval": "relation.sum_field('pins', 'reply_count')"
		},

		"share_count": {
			"mode": "eval",
			"eval": "relation.sum_field('pins', 'share_count')"
		},

		"interaction_count": {
			"mode": "eval",
			"eval":  "relation.sum_field('pins', 'interaction_count')"
		},

		"reach": {
			"mode": "eval",
			"map": {
				"share_reach": "relation.sum_field('pins', 'reach')",
				"page_reach": "this.followers"
			},
			"eval": "Number(share_reach) + Number(page_reach)"
		},

		"engagement": {
			"mode": "eval",
			"map": {
				"fvc": "relation.sum_field('pins', 'like_count')",
				"rtc": "relation.sum_field('pins', 'share_count')",
				"rec": "relation.sum_field('pins', 'reply_count')",
				"engagement": "(fvc * 1.5) + (rtc * 0.5) + rec"
			},
			"eval": "engagement"
		},

		"engagement_ratio": {
			"mode": "eval",
			"map": {
				"fvc": "relation.sum_field('pins', 'like_count')",
				"rtc": "relation.sum_field('pins', 'share_count')",
				"rec": "relation.sum_field('pins', 'reply_count')",
				"engagement": "(fvc * 1.5) + (rtc * 0.5) + rec",
				"ratio": "this.followers / engagement"
			},
			"eval": "parseFloat(ratio.toFixed(4))"
		}
	}
},

"pinterest_pin": {
	"fields": {
		"page": {
			"mode": "relation",
			"fact_type": "pinterest_user",
			"has": "one",
			"query": {
				"_id": "fact.user_id"
			}
		},

		"board": {
			"mode": "relation",
			"fact_type": "pinterest_board",
			"has": "one",
			"query": {
				"_id": "fact.board_id"
			}
		},

		"likes": {
			"mode": "relation",
			"fact_type": "pinterest_interaction",
			"has": "many",
			"query": {
				"type": "'like'",
				"pin_id": "fact._id"
			}
		},

		"like_count": {
			"mode": "eval",
			"eval": "relation.count('likes')"
		},

		"replies": {
			"mode": "relation",
			"fact_type": "pinterest_interaction",
			"has": "many",
			"query": {
				"type": "'reply'",
				"pin_id": "fact._id"
			}
		},

		"reply_count": {
			"mode": "eval",
			"eval": "relation.count('replies')"
		},

		"shares": {
			"mode": "relation",
			"fact_type": "pinterest_interaction",
			"has": "many",
			"query": {
				"type": "'share'",
				"pin_id": "fact._id"
			}
		},

		"share_count": {
			"mode": "eval",
			"eval": "relation.count('shares')"
		},

		"interaction_count": {
			"mode": "eval",
			"eval": "relation.count('replies', 'likes', 'shares')"
		},

		"reach": {
			"mode": "eval",
			"map": {
				"share_reach": "relation.sum_field('shares', 'reach')",
				"page_reach": ["this.page.followers", 0]
			},
			"eval": "Number(share_reach) + Number(page_reach)"
		}
	}
},

"pinterest_interaction": {
	"fields": {
		"_created": {
			"mode": "oldest"
		},

		"page": {
			"mode": "relation",
			"fact_type": "pinterest_user",
			"has": "one",
			"query": {
				"page_id": "fact.page_id"
			}
		},

		"user": {
			"mode": "relation",
			"fact_type": "pinterest_user",
			"has": "one",
			"query": {
				"_id": "fact.user_id"
			}
		},

		"pin": {
			"mode": "relation",
			"fact_type": "pinterest_pin",
			"has": "one",
			"query": {
				"_id": "fact.pin_id"
			}
		}
	}
}

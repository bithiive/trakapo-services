{
	name: "Trakapo Remarketing"
	version: "0.3.2"
	description: "Display different content based on user properties"

	dependencies: ["trakapo-core"]

	icon: 'newspaper'

	hooks:
		# used to ensure counts are kept up to date
		retargeting_internal_type:
			type: 'fact_update'
			events: ['fact/*/ir_content']
			options:
				fact_type: 'ir_content_type'
				fact_identifier: 'data.type'

		# update target when rules are modified
		retargeting_internal_rules:
			type: 'fact_update'
			events: ['fact/*/ir_target_rule']
			options:
				fact_type: 'ir_target'
				fact_identifier: 'data.target_id'

		# used for the content server
		# send updated targets+content
		retargeting_target:
			type: 'url'
			events: ["fact/modify/ir_target"]
			options:
				url: "http://content.trakapo.com:2584/hooker"
				method: "POST"
			# get the rules for this target
			with: ["rules"]

		# send updated session
		retargeting_session:
			type: "url",
			events: ["fact/modify/session"]
			options:
				url: "http://content.trakapo.com:2584/hooker",
				method: "POST"
			with: ["baskets"]

		# these can be grouped because they have no "with" component
		retargeting_other:
			type: "url",
			events: ["fact/modify/user", "fact/modify/device", "fact/modify/ir_content"]
			options:
				url: "http://content.trakapo.com:2584/hooker",
				method: "POST"

		# remove stuff
		retargeting_removal:
			type: "url"
			events: ["fact/remove/device", "fact/remove/ir_content", "fact/remove/ir_target"]
			options:
				url: "http://content.trakapo.com:2584/hooker",
				method: "DELETE"

	fact_settings:
		ir_content_type:
			fields:
				content:
					mode: 'relation'
					fact_type: 'ir_content'
					has: 'many'
					query:
						type: 'fact._id'
				targets:
					mode: 'relation'
					fact_type: 'ir_target'
					has: 'many'
					query:
						types: 'fact._id'
				count:
					mode: 'eval'
					eval: 'fact.content.length'

		ir_target:
			fields:
				rules:
					mode: 'relation'
					fact_type: 'ir_target_rule'
					has: 'many'
					query:
						target_id: 'fact._id'

		ir_target_rule:
			fields:
				target:
					mode: 'relation'
					fact_type: 'ir_target'
					has: 'one'
					query:
						_id: 'fact.target_id'
				content:
					mode: 'relation'
					fact_type: 'ir_content'
					has: 'one'
					query:
						_id: 'fact.content_id'

	tracking_scripts:
		remarketing: ->
			fn = () ->
				ids = T.getIds()

				window.Trakapo_remarketing_callback = (data) ->
					for row in data
						item = Sizzle row.selector
						if item.length is 0
							continue
						# only replace the first item
						item[0].innerHTML = row.body

				script = document.createElement 'script'
				script.async = true
				script.id = 'trakapo-remarketing'
				script.src = "//#{T.settings.url}/content/#{T.siteId}/#{ids.device}/#{ids.session}?callback=Trakapo_remarketing_callback"

				firstScript = document.getElementsByTagName('script')[0]
				firstScript.parentNode.insertBefore script, firstScript

			return "setTimeout(" + fn.toString() + ", 10)"

}

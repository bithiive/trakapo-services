{
	name: "Healthbay",
	version: "1.0.4",
	public: false

	dependencies: ["trakapo-core", "trakapo-commerce"],

	info_mappings: [
		{
		    info_type: "import",
		    fact_type: "basket",
		    mapping_id: "basket_import",
		    fact_identifier: "info.basket.bid",
		    fields: [
		        {field: "_updated", value: "new Date(info.timestamp)"},
		        {field: "user_id", value: "info.user._id"},
		        {field: "sid", value: "info.session"}
		        {field: "did", value: "info.device"}
		        {field: "basket", value: "info.basket.line_items"}
		        {field: "price", value: "info.basket.price || info.basket.prices"},
		        {field: "status.{{info.basket.status}}", value: "new Date(info.timestamp)"}
		        {field: "status.current", value: "info.basket.status"}
		        {field: "imported", value: true}
		    ]
		},
		{
		    info_type: "import",
		    fact_type: "user",
		    mapping_id: "user_import",
		    fact_identifier: "info.user._id",
		    fields: {
		        _updated: "new Date(info.timestamp)",
		        email: "info.user.email",
		        name: "info.user.name",
		        info: "info.user.info",
		        imported: true
		    }
		}
		{
		    info_type: "import",
		    fact_type: "session",
		    mapping_id: "session_import",
		    fact_identifier: "info.session",
		    fields: {
		        _updated: "new Date(info.timestamp)",
		        did: "info.device",
		        actions: "[]",
		        referrer: "null",
		        query: "{}",
		        imported: true
		    }
		}
		{
		    info_type: "import",
		    fact_type: "device",
		    mapping_id: "device_import",
		    fact_identifier: "info.device",
		    fields: {
		        _updated: "new Date(info.timestamp)",
		        user_id: "info.user._id",
		        imported: true
		    }
		}
		
	]

	tracking_goals:
		basket:
			triggers: [
				{type: 'load'}
				{type: 'timeout', timeout: 3000}
			]
			conditions: [
				{
					type: 'eval'
					eval: "typeof Trakapo.viewInfo === 'object' && Trakapo.viewInfo.basket"
				}
			]
			events: [
				{
					type: "basket",
					atomic: true,
					data: "=Trakapo.viewInfo.basket.data",
					restrict: "=restrict(session, key, data)"
				}
			]

		product:
			triggers: [
				{type: 'load'}
				{type: 'timeout', timeout: 3000}
			]
			conditions: [
				{
					type: 'eval'
					eval: "typeof Trakapo.viewInfo === 'object' && Trakapo.viewInfo.product"
				}
			]
			events: [
				{
					type: "track",
					atomic: true,
					data: "=Trakapo.viewInfo.product.data",
					restrict: "=restrict(session, key, data)"
				}
			]

		identify:
			debug: true
			triggers: [
				{
					type: 'load',
					element: 'form#onestepcheckout-form'}
				{
					type: 'submit',
					element: 'form#onestepcheckout-form'
				}
				{
					type: 'event',
					event: 'blur'
					element: 'input'
					processForm: true
				}
			]
			conditions: [
				{
					type: "exists",
					element: "form#onestepcheckout-form",
					debug: true
				},
				{
					type: "contains",
					element: "form#onestepcheckout-form [name=\"billing[email]\"]",
					value: "@",
					debug: true
				}
			]
			events: [
				{
					type: "identify",
					atomic: true,
					data:
						id: "=data.billing.email",
						email: "=data.billing.email",
						name: "=data.billing.firstname + ' ' + data.billing.lastname",
						info:
							first_name: "=data.billing.firstname",
							last_name: "=data.billing.lastname",
							tel: "=data.billing.telephone",
							street_address: "=data.billing.street",
							city: "=data.billing.city",
							postcode: "=data.billing.postcode",
							country: "=data.billing.country_id"
					restrict: "=restrict(session, key, data)"
				}
			]
		identify_on_login:
			debug: true
			triggers: [
				{
					type: "load",
					element: "form#login-form"
				},
				{
					type: "submit",
					element: "form#login-form"
				},
				{
					type: "event",
					event: "blur",
					element: "input",
					processForm: true
				}
			],
			conditions: [
				{
					type: "exists",
					element: "form#login-form",
					debug: true
				},
				{
					type: "contains",
					element: "form#login-form [name=\"login[username]\"]",
					value: "@",
					debug: true
				}
			],
			events: [
				{
					type: "identify",
					atomic: true,
					data:
						id: "=data.login.username",
						email: "=data.login.username"
					restrict: "=restrict(session, key, data)"
				}
			]

		basket_status_checkout: {
			triggers: [
				{
					type: "load"
				}
			],
			conditions: [
				{
					type: "url",
					path: "/checkout/onepage/success"
				}
			],
			events: [
				{
					type: "basket_status",
					atomic: true,
					data: {
						bid: "=Trakapo.viewInfo.basket.data.bid",
						status: "completed"
					},
					restrict: "=restrict(session, key, data)"
				}
			]
		},
}

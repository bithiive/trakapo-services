"tracking_goals": {
	"basket": {
		"triggers": [
			{
				"type": "load"
			},
			{
				"type": "ajax",
				"url": "/checkout/cart/add",
				"ajax_response": true
			}
		],
		"conditions": [
			{

				"type": "eval",
				"eval": "Trakapo.helpers.DeadLegacyBasket() !== false"
			}
		],
		"events": [
			{
				"type": "basket",
				"data": "=Trakapo.helpers.DeadLegacyBasket()",
				"restrict": "=restrict(session, key, data)"
			}
		]
	},
	"product": {
		"triggers": [
			{
				"type": "load"
			}
		],
		"conditions": [
			{
				"type": "eval",
				"eval": "Trakapo.helpers.DeadLegacyProduct() !== false"
			}
		],
		"events": [
			{
				"type": "track",
				"data": {
					"type": "product",
					"action": "=Trakapo.helpers.DeadLegacyProduct()"
				}
			}
		]
	},


	"identify_on_onepage_checkout": {
		"debug": true,
		"triggers": [
			{
				"type": "load",
				"element": "form#opc-address-form-billing"
			},
			{
				"type": "submit",
				"element": "form#opc-address-form-billing"
			},
			{
				"type": "event",
				"event": "blur",
				"element": "input",
				"processForm": true
			}
		],
		"conditions": [
			{
				"type": "exists",
				"element": "form#opc-address-form-billing",
				"debug": true
			},
			{
				"type": "contains",
				"element": "form#opc-address-form-billing [name=\"billing[email]\"]",
				"value": "@",
				"debug": true
			}
		],
		"events": [
			{
				"type": "identify",
				"atomic": true,
				"data": {
					"id": "=data.billing.email",
					"email": "=data.billing.email",
					"name": "=data.billing.firstname + ' ' + data.billing.lastname",
					"info": {
						"first_name": "=data.billing.firstname",
						"last_name": "=data.billing.lastname",
						"tel": "=data.billing.telephone",
						"street_address": "=data.billing.street",
						"city": "=data.billing.city",
						"postcode": "=data.billing.postcode",
						"country": "=data.billing.country_id"
					}
				},
				"restrict": "=restrict(session, key, data)"
			}
		]
	},

	"identify_on_top_login": {
		"debug": true,
		"triggers": [
			{
				"type": "load",
				"element": "form#login-form"
			},
			{
				"type": "submit",
				"element": "form#login-form"
			},
			{
				"type": "event",
				"event": "blur",
				"element": "input",
				"processForm": true
			}
		],
		"conditions": [
			{
				"type": "exists",
				"element": "form#login-form",
				"debug": true
			},
			{
				"type": "contains",
				"element": "form#login-form [name=\"login[username]\"]",
				"value": "@",
				"debug": true
			}
		],
		"events": [
			{
				"type": "identify",
				"atomic": true,
				"data": {
					"id": "=data.login.username",
					"email": "=data.login.username"
				},
				"restrict": "=restrict(session, key, data)"
			}
		]
	},

	"identify_on_newsletter": {
		"debug": true,
		"triggers": [
			{
				"type": "load",
				"element": "form#newsletter-validate-detail"
			},
			{
				"type": "submit",
				"element": "form#newsletter-validate-detail"
			},
			{
				"type": "event",
				"event": "blur",
				"element": "input",
				"processForm": true
			}
		],
		"conditions": [
			{
				"type": "contains",
				"element": "form#newsletter-validate-detail [name=\"email\"]",
				"value": "@",
				"debug": true
			}
		],
		"events": [
			{
				"type": "identify",
				"atomic": true,
				"data": {
					"id": "=data.email",
					"email": "=data.email"
				},
				"restrict": "=restrict(session, key, data)"
			}
		]
	},

	"identify_on_login_ajax": {
		"triggers": [
			{
				"type": "ajax",
				"url": "/signin/json",
				"ajax_response": true
			}
		],
		"conditions": [
			{

				"type": "eval",
				"eval": "data.parsedBody['login%5Busername%5D'] && data.parsedBody['login%5Busername%5D'].indexOf('@') !== -1"
			}
		],
		"events": [
			{
				"type": "identify",
				"atomic": true,
				"data": {
					"id": "=data.parsedBody['login%5Busername%5D']",
					"email": "=data.parsedBody['login%5Busername%5D']"
				},
				"restrict": "=restrict(session, key, data)"
			}
		]
	},

	"basket_status_checkout_a": {
		"triggers": [
			{
				"type": "load"
			}
		],
		"conditions": [
			{
				"type": "url",
				"path": "/checkout/onepage/success"
			}
		],
		"events": [
			{
				"type": "basket_status",
				"atomic": true,
				"data": {
					"bid": "=Trakapo.getIds( ).session",
					"status": "completed"
				},
				"restrict": "=restrict(session, key, data)"
			}
		]
	},

	"basket_status_checkout_b": {
		"triggers": [
			{
				"type": "load"
			}
		],
		"conditions": [
			{
				"type": "url",
				"path": "/onepage/success"
			}
		],
		"events": [
			{
				"type": "basket_status",
				"atomic": true,
				"data": {
					"bid": "=Trakapo.getIds( ).session",
					"status": "completed"
				},
				"restrict": "=restrict(session, key, data)"
			}
		]
	},

	"basket_discount_code": {
		"triggers": [
			{
				"type": "submit",
				"element": "form"
			}
		],
		"conditions": [
			{
				"type": "exists",
				"element": "form#opc-discount-coupon-form"
			}
		],
		"events": [
			{
				"type": "basket_status",
				"atomic": true,
				"data": {
					"bid": "=Trakapo.getIds( ).session",
					"info": {
						"discount_code": "=data.coupon_code"
					}
				},
				"restrict": "=restrict(session, key, data)"
			}
		]
	}
}








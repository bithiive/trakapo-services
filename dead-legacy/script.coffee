# Dead Legacy has jQuery, so we can use it
trakapoHelperFactory = ( ) ->
	###
	# Constants, Maps, Etc
	###
	currencyMap =
		'£': 'GBP'
		'€': 'EUR'
	#	'$': 'USD'
		'S': 'SEK' # 'Skr' is the actual LHS, but we only ever pass the first character

	# Done like this because of a bug in Faction's service processing
	currencyMap[String.fromCharCode 36] = 'USD'

	totalsMap =
		'Subtotal (Incl. Tax)': 'items'
		'Grand Total': 'ordertotal'
		'Delivery': 'shipping'
		'Discount': 'specialdiscount'

	# dumb cart cache to speed up goals
	cartCache =
		'cart': false
		'date': 0

	productCache = null

	###
	# Handle setting up the storage, etc
	###
	storage = localStorage
	if +new Date - ( storage.trakapo_timestamp ? 0 ) > 3600000 # 60 mins expiry
		storage.trakapo_viewedProducts ?= '{}'
		storage.trakapo_currentBasket ?= '{}'
	storage.trakapo_timestamp = +new Date

	###
	# Helper Function - Converts price to Trakapo format
	###
	convertPrice = ( price, exchangeRate = 0 ) ->
		out =
			'local_currency': currencyMap[price.slice 0, 1]
			'local_value': parseFloat (price.match( /[0-9\.]+/ ) or [false])[0]

		if out.local_currency is 'GBP'
			out.value = out.local_value
			delete out.local_currency
		else if exchangeRate > 0
			out.value = ( out.local_value * exchangeRate ).toFixed 2

		out

	###
	# Helper Functions - Setter & Getters for cached products
	###
	getCachedProduct = ( url ) ->
		try
			JSON.parse( storage.trakapo_viewedProducts )[url.match( /[^\/]+$/ )[0]] or 'info': {}
		catch ex
			'info': {}

	setCachedProduct = ( product ) ->
		try
			products = JSON.parse storage.trakapo_viewedProducts
			products[product.url.match( /[^\/]+$/ )[0]] = product
			storage.trakapo_viewedProducts = JSON.stringify products

		product

	###
	# Helper Functions - Converters for various kinds of products
	###
	convertProductView = ( ) ->
		elem = jQuery '#product_addtocart_form'

		if elem.length < 1
			false
		else
			setCachedProduct
				'sku': elem.find( 'input[name="product"]' ).val( )
				'name': elem.find( '.product-title:first' ).text( )
				'url': 'http:' + decodeURIComponent(
					elem.find( '.facebook' ).prop( 'href' ).match( /%2F%2F.*/ )[0]
				)
				'image_url': elem.find( '.product-image img' ).prop 'src'
				'quantity': 0
				'type': 'product'
				'price': convertPrice elem.find( '.price' ).text( )
				'info':
					'legacy_points': parseInt(
						elem.find( '.rewardpoints-product-view-earning' ).text( ).match( /[0-9]+/ )[0]
					)

	convertMinicartProduct = ( productElement ) ->
		productElement = jQuery productElement
		cachedProduct = getCachedProduct productElement.find( '.cart-name a' ).prop 'href'

		cachedProduct.quantity = parseInt(
			productElement.find( '.cart-price' ).text( ).match( /([0-9]+)\ x/ )[1]
		)
		cachedProduct.price = convertPrice productElement.find( '.price' ).text( )
		cachedProduct.info.row_id = productElement.find '.cart-thumbnail a'
			.prop( 'href' ).match( /delete\/id\/([0-9]+)/ )[1]

		setCachedProduct cachedProduct

	convertCartProduct = ( productElement ) ->
		productElement = jQuery productElement
		cachedProduct = getCachedProduct productElement.find( '.product-name a' ).prop 'href'

		cachedProduct.quantity = parseInt productElement.find( 'input[title="Qty"]' ).val( )
		cachedProduct.price = convertPrice productElement.find( '.price' ).text( )
		try
			cachedProduct.info.row_id = productElement.find 'input[title="Qty"]'
				.prop( 'name' ).match( /[0-9]+/ )[0];

		setCachedProduct cachedProduct

	convertCheckoutCartProduct = ( productElement ) ->
		productElement = jQuery productElement
		cachedProduct = getCachedProduct productElement.find( '.product-name' ).attr 'data-url'

		cachedProduct.quantity = parseInt productElement.find( '.a-center' ).text( )
		cachedProduct.price = convertPrice productElement.find( '.price' ).text( )

		setCachedProduct cachedProduct

	###
	# Helper Functions - Converters for various kinds of basket
	###
	convertTotals = ( totals ) ->
		intermediate = {}
		out = {}

		totals.each ( ) ->
			elem = jQuery @
			intermediate[elem.find( '.a-right:not(.last)' ).text( ).trim( )] =
				'price': elem.find( '.price' ).text( )

		# Handle exchange
		if intermediate['Your credit card will be charged for']
			exchangeRate = parseFloat intermediate['Your credit card will be charged for'].match( /[0-9\,]+/ )[0]
			exchangeRate /= parseFloat intermediate['Grand Total'].match( /[0-9\,]+/ )[0]

			out.currency =
				'original': currencyMap[parseFloat intermediate['Grand Total'].slice 0, 1]
				'converted': 'GBP'
				'exchange_rate': exchangeRate

		for k, v of totalsMap when intermediate[k]?
			out[v] = convertPrice intermediate[k].price

		out

	updateCart = ( query, minicart, prodfunc ) ->
		products = jQuery query

		if products.length < 0
			return false
		if minicart and products.length < 3
			basket = {}

		basket = JSON.parse storage.trakapo_currentBasket
		products.each ( ) ->
			product = prodfunc @
			if typeof product.sku isnt 'undefined'
				basket[product.sku] = product

		storage.trakapo_currentBasket = JSON.stringify basket

		out =
			'bid': T.getIds( ).session
			'line_items': []
			'prices':
				'currency': {}
				'items': {}

		# Get actual totals if they're a thing
		totals = jQuery '#checkout-review-table tfoot tr'
		if totals.length
			out.prices = convertTotals totals

		for sku, product of basket
			out.line_items.push product

			if not totals.length
				if product.price.local_currency
					out.prices.currency.original = product.price.local_currency
					out.prices.items.local_currency = product.price.local_currency
					out.prices.items.local_value = parseFloat( product.price.local_value + (
						out.prices.items.local_value or 0
					) ).toFixed 2

				else
					out.prices.currency.original = 'GBP'
					out.prices.currency.converted = 'GBP'
					out.prices.items.value = parseFloat( product.price.value + (
						out.prices.items.value or 0
					) ).toFixed 2

		out

	updateMiniCart = ( ) ->
		updateCart '#mobcart .cart-product', true, convertMinicartProduct

	updateFullCart = ( ) ->
		updateCart '.cart-list > li', false, convertCartProduct

	updateCheckoutCart = ( ) ->
		updateCart '#checkout-review-table > tbody > tr', false, convertCheckoutCartProduct

	###
	# Exported Trakapo Helper Functions
	###
	T.helpers.DeadLegacyProduct = ->
		productCache ?= do convertProductView

	T.helpers.DeadLegacyBasket = ( ) ->
		now = +new Date
		ret = false

		if now - cartCache.date > 1000
			fns = [updateFullCart, updateCheckoutCart, updateMiniCart]
			while ret is false and fns.length
				ret = do do fns.shift

			if ret
				cartCache.date = now
				cartCache.cart = ret

		else
			ret = cartCache.cart

		ret

return "(#{trakapoHelperFactory.toString( )})()"

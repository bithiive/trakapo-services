({
  name: "Dead Legacy",
  version: "2.0.22",
  "public": false,
  dependencies: ["trakapo-core", "trakapo-commerce"],
  "tracking_goals": {
    "basket": {
      "triggers": [
        {
          "type": "load"
        }, {
          "type": "ajax",
          "url": "/checkout/cart/add",
          "ajax_response": true
        }
      ],
      "conditions": [
        {
          "type": "eval",
          "eval": "Trakapo.helpers.DeadLegacyBasket() !== false"
        }
      ],
      "events": [
        {
          "type": "basket",
          "data": "=Trakapo.helpers.DeadLegacyBasket()",
          "restrict": "=restrict(session, key, data)"
        }
      ]
    },
    "product": {
      "triggers": [
        {
          "type": "load"
        }
      ],
      "conditions": [
        {
          "type": "eval",
          "eval": "Trakapo.helpers.DeadLegacyProduct() !== false"
        }
      ],
      "events": [
        {
          "type": "track",
          "data": {
            "type": "product",
            "action": "=Trakapo.helpers.DeadLegacyProduct()"
          }
        }
      ]
    },
    "identify_on_onepage_checkout": {
      "debug": true,
      "triggers": [
        {
          "type": "load",
          "element": "form#opc-address-form-billing"
        }, {
          "type": "submit",
          "element": "form#opc-address-form-billing"
        }, {
          "type": "event",
          "event": "blur",
          "element": "input",
          "processForm": true
        }
      ],
      "conditions": [
        {
          "type": "exists",
          "element": "form#opc-address-form-billing",
          "debug": true
        }, {
          "type": "contains",
          "element": "form#opc-address-form-billing [name=\"billing[email]\"]",
          "value": "@",
          "debug": true
        }
      ],
      "events": [
        {
          "type": "identify",
          "atomic": true,
          "data": {
            "id": "=data.billing.email",
            "email": "=data.billing.email",
            "name": "=data.billing.firstname + ' ' + data.billing.lastname",
            "info": {
              "first_name": "=data.billing.firstname",
              "last_name": "=data.billing.lastname",
              "tel": "=data.billing.telephone",
              "street_address": "=data.billing.street",
              "city": "=data.billing.city",
              "postcode": "=data.billing.postcode",
              "country": "=data.billing.country_id"
            }
          },
          "restrict": "=restrict(session, key, data)"
        }
      ]
    },
    "identify_on_top_login": {
      "debug": true,
      "triggers": [
        {
          "type": "load",
          "element": "form#login-form"
        }, {
          "type": "submit",
          "element": "form#login-form"
        }, {
          "type": "event",
          "event": "blur",
          "element": "input",
          "processForm": true
        }
      ],
      "conditions": [
        {
          "type": "exists",
          "element": "form#login-form",
          "debug": true
        }, {
          "type": "contains",
          "element": "form#login-form [name=\"login[username]\"]",
          "value": "@",
          "debug": true
        }
      ],
      "events": [
        {
          "type": "identify",
          "atomic": true,
          "data": {
            "id": "=data.login.username",
            "email": "=data.login.username"
          },
          "restrict": "=restrict(session, key, data)"
        }
      ]
    },
    "identify_on_newsletter": {
      "debug": true,
      "triggers": [
        {
          "type": "load",
          "element": "form#newsletter-validate-detail"
        }, {
          "type": "submit",
          "element": "form#newsletter-validate-detail"
        }, {
          "type": "event",
          "event": "blur",
          "element": "input",
          "processForm": true
        }
      ],
      "conditions": [
        {
          "type": "contains",
          "element": "form#newsletter-validate-detail [name=\"email\"]",
          "value": "@",
          "debug": true
        }
      ],
      "events": [
        {
          "type": "identify",
          "atomic": true,
          "data": {
            "id": "=data.email",
            "email": "=data.email"
          },
          "restrict": "=restrict(session, key, data)"
        }
      ]
    },
    "identify_on_login_ajax": {
      "triggers": [
        {
          "type": "ajax",
          "url": "/signin/json",
          "ajax_response": true
        }
      ],
      "conditions": [
        {
          "type": "eval",
          "eval": "data.parsedBody['login%5Busername%5D'] && data.parsedBody['login%5Busername%5D'].indexOf('@') !== -1"
        }
      ],
      "events": [
        {
          "type": "identify",
          "atomic": true,
          "data": {
            "id": "=data.parsedBody['login%5Busername%5D']",
            "email": "=data.parsedBody['login%5Busername%5D']"
          },
          "restrict": "=restrict(session, key, data)"
        }
      ]
    },
    "basket_status_checkout_a": {
      "triggers": [
        {
          "type": "load"
        }
      ],
      "conditions": [
        {
          "type": "url",
          "path": "/checkout/onepage/success"
        }
      ],
      "events": [
        {
          "type": "basket_status",
          "atomic": true,
          "data": {
            "bid": "=Trakapo.getIds( ).session",
            "status": "completed"
          },
          "restrict": "=restrict(session, key, data)"
        }
      ]
    },
    "basket_status_checkout_b": {
      "triggers": [
        {
          "type": "load"
        }
      ],
      "conditions": [
        {
          "type": "url",
          "path": "/onepage/success"
        }
      ],
      "events": [
        {
          "type": "basket_status",
          "atomic": true,
          "data": {
            "bid": "=Trakapo.getIds( ).session",
            "status": "completed"
          },
          "restrict": "=restrict(session, key, data)"
        }
      ]
    },
    "basket_discount_code": {
      "triggers": [
        {
          "type": "submit",
          "element": "form"
        }
      ],
      "conditions": [
        {
          "type": "exists",
          "element": "form#opc-discount-coupon-form"
        }
      ],
      "events": [
        {
          "type": "basket_status",
          "atomic": true,
          "data": {
            "bid": "=Trakapo.getIds( ).session",
            "info": {
              "discount_code": "=data.coupon_code"
            }
          },
          "restrict": "=restrict(session, key, data)"
        }
      ]
    }
  },
  tracking_scripts: {
    dead_legacy: function() {
      var trakapoHelperFactory;
      trakapoHelperFactory = function() {

        /*
        				 * Constants, Maps, Etc
         */
        var cartCache, convertCartProduct, convertCheckoutCartProduct, convertMinicartProduct, convertPrice, convertProductView, convertTotals, currencyMap, getCachedProduct, productCache, setCachedProduct, storage, totalsMap, updateCart, updateCheckoutCart, updateFullCart, updateMiniCart, _ref;
        currencyMap = {
          '£': 'GBP',
          '€': 'EUR',
          'S': 'SEK'
        };
        currencyMap[String.fromCharCode(36)] = 'USD';
        totalsMap = {
          'Subtotal (Incl. Tax)': 'items',
          'Grand Total': 'ordertotal',
          'Delivery': 'shipping',
          'Discount': 'specialdiscount'
        };
        cartCache = {
          'cart': false,
          'date': 0
        };
        productCache = null;

        /*
        				 * Handle setting up the storage, etc
         */
        storage = localStorage;
        if (+(new Date) - ((_ref = storage.trakapo_timestamp) != null ? _ref : 0) > 3600000) {
          if (storage.trakapo_viewedProducts == null) {
            storage.trakapo_viewedProducts = '{}';
          }
          if (storage.trakapo_currentBasket == null) {
            storage.trakapo_currentBasket = '{}';
          }
        }
        storage.trakapo_timestamp = +(new Date);

        /*
        				 * Helper Function - Converts price to Trakapo format
         */
        convertPrice = function(price, exchangeRate) {
          var out;
          if (exchangeRate == null) {
            exchangeRate = 0;
          }
          out = {
            'local_currency': currencyMap[price.slice(0, 1)],
            'local_value': parseFloat((price.match(/[0-9\.]+/) || [false])[0])
          };
          if (out.local_currency === 'GBP') {
            out.value = out.local_value;
            delete out.local_currency;
          } else if (exchangeRate > 0) {
            out.value = (out.local_value * exchangeRate).toFixed(2);
          }
          return out;
        };

        /*
        				 * Helper Functions - Setter & Getters for cached products
         */
        getCachedProduct = function(url) {
          var ex;
          try {
            return JSON.parse(storage.trakapo_viewedProducts)[url.match(/[^\/]+$/)[0]] || {
              'info': {}
            };
          } catch (_error) {
            ex = _error;
            return {
              'info': {}
            };
          }
        };
        setCachedProduct = function(product) {
          var products;
          try {
            products = JSON.parse(storage.trakapo_viewedProducts);
            products[product.url.match(/[^\/]+$/)[0]] = product;
            storage.trakapo_viewedProducts = JSON.stringify(products);
          } catch (_error) {}
          return product;
        };

        /*
        				 * Helper Functions - Converters for various kinds of products
         */
        convertProductView = function() {
          var elem;
          elem = jQuery('#product_addtocart_form');
          if (elem.length < 1) {
            return false;
          } else {
            return setCachedProduct({
              'sku': elem.find('input[name="product"]').val(),
              'name': elem.find('.product-title:first').text(),
              'url': 'http:' + decodeURIComponent(elem.find('.facebook').prop('href').match(/%2F%2F.*/)[0]),
              'image_url': elem.find('.product-image img').prop('src'),
              'quantity': 0,
              'type': 'product',
              'price': convertPrice(elem.find('.price').text()),
              'info': {
                'legacy_points': parseInt(elem.find('.rewardpoints-product-view-earning').text().match(/[0-9]+/)[0])
              }
            });
          }
        };
        convertMinicartProduct = function(productElement) {
          var cachedProduct;
          productElement = jQuery(productElement);
          cachedProduct = getCachedProduct(productElement.find('.cart-name a').prop('href'));
          cachedProduct.quantity = parseInt(productElement.find('.cart-price').text().match(/([0-9]+)\ x/)[1]);
          cachedProduct.price = convertPrice(productElement.find('.price').text());
          cachedProduct.info.row_id = productElement.find('.cart-thumbnail a').prop('href').match(/delete\/id\/([0-9]+)/)[1];
          return setCachedProduct(cachedProduct);
        };
        convertCartProduct = function(productElement) {
          var cachedProduct;
          productElement = jQuery(productElement);
          cachedProduct = getCachedProduct(productElement.find('.product-name a').prop('href'));
          cachedProduct.quantity = parseInt(productElement.find('input[title="Qty"]').val());
          cachedProduct.price = convertPrice(productElement.find('.price').text());
          try {
            cachedProduct.info.row_id = productElement.find('input[title="Qty"]').prop('name').match(/[0-9]+/)[0];
          } catch (_error) {}
          return setCachedProduct(cachedProduct);
        };
        convertCheckoutCartProduct = function(productElement) {
          var cachedProduct;
          productElement = jQuery(productElement);
          cachedProduct = getCachedProduct(productElement.find('.product-name').attr('data-url'));
          cachedProduct.quantity = parseInt(productElement.find('.a-center').text());
          cachedProduct.price = convertPrice(productElement.find('.price').text());
          return setCachedProduct(cachedProduct);
        };

        /*
        				 * Helper Functions - Converters for various kinds of basket
         */
        convertTotals = function(totals) {
          var exchangeRate, intermediate, k, out, v;
          intermediate = {};
          out = {};
          totals.each(function() {
            var elem;
            elem = jQuery(this);
            return intermediate[elem.find('.a-right:not(.last)').text().trim()] = {
              'price': elem.find('.price').text()
            };
          });
          if (intermediate['Your credit card will be charged for']) {
            exchangeRate = parseFloat(intermediate['Your credit card will be charged for'].match(/[0-9\,]+/)[0]);
            exchangeRate /= parseFloat(intermediate['Grand Total'].match(/[0-9\,]+/)[0]);
            out.currency = {
              'original': currencyMap[parseFloat(intermediate['Grand Total'].slice(0, 1))],
              'converted': 'GBP',
              'exchange_rate': exchangeRate
            };
          }
          for (k in totalsMap) {
            v = totalsMap[k];
            if (intermediate[k] != null) {
              out[v] = convertPrice(intermediate[k].price);
            }
          }
          return out;
        };
        updateCart = function(query, minicart, prodfunc) {
          var basket, out, product, products, sku, totals;
          products = jQuery(query);
          if (products.length < 0) {
            return false;
          }
          if (minicart && products.length < 3) {
            basket = {};
          }
          basket = JSON.parse(storage.trakapo_currentBasket);
          products.each(function() {
            var product;
            product = prodfunc(this);
            if (typeof product.sku !== 'undefined') {
              return basket[product.sku] = product;
            }
          });
          storage.trakapo_currentBasket = JSON.stringify(basket);
          out = {
            'bid': T.getIds().session,
            'line_items': [],
            'prices': {
              'currency': {},
              'items': {}
            }
          };
          totals = jQuery('#checkout-review-table tfoot tr');
          if (totals.length) {
            out.prices = convertTotals(totals);
          }
          for (sku in basket) {
            product = basket[sku];
            out.line_items.push(product);
            if (!totals.length) {
              if (product.price.local_currency) {
                out.prices.currency.original = product.price.local_currency;
                out.prices.items.local_currency = product.price.local_currency;
                out.prices.items.local_value = parseFloat(product.price.local_value + (out.prices.items.local_value || 0)).toFixed(2);
              } else {
                out.prices.currency.original = 'GBP';
                out.prices.currency.converted = 'GBP';
                out.prices.items.value = parseFloat(product.price.value + (out.prices.items.value || 0)).toFixed(2);
              }
            }
          }
          return out;
        };
        updateMiniCart = function() {
          return updateCart('#mobcart .cart-product', true, convertMinicartProduct);
        };
        updateFullCart = function() {
          return updateCart('.cart-list > li', false, convertCartProduct);
        };
        updateCheckoutCart = function() {
          return updateCart('#checkout-review-table > tbody > tr', false, convertCheckoutCartProduct);
        };

        /*
        				 * Exported Trakapo Helper Functions
         */
        T.helpers.DeadLegacyProduct = function() {
          return productCache != null ? productCache : productCache = convertProductView();
        };
        return T.helpers.DeadLegacyBasket = function() {
          var fns, now, ret;
          now = +(new Date);
          ret = false;
          if (now - cartCache.date > 1000) {
            fns = [updateFullCart, updateCheckoutCart, updateMiniCart];
            while (ret === false && fns.length) {
              ret = fns.shift()();
            }
            if (ret) {
              cartCache.date = now;
              cartCache.cart = ret;
            }
          } else {
            ret = cartCache.cart;
          }
          return ret;
        };
      };
      return "(" + (trakapoHelperFactory.toString()) + ")()";
    }
  }
});

{
    name: "Trakapo Clickmap",
    version: "0.0.2",
    description: "Track which links are clicked on a page",
    icon: 'mouse'

    tracking_goals:
        links:
            triggers: [{type: 'click', element: 'a[href]'}]
            events: [
                {
                    type: "link",
                    atomic: false,
                    data:
                        url: "=location.href",
                        source: "=T.helpers.selector(element)",
                        target: "=element.href"
                }
            ]

    tracking_scripts:
        clicktracking: ->
            fn = () ->
                T.helpers.selector = (el) ->
                    xpath = ''
                    while element and element.nodeType is 1
                        element = element.parentNode
                        siblings = Sizzle('> *', element.parentNode)

                        idx = ''
                        for i, sibling of siblings when sibling is element
                            idx = '[' + (parseInt(i) + 1) + ']'

                        xpath = '/' + element.tagName.toLowerCase() + idx + xpath
                    return xpath

                    names = []
                    while el.parentNode
                        if el.id
                            names.unshift '#' + el.id
                            break
                        else
                            if el is el.ownerDocument.documentElement
                                names.unshift el.tagName
                            else
                                `for (var c=1, e=el; e.previousElementSibling; e = e.previousElementSibling,c++)
                                    names.unshift(el.tagName+":nth-child("+c+")");
                                `
                            el = el.parentNode
                    return names.join ' > '

            return '(' + fn.toString() + ')()'
}



# Sizzle = jQuery


# selector = (element) ->
#     xpath = ''
#     while element and element.nodeType is 1 and element.parentNode?.tagName?
#         siblings = Sizzle('> *', element.parentNode)

#         idx = ''
#         if siblings.length > 1
#             for sibling in siblings when sibling is element
#                 idx = '[' + (parseInt(_i) + 1) + ']'


#         console.log element

#         xpath = '/' + element.tagName.toLowerCase() + idx + xpath
#         element = element.parentNode

#     console.log xpath

#

({
  "name": "Facebook",
  "description": "Facebook social service",
  "version": "0.1.5",
  "public": false,
  "info_mappings": [
    {
      "mapping_id": "facebook_page",
      "info_type": "facebook_page",
      "fact_type": "facebook_page",
      "fact_identifier": "info._id",
      "fields": [
        {
          "field": "handle",
          "value": "info.screen_name"
        }, {
          "field": "name",
          "value": "info.name"
        }, {
          "field": "location",
          "value": "info.location"
        }, {
          "field": "followers",
          "value": "info.followers"
        }, {
          "field": "engagement",
          "value": "info.engagement"
        }, {
          "field": "image",
          "value": "info.image"
        }, {
          "field": "summarize",
          "value": "!!info.summarize"
        }
      ]
    }, {
      "mapping_id": "facebook_page_er_counts",
      "info_type": "facebook_page_er_count",
      "fact_type": "facebook_page",
      "fact_identifier": "info._id.page_id",
      "fields": [
        {
          "field": "count.{{info._id.type}}",
          "value": "info.count"
        }
      ]
    }, {
      "mapping_id": "facebook_page_metric_engagement",
      "info_type": "facebook_page",
      "fact_type": "facebook_metric",
      "fact_identifier": "info._id + '-engagement-day'",
      "fields": [
        {
          "field": "name",
          "value": "'Engagement'"
        }, {
          "field": "description",
          "value": "''"
        }, {
          "field": "page_id",
          "value": "info._id"
        }, {
          "field": "object",
          "value": "info._id"
        }, {
          "field": "metric",
          "value": "'engagement'"
        }, {
          "field": "period",
          "value": "'day'"
        }
      ]
    }, {
      "mapping_id": "facebook_page_metric_values_engagement",
      "info_type": "facebook_page",
      "fact_type": "facebook_metric_value",
      "fact_identifier": "info._id + '-engagement-day-' + (info.historic || (new Date()).toISOString())",
      "fields": [
        {
          "field": "_created",
          "value": "new Date(info.historic || new Date())"
        }, {
          "field": "value",
          "value": "info.engagement"
        }, {
          "field": "object",
          "value": "info._id"
        }, {
          "field": "metric",
          "value": "'engagement'"
        }, {
          "field": "period",
          "value": "'day'"
        }
      ]
    }, {
      "mapping_id": "facebook_page_metric_followers",
      "info_type": "facebook_page",
      "fact_type": "facebook_metric",
      "fact_identifier": "info._id + '-followers-all_time'",
      "fields": [
        {
          "field": "name",
          "value": "'Followers'"
        }, {
          "field": "description",
          "value": "''"
        }, {
          "field": "page_id",
          "value": "info._id"
        }, {
          "field": "object",
          "value": "info._id"
        }, {
          "field": "metric",
          "value": "'followers'"
        }, {
          "field": "period",
          "value": "'all_time'"
        }
      ]
    }, {
      "mapping_id": "facebook_page_metric_values_followers",
      "info_type": "facebook_page",
      "fact_type": "facebook_metric_value",
      "fact_identifier": "info._id + '-followers-all_time-' + (info.historic || (new Date()).toISOString())",
      "fields": [
        {
          "field": "_created",
          "value": "new Date(info.historic || new Date())"
        }, {
          "field": "value",
          "value": "info.followers"
        }, {
          "field": "object",
          "value": "info._id"
        }, {
          "field": "metric",
          "value": "'followers'"
        }, {
          "field": "period",
          "value": "'all_time'"
        }
      ]
    }, {
      "mapping_id": "facebook_status",
      "info_type": "facebook_status",
      "fact_type": "facebook_status",
      "fact_identifier": "info._id",
      "fields": [
        {
          "field": "_created",
          "value": "new Date(info.timestamp)"
        }, {
          "field": "text",
          "value": "info.text"
        }, {
          "field": "page_id",
          "value": "info.account"
        }, {
          "field": "user_id",
          "value": "info.from._id"
        }, {
          "field": "like_count",
          "value": "info.like_count || 0"
        }, {
          "field": "reply_count",
          "value": "info.reply_count || 0"
        }, {
          "field": "share_count",
          "value": "info.share_count || 0"
        }
      ]
    }, {
      "mapping_id": "facebook_comment_interaction",
      "info_type": "facebook_comment",
      "fact_type": "facebook_interaction",
      "fact_identifier": "info._id",
      "fields": [
        {
          "field": "_created",
          "value": "new Date(info.timestamp)"
        }, {
          "field": "type",
          "value": "'reply'"
        }, {
          "field": "text",
          "value": "info.text"
        }, {
          "field": "page_id",
          "value": "info.account"
        }, {
          "field": "user_id",
          "value": "info.from._id"
        }, {
          "field": "status_id",
          "value": "info.parent_id"
        }
      ]
    }, {
      "mapping_id": "facebook_comment_status",
      "info_type": "facebook_comment",
      "fact_type": "facebook_status",
      "fact_identifier": "info.parent_id",
      "fields": []
    }, {
      "mapping_id": "facebook_comment_page",
      "info_type": "facebook_comment",
      "fact_type": "facebook_page",
      "fact_identifier": "info.from._id",
      "fields": [
        {
          "field": "name",
          "value": "info.from.name"
        }
      ]
    }, {
      "mapping_id": "facebook_like_interaction",
      "info_type": "facebook_like",
      "fact_type": "facebook_interaction",
      "fact_identifier": "info._id",
      "fields": [
        {
          "field": "_created",
          "value": "new Date(info.timestamp)"
        }, {
          "field": "type",
          "value": "'like'"
        }, {
          "field": "page_id",
          "value": "info.account"
        }, {
          "field": "user_id",
          "value": "info.user._id"
        }, {
          "field": "status_id",
          "value": "info.parent_id"
        }
      ]
    }, {
      "mapping_id": "facebook_like_status",
      "info_type": "facebook_like",
      "fact_type": "facebook_status",
      "fact_identifier": "info.parent_id",
      "fields": []
    }, {
      "mapping_id": "facebook_like_page",
      "info_type": "facebook_like",
      "fact_type": "facebook_page",
      "fact_identifier": "info.user._id",
      "fields": [
        {
          "field": "handle",
          "value": "info.user.url.split('/')[3] === 'pages' ? info.user.url.split('/')[4] : info.user.url.split('/')[3]"
        }, {
          "field": "url",
          "value": "info.user.url"
        }, {
          "field": "name",
          "value": "info.user.name"
        }, {
          "field": "image",
          "value": "info.user.picture"
        }
      ]
    }, {
      "mapping_id": "facebook_metric_values",
      "info_type": "facebook_metric",
      "fact_type": "facebook_metric_value",
      "fact_identifier": "info._id.object + '-' + info._id.metric + '-' + info._id.period + '-' + info._id.timestamp",
      "fields": [
        {
          "field": "_created",
          "value": "new Date(info._id.timestamp)"
        }, {
          "field": "value",
          "value": "info.value"
        }, {
          "field": "object",
          "value": "info._id.object"
        }, {
          "field": "metric",
          "value": "info._id.metric"
        }, {
          "field": "period",
          "value": "info._id.period"
        }
      ]
    }, {
      "mapping_id": "facebook_metric",
      "info_type": "facebook_metric",
      "fact_type": "facebook_metric",
      "fact_identifier": "info._id.object + '-' + info._id.metric + '-' + info._id.period",
      "fields": [
        {
          "field": "name",
          "value": "info.name"
        }, {
          "field": "description",
          "value": "info.description"
        }, {
          "field": "page_id",
          "value": "info.account"
        }, {
          "field": "object",
          "value": "info._id.object"
        }, {
          "field": "metric",
          "value": "info._id.metric"
        }, {
          "field": "period",
          "value": "info._id.period"
        }
      ]
    }
  ],
  "metrics": [
    {
      "metric_id": "type_counts",
      "fact_type": "facebook_interaction",
      "by_created": true,
      "backfill_count": 5,
      "group": ["page_id", "type"],
      "fields": {
        "count": {
          "mode": "count"
        }
      }
    }, {
      "metric_id": "summary",
      "fact_type": "facebook_page",
      "filter": {
        "summarize": true
      },
      "project": {
        "followers": 1,
        "engagement_ratio": 1
      }
    }, {
      "metric_id": "summary",
      "fact_type": "facebook_status",
      "group": "page_id",
      "by_created": true,
      "filter": {
        "text": {
          "$ne": null
        }
      },
      "fields": {
        "count": {
          "mode": "count"
        },
        "reply_sum": {
          "mode": "sum",
          "input": "reply_count"
        },
        "reply_avg": {
          "mode": "avg",
          "input": "reply_count"
        },
        "like_sum": {
          "mode": "sum",
          "input": "like_count"
        },
        "like_avg": {
          "mode": "avg",
          "input": "like_count"
        },
        "share_sum": {
          "mode": "sum",
          "input": "share_count"
        },
        "share_avg": {
          "mode": "avg",
          "input": "share_count"
        }
      }
    }, {
      "metric_id": "summary_self",
      "fact_type": "facebook_status",
      "backfill_count": 5,
      "group": "page_id",
      "filter": {
        "text": {
          "$ne": null
        },
        "self": true
      },
      "by_created": true,
      "fields": {
        "count": {
          "mode": "count"
        },
        "reply_sum": {
          "mode": "sum",
          "input": "reply_count"
        },
        "reply_avg": {
          "mode": "avg",
          "input": "reply_count"
        },
        "like_sum": {
          "mode": "sum",
          "input": "like_count"
        },
        "like_avg": {
          "mode": "avg",
          "input": "like_count"
        },
        "share_sum": {
          "mode": "sum",
          "input": "share_count"
        },
        "share_avg": {
          "mode": "avg",
          "input": "share_count"
        }
      }
    }, {
      "metric_id": "numbers",
      "fact_type": "facebook_metric_value",
      "by_created": true,
      "backfill_count": 3,
      "filter": {
        "period": "day",
        "value": {
          "$type": 16
        }
      },
      "group": ["object", "metric"],
      "fields": {
        "sum": {
          "mode": 'sum',
          "input": 'value'
        },
        "mean": {
          "mode": 'avg',
          "input": 'value'
        },
        "min": {
          "mode": 'min',
          "input": 'value'
        },
        "max": {
          "mode": 'max',
          "input": 'value'
        }
      }
    }, {
      "metric_id": "followers",
      "fact_type": "facebook_metric_value",
      "by_created": true,
      "backfill_count": 3,
      "filter": {
        "period": 'all_time',
        "metric": 'followers'
      },
      "group": 'object',
      "fields": {
        "start": {
          "mode": 'last',
          "input": 'value'
        },
        "end": {
          "mode": 'first',
          "input": 'value'
        },
        "min": {
          "mode": 'min',
          "input": 'value'
        },
        "max": {
          "mode": 'max',
          "input": 'value'
        }
      }
    }, {
      "metric_id": "reach",
      "fact_type": "facebook_metric_value",
      "by_created": true,
      "backfill_count": 3,
      "filter": {
        "period": 'week',
        "metric": 'page_impressions_unique'
      },
      "group": 'object',
      "fields": {
        "sum": {
          "mode": 'sum',
          "input": 'value'
        },
        "mean": {
          "mode": 'avg',
          "input": 'value'
        },
        "min": {
          "mode": 'min',
          "input": 'value'
        },
        "max": {
          "mode": 'max',
          "input": 'value'
        }
      }
    }
  ],
  "hooks": {},
  "fact_settings": {
    "facebook_page": {
      "fields": {
        "statuses": {
          "mode": "relation",
          "fact_type": "facebook_status",
          "has": "many",
          "query": {
            "page_id": "fact._id"
          }
        },
        "count": {
          "mode": "extend"
        },
        "engagement_ratio": {
          "mode": "eval",
          "condition": "fact.summarize",
          "eval": "parseFloat((fact.followers / fact.engagement).toFixed(4))"
        },
        "reach": {
          "mode": "relation",
          "fact_type": "facebook_metric",
          "has": "many",
          "query": {
            "object": "fact._id",
            "metric": "'page_impressions_unique'",
            "period": "'week'"
          }
        },
        "engagement_vals": {
          "mode": "relation",
          "fact_type": "facebook_metric",
          "has": "many",
          "query": {
            "object": "fact._id",
            "metric": "'engagement'",
            "period": "'week'"
          }
        },
        "followers_vals": {
          "mode": "relation",
          "fact_type": "facebook_metric",
          "has": "many",
          "query": {
            "object": "fact._id",
            "metric": "'followers'",
            "period": "'all_time'"
          }
        }
      }
    },
    "facebook_status": {
      "fields": {
        "self": {
          "mode": "eval",
          "eval": "fact.page_id == fact.user_id"
        },
        "page": {
          "mode": "relation",
          "fact_type": "facebook_page",
          "has": "one",
          "query": {
            "_id": "fact.page_id"
          }
        },
        "likes": {
          "mode": "relation",
          "fact_type": "facebook_interaction",
          "has": "many",
          "query": {
            "type": "'like'",
            "status_id": "fact._id"
          }
        },
        "like_count": {
          "mode": "eval",
          "eval": "relation.count('likes')"
        },
        "replies": {
          "mode": "relation",
          "fact_type": "facebook_interaction",
          "has": "many",
          "query": {
            "type": "'reply'",
            "status_id": "fact._id"
          }
        },
        "reply_count": {
          "mode": "eval",
          "eval": "relation.count('replies')"
        },
        "interaction_count": {
          "mode": "eval",
          "eval": "relation.count('replies', 'likes')"
        }
      }
    },
    "facebook_interaction": {
      "fields": {
        "page": {
          "mode": "relation",
          "fact_type": "facebook_page",
          "has": "one",
          "query": {
            "_id": "fact.page_id"
          }
        },
        "user": {
          "mode": "relation",
          "fact_type": "facebook_page",
          "has": "one",
          "query": {
            "_id": "fact.user_id"
          }
        },
        "status": {
          "mode": "relation",
          "fact_type": "facebook_status",
          "has": "one",
          "query": {
            "_id": "fact.status_id"
          }
        }
      }
    },
    "facebook_metric": {
      "fields": {
        "values": {
          "mode": "relation",
          "fact_type": "facebook_metric_value",
          "has": "many",
          "query": {
            "object": "fact.object",
            "metric": "fact.metric",
            "period": "fact.period"
          }
        }
      }
    }
  }
});

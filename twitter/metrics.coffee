{
	metric_id: "type_counts"
	fact_type: "twitter_interaction"
	by_created: true
	group: ['page_id', 'type']
	fields:
		count:
			mode: 'count'
}

{
	metric_id: "summary",
	fact_type: "twitter_page",
	filter:
		summarize: true
	group: ['_id']
	fields:
		followers:
			mode: 'max'
			input: 'followers'
		followers_start:
			mode: 'min'
			input: 'followers'
		engagement_ratio:
			mode: 'avg'
			input: 'engagement_ratio'
		count_mention:
			mode: 'sum'
			input: 'count.mention'
		count_reply:
			mode: 'sum'
			input: 'count.reply'
		count_favorite:
			mode: 'sum'
			input: 'count.favorite'
		count_retweet:
			mode: 'sum'
			input: 'count.retweet'
		count_follow:
			mode: 'sum'
			input: 'count.follow'
}

{
	metric_id: 'summary'
	fact_type: 'twitter_status'
	group: 'page_id'
	by_created: true
	backfill_count: 3
	filter:
		reach: $gte: 0
	fields:
		count: mode: 'count'
		reach_sum: mode: 'sum', input: 'reach'
		reach_avg: mode: 'avg', input: 'reach'

		reply_sum: mode: 'sum', input: 'reply_count'
		reply_avg: mode: 'avg', input: 'reply_count'

		share_sum: mode: 'sum', input: 'share_count'
		share_avg: mode: 'avg', input: 'share_count'

		share_poll_sum: mode: 'sum', input: 'share_count_poll'
		share_poll_avg: mode: 'avg', input: 'share_count_poll'

		like_poll_sum: mode: 'sum', input: 'like_count_poll'
		like_poll_avg: mode: 'avg', input: 'like_count_poll'
}

{
	mapping_id: "twitter_page",
	info_type: "twitter_page",
	fact_type: "twitter_page",
	fact_identifier: "info._id",
	fields: {
		_created: 'new Date(info.user_since)'
		handle: "info.screen_name"
		name: "info.name"
		location: "info.location"
		followers: "info.followers"
		image: "info.image"
		summarize: "info.summarize == false ? false : true"
	}
}
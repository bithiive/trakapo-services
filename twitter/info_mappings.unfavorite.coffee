{
	mapping_id: "twitter_unfavorite_interaction"
	info_type: "twitter_unfavorite"
	fact_type: "twitter_interaction",
	fact_identifier: "info.account + '-' + info.type + '-' + info.user._id + '-' + info._id"
	fields: {
		_created: 'new Date(info.timestamp)'
		type: 'info.type'
		page: 'info.account'
		user: 'info.user._id'
	}
}
{
	mapping_id: "twitter_unfavorite_status",
	info_type: "twitter_unfavorite"
	fact_type: "twitter_status"
	fact_identifier: "info._id"
	fields: {}
}
{
	mapping_id: "twitter_unfavorite_page",
	info_type: "twitter_unfavorite"
	fact_type: "twitter_page",
	fact_identifier: "info.user._id",
	fields: {
		handle: "info.user.screen_name"
		name: "info.user.name"
		location: "info.user.location"
		followers: "info.user.followers"
		lang: 'info.user.lang'
		image: "info.user.image"
	}
}

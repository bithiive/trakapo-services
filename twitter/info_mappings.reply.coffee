{
	mapping_id: "twitter_reply_interaction"
	info_type: "twitter_reply"
	fact_type: "twitter_interaction",
	fact_identifier: "info.account + '-' + info.type + '-' + info.user._id + '-' + info._id"
	fields: {
		type: 'info.type'
		text: 'info.text'
		_created: 'new Date(info.timestamp)'
		page_id: 'info.account'
		user_id: 'info.user._id'
		status_id: 'info.parent_id'
	}
}
{
	mapping_id: "twitter_reply_status",
	info_type: "twitter_reply"
	fact_type: "twitter_status"
	fact_identifier: "info.parent_id"
	fields: {}
}
{
	mapping_id: "reply_page",
	info_type: "reply"
	fact_type: "twitter_page",
	fact_identifier: "info.user._id",
	fields: {
		handle: "info.user.screen_name"
		name: "info.user.name"
		location: "info.user.location"
		followers: "info.user.followers"
		lang: 'info.user.lang'
		image: "info.user.image"
	}
}

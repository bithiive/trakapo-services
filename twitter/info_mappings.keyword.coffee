{
	mapping_id: "twitter_keyword_count",
	info_type: "twitter_keyword",
	fact_type: "twitter_keyword"
	fact_identifier: "info.keyword.replace(/[^a-z0-9_]+/gi, '_')", # certain chars are not (easily) queryable
	fields: {
		keyword: "info.keyword",
		count: 1
	}
}

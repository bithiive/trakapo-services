{
	"name": "Twitter",
	"description": "Twitter social service",
	"version": "0.1.1",
	"public": false,

	"info_mappings": [
		#!info_mappings.coffee
	],

	"metrics": [
		#!metrics.coffee
	],

	"fact_settings": {
		#!fact_settings.coffee
	}
}

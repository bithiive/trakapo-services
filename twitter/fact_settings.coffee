twitter_page:
	fields:
		statuses:
			mode: 'relation'
			fact_type: 'twitter_status'
			has: 'many'
			query:
				page_id: 'fact._id'

		favorites:
			mode: 'relation'
			fact_type: 'twitter_interaction'
			has: 'many'
			query:
				page_id: 'fact._id'
				type: '"favorite"'
				_created: 'x={$gt: new Date(new Date().setDate(1) - (Date.now() % 86400000))}'

		retweets:
			mode: 'relation'
			fact_type: 'twitter_interaction'
			has: 'many'
			query:
				page_id: 'fact._id'
				type: '"retweet"'
				_created: 'x={$gt: new Date(new Date().setDate(1) - (Date.now() % 86400000))}'

		replies:
			mode: 'relation'
			fact_type: 'twitter_interaction'
			has: 'many'
			query:
				page_id: 'fact._id'
				type: '"reply"'
				_created: 'x={$gt: new Date(new Date().setDate(1) - (Date.now() % 86400000))}'

		# these are mapped in via a metric on interactions, coming through as individual {type=count} info
		# as such, they need to extend the existing object
		count:
			mode: 'eval'
			map:
				fvc: 'relation.count("favorites")'
				rtc: 'relation.count("retweets")'
				rec: 'relation.count("replies")'
			eval: 'x = {favorite: fvc, retweet: rtc, reply: rec}'

		# this will only be not-null for "summarize=true" pages.
		engagement:
			mode: 'eval'
			condition: "this.summarize"
			map:
				fvc: 'relation.count("favorites")'
				rtc: 'relation.count("retweets")'
				rec: 'relation.count("replies")'
			eval: '(fvc * 1.5) + (rtc * 0.5) + rec'

		engagement_ratio:
			mode: 'eval'
			condition: "this.summarize"
			map:
				# have to recalculate this, annoyingly...
				fvc: 'relation.count("favorites")'
				rtc: 'relation.count("retweets")'
				rec: 'relation.count("replies")'
				engagement: '(fvc * 1.5) + (rtc * 0.5) + rec'
				ratio: 'parseFloat(this.followers / engagement)'
			eval: 'parseFloat(ratio.toFixed(4))'

twitter_status:
	fields:
		page:
			mode: 'relation'
			fact_type: 'twitter_page'
			has: 'one'
			query:
				_id: 'fact.page_id'

		shares:
			mode: 'relation'
			fact_type: 'twitter_interaction'
			has: 'many'
			query:
				type: '"retweet"'
				status_id: 'fact._id'
		share_count:
			mode: 'eval'
			eval: 'relation.count("shares")'
		reach:
			mode: 'eval'
			map:
				share_reach: 'relation.sum_field("shares", "reach")'
				text: ['this.text', '']
				base: ['this.base_reach', 0]
				page_reach: ['(text.toString().charAt(0) == "@" ? 1 : (base || this.page.followers))', 0]
			eval: 'Number(share_reach) + Number(page_reach)'

		likes:
			mode: 'relation'
			fact_type: 'twitter_interaction'
			has: 'many'
			query:
				type: '"favorite"'
				status_id: 'fact._id'
		like_count:
			mode: 'eval'
			eval: 'relation.count("likes")'

		replies:
			mode: 'relation'
			fact_type: 'twitter_interaction'
			has: 'many'
			query:
				type: '"reply"'
				status_id: 'fact._id'
		reply_count:
			mode: 'eval'
			eval: 'relation.count("replies")'

		interaction_count:
			mode: 'eval'
			eval: 'relation.count("likes", "replies", "shares")'

twitter_interaction:
	fields:
		page:
			mode: 'relation'
			fact_type: 'twitter_page'
			has: 'one'
			query:
				_id: 'fact.page_id'
		user:
			mode: 'relation'
			fact_type: 'twitter_page'
			has: 'one'
			query:
				_id: 'fact.user_id'
		status:
			mode: 'relation'
			fact_type: 'twitter_status'
			has: 'one'
			query:
				_id: 'fact.status_id'

twitter_keyword:
	fields:
		count:
			mode: 'inc'

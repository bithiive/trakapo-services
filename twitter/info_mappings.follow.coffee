{
	mapping_id: "twitter_follow_interaction",
	info_type: "twitter_follow",
	fact_type: "twitter_interaction",
	fact_identifier: "info.account + '-' + info.type + '-' + info._id"
	fields: {
		_created: 'new Date(info.timestamp)'
		type: 'info.type'
		page_id: 'info.account'
		user_id: 'info._id'
	}
}
{
	mapping_id: "twitter_follow_page",
	info_type: "twitter_follow"
	fact_type: "twitter_page",
	fact_identifier: "info._id",
	fields: {
		handle: "info.screen_name"
		name: "info.name"
		location: "info.location"
		followers: "info.followers"
		lang: 'info.lang'
		image: "info.image"
	}
}

{
	mapping_id: "twitter_status",
	info_type: "twitter_status",
	fact_type: "twitter_status"
	fact_identifier: "info._id",
	fields: [
		{ field: "_created", value: "new Date(info.timestamp)" }
		{ field: "text", value: "info.text" }
		{ field: "page_id", value: 'info.account' }
		{ field: "user_id", value: 'info.account' }
		{ field: "base_reach", value: "info.user.reach" }
		{ field: "like_count_poll", value: "info.like_count" }
		{ field: "share_count_poll", value: "info.share_count" }
	]
}

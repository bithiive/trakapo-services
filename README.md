Trakapo Services Definitions
============================

This repo contains all the Trakapo service definitions. These decide how data is processed and stored in faction, Goals to be tested for in the Trakapo Script as well as the layout of the Trakapo UI.

To update a service, modify the appropriate coffeescript files in a subfolder and make sure to remember to bump the version number up. Next run `cake fullbuild` from the root directory and commit and push the repo to bitbucket.

If you're creating a site specific or beta service, ensure you set the public property to false so as to not have it show up on the Trakapo user interface service selection screen.

Once the updates services are live you need to trigger a service update on any accounts you wish to use the updated service on. This is currently ususally achieved using the faction-console, but this should be possible using the Trakapo UI soon enough.
 

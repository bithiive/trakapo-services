({
  "name": "Trakapo Core",
  "version": "1.2.5",
  "description": "Primary configuration for Trakapo accounts",
  "icon": "line-graph",
  info_mappings: [
    {
      mapping_id: "device",
      fact_type: "device",
      fact_identifier: "info.did",
      info_type: "track",
      fields: {
        device: "info.device",
        user_id: "info.id || ('undefined' !== typeof fact ? fact.user_id : null)",
        sids: "info.sid"
      }
    }, {
      mapping_id: "user",
      fact_type: "user",
      fact_identifier: "info.id",
      info_type: "track",
      fields: {
        last_sid: "info.sid",
        last_did: "info.did",
        sids: "info.sid",
        dids: "info.did"
      }
    }, {
      mapping_id: "sessions",
      fact_type: "session",
      fact_identifier: "info.sid",
      info_type: "track",
      fields: {
        did: "info.did",
        dids: "info.did",
        actions: "info.action"
      }
    }, {
      mapping_id: "device",
      fact_type: "device",
      fact_identifier: "info.did",
      info_type: "identify",
      fields: {
        user_id: "info.id",
        sids: "info.sid"
      }
    }, {
      mapping_id: "user",
      fact_type: "user",
      fact_identifier: "info.id",
      info_type: "identify",
      fields: {
        email: "info.email || fact.email",
        name: "info.name || fact.name",
        info: "info.info"
      }
    }, {
      mapping_id: "session",
      fact_type: "session",
      fact_identifier: "info.sid",
      info_type: "identify",
      conditions: ["fact.user_id != info.id"],
      fields: {
        user_id: "info.id",
        actions: {
          type: '"identify"',
          id: 'info.id',
          email: 'info.email',
          name: 'info.name',
          info: "info.info"
        }
      }
    }, {
      mapping_id: "fields",
      fact_type: "fields",
      fact_identifier: "info.type",
      info_type: "fields",
      fields: {
        field_paths: "info.field_paths",
        field_counts: "info.field_paths",
        field_types: "info.field_types"
      }
    }, {
      mapping_id: "errors",
      fact_type: "errors",
      fact_identifier: "Date.now() + '-' + info.sid",
      info_type: "error",
      fields: {
        sid: "info.sid",
        source: "info.source",
        error: "info.error",
        info: "info.extra"
      }
    }
  ],
  info_schemas: {
    identify: {
      fields: [
        {
          field: 'id',
          required: true,
          cast: 'string | lowercase'
        }
      ]
    },
    track: {
      fields: [
        {
          field: 'action.type',
          type: 'string',
          required: true
        }, {
          field: 'sid',
          cast: 'string',
          required: true
        }, {
          field: 'did',
          cast: 'string',
          required: true
        }, {
          field: 'user_id',
          cast: 'string | lowercase'
        }, {
          field: 'id',
          cast: 'string | lowercase'
        }
      ]
    }
  },
  metrics: [
    {
      "fact_type": "user",
      "metric_id": "most_improved",
      "project": {
        "score.today": 1
      },
      "sort": "-score.change.day.absolute",
      "limit": 50
    }, {
      "fact_type": "session",
      "metric_id": "unique_visitors",
      "fields": {
        "count": {
          "mode": "count"
        }
      }
    }, {
      "fact_type": "session",
      "metric_id": "action_counts",
      "unwind": "actions",
      "filter": {
        "actions._time": "$bucket"
      },
      "group": "actions._value.type",
      "fields": {
        "count": {
          "mode": "count"
        }
      }
    }, {
      "fact_type": "session",
      "metric_id": "popular_pages",
      "unwind": "actions",
      "filter": {
        "actions._value.type": "page",
        "actions._time": "$bucket"
      },
      "group": "actions._value.url",
      "fields": {
        "count": {
          "mode": "count"
        }
      },
      "sort": "-count",
      "limit": 50
    }, {
      "fact_type": "session",
      "metric_id": "referrer_domains",
      "filter": {
        "referrer_domain": {
          "$exists": 1
        }
      },
      "group": "referrer_domain",
      "fields": {
        "count": {
          "mode": "count"
        }
      },
      "sort": "-count",
      "limit": 50
    }, {
      "fact_type": "session",
      "metric_id": "referrer_urls",
      "group": "referrer",
      "fields": {
        "count": {
          "mode": "count"
        }
      },
      "sort": "-count",
      "limit": 50
    }, {
      "fact_type": "device",
      "metric_id": "top_countries",
      "filter": {
        "location.country_name": {
          "$exists": true
        }
      },
      "group": "location.country_name",
      "fields": {
        "count": {
          "mode": "count"
        }
      },
      "sort": "-count",
      "limit": 50
    }, {
      "fact_type": "device",
      "metric_id": "top_cities",
      "filter": {
        "location.city": {
          "$exists": true
        }
      },
      "group": "location.city",
      "fields": {
        "count": {
          "mode": "count"
        },
        "country": {
          "mode": "first",
          "input": "location.country_name"
        }
      },
      "sort": "-count",
      "limit": 50
    }, {
      "fact_type": "session",
      "metric_id": "pages",
      "unwind": "actions",
      "filter": {
        "actions._value.type": "page",
        "actions._time": "$bucket"
      },
      "group": "actions._value.url",
      "fields": {
        "count": {
          "mode": "count"
        }
      },
      "sort": "-count"
    }
  ],
  fact_settings: {
    device: {
      fields: {
        location: {
          mode: 'eval',
          condition: "'undefined' == typeof fact.location",
          "eval": 'fact.location ? fact.location : http("http://freegeoip.net/json/" + fact.device.ip)'
        },
        user: {
          mode: 'relation',
          fact_type: "user",
          has: "one",
          query: {
            _id: "fact.user_id"
          }
        },
        sessions: {
          mode: 'relation',
          fact_type: "session",
          has: "many",
          query: {
            did: "fact._id"
          }
        },
        sids: {
          mode: "push_unique"
        }
      }
    },
    referrer: {
      fields: {
        path: {
          mode: "inc_map"
        },
        full: {
          mode: "remove"
        }
      }
    },
    session: {
      fields: {
        device: {
          mode: 'relation',
          fact_type: "device",
          has: "one",
          query: {
            _id: "fact.did"
          }
        },
        user: {
          mode: 'relation',
          fact_type: 'user',
          has: 'one',
          query: {
            _id: 'fact.user_id'
          }
        },
        dids: {
          mode: "push_unique"
        },
        user_id: {
          mode: 'eval',
          condition: '"string" != typeof fact.user_id',
          map: {
            uid: "fact.device.user_id"
          },
          "eval": "'string' == typeof uid ? uid : null"
        },
        actions: {
          mode: "all"
        },
        referrer: {
          mode: "eval",
          condition: "'undefined' == typeof fact.referrer",
          "eval": "fact.get('actions._value.referrer').shift()"
        },
        referrer_domain: {
          mode: 'eval',
          condition: "'undefined' == typeof fact.referrer_domain",
          "eval": 'url(fact.referrer, "host")'
        },
        query: {
          mode: "eval",
          condition: "'undefined' == typeof fact.query",
          "eval": "url(fact.get('actions._value.url').shift(), 'query')"
        },
        source: {
          mode: "eval",
          "eval": function(settings) {
            var fn;
            fn = function(action) {
              var e, engine, local, query, referrer;
              try {
                local = url(action.url, 'host');
                query = url(action.url, 'query');
                referrer = action.referrer && url(action.referrer, 'host');
                if (query.gclid != null) {
                  return {
                    medium: 'cpc',
                    source: 'google',
                    campaign: query.gclid
                  };
                }
                if (referrer && referrer.length > 0 && referrer !== local) {
                  if (engine = referrer.match(/(google|bing|yahoo|ask|aol)\./)) {
                    return {
                      medium: 'search',
                      source: engine[1]
                    };
                  }
                  return {
                    medium: 'referrer',
                    source: referrer
                  };
                }
                if ((query != null ? query.utm_medium : void 0) != null) {
                  return {
                    medium: query.utm_medium,
                    source: query.utm_source,
                    campaign: query.utm_campaign
                  };
                }
                return {
                  medium: 'direct',
                  source: action.url
                };
              } catch (_error) {
                e = _error;
                return {
                  medium: 'error',
                  error: e.message,
                  stage: stage
                };
              }
            };
            return "(" + (fn.toString()) + ")(fact.actions[0]._value)";
          }
        }
      }
    },
    user: {
      fields: {
        eid: {
          mode: 'eval',
          "eval": "encrypt('md5', this._id, 'base64').replace(/[^a-z0-9]+/ig, '').slice(0, 6)"
        },
        info: {
          mode: 'extend'
        },
        devices: {
          mode: 'relation',
          fact_type: "device",
          has: "many",
          query: {
            user_id: "fact._id"
          }
        },
        sids: {
          mode: "push_unique"
        },
        dids: {
          mode: "push_unique"
        },
        score: {
          mode: "eval",
          "eval": "new_score",
          map: {
            actions: "this.devices.sessions.actions",
            rules: function(settings) {
              if ((settings.score_rules == null) || !Array.isArray(settings.score_rules)) {
                settings.score_rules = [];
              }
              return 'rules = ' + JSON.stringify(settings.score_rules);
            },
            changes: function(settings) {
              if ((settings.score_intervals == null) || !Array.isArray(settings.score_intervals)) {
                settings.score_intervals = [];
              }
              return 'changes = ' + JSON.stringify(settings.score_intervals);
            },
            new_score: function(settings) {
              var fn;
              fn = function(actions, rules, changes) {
                var action, date, days, eval_cond, make_datestamp, ret, scores, start_date, ts;
                if ((actions != null ? actions.map : void 0) == null) {
                  actions = [];
                }
                start_date = Infinity;
                actions = actions.filter(function(action) {
                  return action && (action._time != null) && (action._value != null);
                }).map(function(action) {
                  action._time = new Date(action._time);
                  start_date = Math.min(start_date, action._time.getTime());
                  return action;
                }).sort(function(one, two) {
                  return one._time.getTime() - two._time.getTime();
                });
                make_datestamp = function(date) {
                  var pad;
                  pad = function(v) {
                    return (v / 100).toFixed(2).slice(2);
                  };
                  date = new Date(date);
                  return [date.getFullYear(), pad(date.getMonth() + 1), pad(date.getDate())].join('-');
                };
                eval_cond = function(action, cond) {
                  var item, path, val, _i, _len;
                  if (!Array.isArray(cond.value)) {
                    cond.value = [cond.value];
                  }
                  try {
                    val = action._value;
                    path = cond.field.split('.');
                    for (_i = 0, _len = path.length; _i < _len; _i++) {
                      item = path[_i];
                      if (item.charAt(0) === '$') {
                        val = url(val, item.substr(1));
                        if (val == null) {
                          return false;
                        }
                      } else if (val[item] != null) {
                        val = val[item];
                      } else {
                        return false;
                      }
                    }
                    val = val.toString();
                    if (cond.method === 'exists') {
                      return true;
                    }
                    return cond.value.some(function(value) {
                      try {
                        switch (cond.method) {
                          case 'eq':
                            return val === value;
                          case 'neq':
                            return val !== value;
                          case 'start':
                            return val.indexOf(value) === 0;
                          case 'contains':
                            return val.indexOf(value) >= 0;
                          case 'end':
                            return val.slice(0 - value.length) === value;
                          case 'pattern':
                            return new RegExp(value).test(val);
                        }
                      } catch (_error) {}
                      return false;
                    });
                  } catch (_error) {}
                  return false;
                };
                scores = {
                  min: {},
                  full: {},
                  change: {},
                  current: 0
                };
                date = new Date(start_date).setHours(23, 59, 60, 0);
                days = [];
                while (actions.length > 0 && date < new Date().setHours(23, 59, 60, 999)) {
                  ts = make_datestamp(date);
                  while (actions.length > 0 && actions[0]._time < date) {
                    action = actions.shift();
                    scores.current += rules.reduce((function(value, rule) {
                      var pass, _ref;
                      pass = rule.conditions.every(function(cond) {
                        return eval_cond(action, cond);
                      });
                      if (!pass) {
                        return value;
                      }
                      if ((rule != null ? (_ref = rule.score) != null ? _ref.weight : void 0 : void 0) != null) {
                        return value + parseInt(rule.score.weight);
                      }
                      if ((rule != null ? rule.score : void 0) != null) {
                        return value + parseInt(rule.score);
                      }
                      console.log('no score?', rule);
                      return value;
                    }), 0);
                    scores.min[ts] = scores.current;
                  }
                  scores.full[ts] = scores.current;
                  days.push(scores.current);
                  date += 86400 * 1000;
                }
                if ((changes.filter(function(c) {
                  return c.name === 'day';
                }).length) === 0) {
                  changes.push({
                    name: 'day',
                    interval: [1, 'day']
                  });
                }
                changes.forEach(function(change) {
                  var key, n, val, _ref;
                  key = change.name;
                  n = change.interval[0] || 1;
                  switch (change.interval[1]) {
                    case 'week':
                      n *= 7;
                      break;
                    case 'month':
                      n *= 30;
                      break;
                    case 'year':
                      n *= 365;
                  }
                  val = scores.current - ((_ref = days[days.length - n - 1]) != null ? _ref : 0);
                  scores.change[key] = {
                    absolute: 0,
                    percentage: 0
                  };
                  if (scores.current > 0) {
                    return scores.change[key] = {
                      absolute: val,
                      percentage: -100 + Math.ceil(100 * (scores.current + val) / scores.current)
                    };
                  }
                });
                ret = scores.min;
                ret.change = scores.change;
                ret.today = scores.current;
                return ret;
              };
              return "(" + (fn.toString()) + ")(actions, rules, changes)";
            }
          }
        }
      }
    }
  },
  tracking_goals: {
    page: {
      triggers: [
        {
          type: 'load'
        }
      ],
      events: [
        {
          type: "track",
          data: {
            device: "=T.helpers.device()",
            action: {
              type: "page",
              url: "=location.href",
              title: "=document.title",
              referrer: "=document.referrer"
            }
          }
        }
      ]
    },
    form: {
      triggers: [
        {
          type: 'submit',
          element: 'form'
        }
      ],
      events: [
        {
          type: 'track',
          data: {
            action: {
              type: 'form',
              form_id: '=element.id || location.pathname',
              data: '=data'
            }
          }
        }
      ]
    },
    utm_identify: {
      triggers: [
        {
          type: "load"
        }
      ],
      conditions: [
        {
          type: "eval",
          "eval": "window.location.search.match(/utm_email=([^&]+@[^&]+)(&|$)/)"
        }
      ],
      events: [
        {
          type: "identify",
          data: {
            id: "=window.location.search.match(/utm_email=([^&]+?)(&|$)/)[1]",
            email: "=window.location.search.match(/utm_email=([^&]+?)(&|$)/)[1]",
            name: null
          }
        }
      ]
    }
  },
  tracking_software: []
});

({
  name: "MUSS specific options",
  version: "1.0.0",
  "public": false,
  fact_settings: {
    session: {
      $diff: [
        {
          field: "fields.langs",
          mode: "set",
          value: {
            mode: 'eval',
            "eval": function(settings) {
              var fn;
              fn = function(actions) {
                return actions.filter(function(action) {
                  return action._value.type === 'page';
                }).map(function(action) {
                  var val;
                  val = action._value.url.split('/')[3];
                  if (val === 'es_es') {
                    return 'es';
                  }
                  if (val === 'ru_ru') {
                    return 'ru';
                  }
                  return 'en';
                }).reduce(function(obj, lang) {
                  obj[lang]++;
                  obj.primary = (obj[lang] > obj[obj.primary] ? lang : obj.primary);
                  obj.last = lang;
                  return obj;
                }, {
                  es: 0,
                  en: 0,
                  ru: 0,
                  primary: 'en'
                });
              };
              return "(" + (fn.toString()) + ")(this.actions)";
            }
          }
        }
      ]
    },
    user: {
      $diff: [
        {
          field: "fields.days_to_purchase",
          mode: "set",
          value: {
            mode: 'eval',
            map: {
              sessions: 'this.devices.sessions'
            },
            "eval": function() {
              var fn;
              fn = function() {
                var action_count, days, end, found, session_count, start;
                sessions.sort(function(a, b) {
                  if (new Date(a._updated) < new Date(b._updated)) {
                    return -1;
                  } else {
                    return 1;
                  }
                });
                start = Infinity;
                end = false;
                session_count = 0;
                action_count = 0;
                found = (function() {
                  var action, session, _i, _j, _len, _len1, _ref;
                  for (_i = 0, _len = sessions.length; _i < _len; _i++) {
                    session = sessions[_i];
                    session_count++;
                    _ref = session.actions;
                    for (_j = 0, _len1 = _ref.length; _j < _len1; _j++) {
                      action = _ref[_j];
                      action_count++;
                      try {
                        start = Math.min(start, new Date(action._time));
                      } catch (_error) {}
                      try {
                        if (action._value.type === 'page' && action._value.url.match(/checkout\/[0-9]+\/complete\/?$/)) {
                          end = new Date(action._time);
                          return true;
                        }
                      } catch (_error) {}
                    }
                  }
                })();
                days = Math.abs(moment(start).diff(end, 'days', true).toFixed(1));
                if (found) {
                  return days;
                } else {
                  return -1;
                }
              };
              return "(" + (fn.toString()) + ")()";
            }
          }
        }
      ]
    }
  },
  pages: [
    {
      url: "/users",
      order: {
        after: "*"
      },
      callback: function() {
        this.widgets.table.fields.columns.push({
          field: 'langs.primary',
          title: 'Language',
          sort: false,
          filter: {
            type: 'select',
            values: {
              en: 'English',
              es: 'Spanish',
              ru: 'Russian'
            }
          }
        });
        return this.done();
      }
    }
  ]
});

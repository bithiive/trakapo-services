tracking_goals:
	identify_new_step_1: {
		triggers: [
			{
				type: "load",
				element: ".quick-apply > form"
			},
			{
				type: "event",
				event: "blur",
				element: "input",
				processForm: true
			},
			{
				type: "submit",
				element: ".quick-apply > form"
			}
		],
		conditions: [
			{
				type: "exists",
				element: ".quick-apply > form"
			},
			{
				type: "contains",
				element: ".quick-apply > form #inputLoanApplicantEmail",
				value: "@"
			}
		],
		events: [
			{
				type: "identify",
				atomic: true,
				data: {
					id: "=data['loan-applicant-email']",
					email: "=data['loan-applicant-email']",
					name: "=data['loan-applicant-name']",
					info:
						first_name: "=data['loan-applicant-name']",
						loan_amount: "=data['quick-apply-borrow']",
						loan_term: "=data['quick-apply-period']",
						tel: "=data['loan-applicant-phone-number']"
				},
				restrict: "=restrict(session, key, data)"
			}
		]
	},
	identify_step_1: {
		triggers: [
			{
				type: "load",
				element: "form#applicant-form-step-1"
			},
			{
				type: "event",
				event: "blur",
				element: "input",
				processForm: true
			},
			{
				type: "submit",
				element: "form#applicant-form-step-1"
			}
		],
		conditions: [
			{
				type: "exists",
				element: "form#applicant-form-step-1"
			},
			{
				type: "contains",
				element: "form#applicant-form-step-1 [name=\"LoanApplicationStepOne[applicant][email]\"]",
				value: "@"
			}
		],
		events: [
			{
				type: "identify",
				atomic: true,
				data: {
					id: "=data.LoanApplicationStepOne.applicant.email",
					email: "=data.LoanApplicationStepOne.applicant.email",
					name: "=data.LoanApplicationStepOne.applicant.first_name + ( ' ' + data.LoanApplicationStepOne.applicant.middle_name + ' ' ).replace( '  ', ' ' ) + data.LoanApplicationStepOne.applicant.surname",
					info:
						title: "=data.LoanApplicationStepOne.applicant.title",
						first_name: "=data.LoanApplicationStepOne.applicant.first_name",
						middle_name: "=data.LoanApplicationStepOne.applicant.middle_name",
						last_name: "=data.LoanApplicationStepOne.applicant.surname",
						loan_amount: "=data.LoanApplicationStepOne.loan_application.loan_amount",
						loan_term: "=data.LoanApplicationStepOne.loan_application.loan_term"
				},
				restrict: "=restrict(session, key, data)"
			}
		]
	},
	identify_step_2: {
		triggers: [
			{
				type: "load",
				element: ".form-step-2 form"
			},
			{
				type: "event",
				event: "blur",
				element: "input",
				processForm: true
			},
			{
				type: "submit",
				element: ".form-step-2 form"
			}
		],
		conditions: [
			{
				type: "exists",
				element: ".form-step-2 form"
			},
			{
				type: "contains",
				element: ".form-step-2 form [name=\"LoanApplicationStepTwo[applicant][mobile_number]\"]",
				value: "7"
			}
		],
		events: [
			{
				type: "identify",
				atomic: true,
				data: {
					info: {
						tel: "=data.LoanApplicationStepTwo.applicant.mobile_number",
						street_address: "=data.LoanApplicationStepTwo.address_1.address_thouroughfare",
						city: "=data.LoanApplicationStepTwo.address_1.address_post_town",
						postcode: "=data.LoanApplicationStepTwo.address_1.address_postcode"
					}
				},
				restrict: "=restrict(session, key, data)"
			}
		]
	},
	step: {
		triggers: [
			{
				type: "load"
			}
		],
		conditions: [
			{
				type: "url",
				path: "/apply/"
			}
		],
		events: [
			{
				type: "identify",
				atomic: true,
				data:
					info:
						stage: "=window.location.pathname.match(/\\/apply\\/([^\\/]*)/)[1]"
				restrict: "=restrict(session, key, data)"
			}
		]
	}

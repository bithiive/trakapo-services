"user": {
	"$diff": [
		{
			"mode": "set",
			"field": "fields.guarantors",
			"value": {
				"mode": "relation",
				"fact_type": "guarantor",
				"has": "many",
				"query": {
					"user_id": "fact._id",
					"_created": 'x={$gt: new Date(2014, 7, 1)}'
				}
			}
		}
	]
},

"guarantor": {
	"fields": {
		"_created": {
			"mode": "oldest"
		}
	}
}

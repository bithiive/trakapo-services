{
	"mapping_id": "guarantors_from_tracking",
	"fact_type": "guarantor",
	"fact_identifier": "info.id + '-' + info.action.data.Guarantor.email",
	"info_type": "track",
	"conditions": [
		"info.action.type == 'form'",
		"info.action.form_id == '/apply/step-5'",
		"info.action.data.Guarantor.email.indexOf('@') !== -1"
	],
	"fields": [
		{
			"field": "_created",
			"value": "(new Date()).toISOString()"
		},
		{
			"field": "first_name",
			"value": "info.action.data.Guarantor.first_name"
		},
		{
			"field": "surname",
			"value": "info.action.data.Guarantor.surname"
		},
		{
			"field": "applicant_confirmed_age",
			"value": "!!(info.action.data.Guarantor.applicant_confirmed_age)"
		},
		{
			"field": "email",
			"value": "info.action.data.Guarantor.email"
		},
		{
			"field": "mobile_number",
			"value": "info.action.data.Guarantor.mobile_number"
		},
		{
			"field": "applicant_relation",
			"value": "info.action.data.Guarantor.applicant_relation"
		},
		{
			"field": "last_did",
			"value": "info.did"
		},
		{
			"field": "last_sid",
			"value": "info.sid"
		},
		{
			"field": "user_id",
			"value": "info.id"
		}
	]
},

{
	"mapping_id": "guarantors_from_guarantor",
	"fact_type": "guarantor",
	"fact_identifier": "info.id + '-' + info.email",
	"info_type": "guarantor",
	"conditions": [
		"info.email.indexOf('@') !== -1"
	],
	"fields": [
		{
			"field": "_created",
			"value": "(new Date()).toISOString()"
		},
		{
			"field": "first_name",
			"value": "info.first_name || fact.data.first_name"
		},
		{
			"field": "surname",
			"value": "info.surname || fact.data.surname"
		},
		{
			"field": "applicant_confirmed_age",
			"value": "!!(info.applicant_confirmed_age) || fact.data.applicant_confirmed_age"
		},
		{
			"field": "email",
			"value": "info.email || fact.data.email"
		},
		{
			"field": "mobile_number",
			"value": "info.mobile_number || fact.data.mobile_number"
		},
		{
			"field": "applicant_relation",
			"value": "info.applicant_relation || fact.data.applicant_relation"
		},
		{
			"field": "account_activated",
			"value": "info.account_activated",
			"condition": "typeof info.account_activated !== 'undefined'"
		},
		{
			"field": "application_completed_step",
			"value": "info.application_completed_step",
			"condition": "typeof info.application_completed_step !== 'undefined'"
		},
		{
			"field": "activation_token",
			"value": "( info.activation_token === false || info.activation_token === 'false' ) ? '' : info.activation_token",
			"condition": "typeof info.activation_token !== 'undefined'"
		},
		{
			"field": "activation_token_sms",
			"value": "( info.activation_token_sms === false || info.activation_token_sms === 'false' ) ? '' : info.activation_token_sms",
			"condition": "typeof info.activation_token_sms !== 'undefined'"
		},
		{
			"field": "status",
			"value": "info.status",
			"condition": "typeof info.status !== 'undefined'"
		},
		{
			"field": "user_id",
			"value": "info.id || fact.data.id"
		}
	]
}

{
	"mapping_id": "guarantors_from_guarantor_update_user",
	"fact_type": "user",
	"fact_identifier": "info.id",
	"info_type": "guarantor",
	"conditions": [
		"info.email.indexOf('@') !== -1"
	],
	"fields": []
}
